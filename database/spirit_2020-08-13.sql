# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.31)
# Base de datos: spirit
# Tiempo de Generación: 2020-08-14 04:03:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla activity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `id_activity` varchar(36) NOT NULL,
  `description_activity` varchar(255) NOT NULL,
  `id_component` varchar(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_activity`),
  KEY `fk_activity_component1_idx` (`id_component`),
  CONSTRAINT `fk_activity_component1` FOREIGN KEY (`id_component`) REFERENCES `component` (`id_component`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla component
# ------------------------------------------------------------

DROP TABLE IF EXISTS `component`;

CREATE TABLE `component` (
  `id_component` varchar(36) NOT NULL,
  `name_component` varchar(36) NOT NULL,
  `id_project` varchar(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_component`),
  KEY `fk_component_project_idx` (`id_project`),
  CONSTRAINT `fk_component_project` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla coverage_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coverage_type`;

CREATE TABLE `coverage_type` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `coverage_type` WRITE;
/*!40000 ALTER TABLE `coverage_type` DISABLE KEYS */;

INSERT INTO `coverage_type` (`id`, `name`)
VALUES
	('79a95878-9882-11ea-bb37-0242ac130002','INTERNACIONAL'),
	('79a95a9e-9882-11ea-bb37-0242ac130002','NACIONAL'),
	('79a95c42-9882-11ea-bb37-0242ac130002','REGIONAL'),
	('79a95d14-9882-11ea-bb37-0242ac130002','PROVINCIAL'),
	('79a95e54-9882-11ea-bb37-0242ac130002','CANTONAL'),
	('79a95f1c-9882-11ea-bb37-0242ac130002','PARROQUIAL');

/*!40000 ALTER TABLE `coverage_type` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla dependency
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dependency`;

CREATE TABLE `dependency` (
  `id_dependency` varchar(36) NOT NULL,
  `name_dependency` varchar(500) NOT NULL,
  `acronym_dependency` varchar(10) DEFAULT NULL,
  `contact_dependency` varchar(150) DEFAULT NULL,
  `city_dependency` varchar(100) DEFAULT NULL,
  `email_dependency` varchar(100) DEFAULT NULL,
  `web_dependency` varchar(100) DEFAULT NULL,
  `phone_dependency` varchar(100) DEFAULT NULL,
  `participationType_dependency` text,
  `id_dependencyType` varchar(36) DEFAULT NULL,
  `father_dependency` varchar(36) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_dependency`),
  KEY `fk_dependency_dependency1_idx` (`father_dependency`),
  KEY `fk_dependency_dependencyType_idx` (`id_dependencyType`),
  CONSTRAINT `fk_dependency_dependency1` FOREIGN KEY (`father_dependency`) REFERENCES `dependency` (`id_dependency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dependency_dependencyType` FOREIGN KEY (`id_dependencyType`) REFERENCES `dependencytype` (`id_dependencyType`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dependency` WRITE;
/*!40000 ALTER TABLE `dependency` DISABLE KEYS */;

INSERT INTO `dependency` (`id_dependency`, `name_dependency`, `acronym_dependency`, `contact_dependency`, `city_dependency`, `email_dependency`, `web_dependency`, `phone_dependency`, `participationType_dependency`, `id_dependencyType`, `father_dependency`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	('a92c8ffa-9893-11ea-bb37-0242ac130002','ESCUELA SUPERIOR POLITECNICA DE CHIMBORAZO','ESPOCH','Byron Vaca','Riobamba','byron.vaca@espoch.edu.ec','http://cimogsys.espoch.edu.ec','0998220818 ext 109','as','9d692040-9896-11ea-bb37-0242ac130002',NULL,NULL,'2020-04-30 18:40:31',NULL),
	('a92c9220-9893-11ea-bb37-0242ac130002','FACULTAD DE INFORMÁTICA Y ELECTRÓNICA','FIE',NULL,NULL,'fie@espoch.edu.ec',NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,'2020-04-26 07:41:17',NULL),
	('a92c9342-9893-11ea-bb37-0242ac130002','FACULTAD DE ADMINISTRACIÓN DE EMPRESAS','FADE',NULL,NULL,'fade@espoch.edu.ec',NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,'2020-04-26 07:45:41',NULL),
	('a92c9428-9893-11ea-bb37-0242ac130002','FACULTAD DE MECÁNICA','FM',NULL,'Riobamba','mecanica@espoch.edu.ec',NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,'2020-04-26 10:53:00',NULL),
	('a92c950e-9893-11ea-bb37-0242ac130002','FACULTAD DE RECURSOS NATURALES','FRN',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c95e0-9893-11ea-bb37-0242ac130002','FACULTAD DE SALUD PÚBLICA','FSP',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c96a8-9893-11ea-bb37-0242ac130002','FACULTAD DE CIENCIAS','FC',NULL,NULL,'ciencias@espoch.edu.ec',NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,'2020-04-26 10:54:33',NULL),
	('a92c9770-9893-11ea-bb37-0242ac130002','FACULTAD DE CIENCIAS PECUARIAS','FCP',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c9838-9893-11ea-bb37-0242ac130002','CENTRO DE EDUCACIÓN FÍSICA','CEF',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c9900-9893-11ea-bb37-0242ac130002','SEDE FRANCISCO DE ORELLANA','SFO',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c99c8-9893-11ea-bb37-0242ac130002','SEDE MORONA SANTIAGO','SMS',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c9a86-9893-11ea-bb37-0242ac130002','CENTRO DE IDIOMAS','SMS',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c9b44-9893-11ea-bb37-0242ac130002','INSTITUTO DE INVESTIGACIONES','IDI',NULL,NULL,'hugo.moreno@espoch.edu.ec',NULL,NULL,NULL,'9d69243c-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,'2020-04-25 00:21:10',NULL),
	('a92c9c0c-9893-11ea-bb37-0242ac130002','BODEGA','BDESPOCH','William Naur','Riobamba','bodega@espoch.edu.ec','bodega.espoch.edu.ec','0998220818','Interna','9d69243c-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002','2020-04-24 00:15:29','2020-04-24 00:15:29',NULL),
	('a92cce48-9893-11ea-bb37-0242ac130002','CARRERA DE INGENIERIA EN SISTEMAS','EIS',NULL,NULL,'eis@espoch.edu.ec',NULL,NULL,NULL,'9d691c58-9896-11ea-bb37-0242ac130002','a92c9220-9893-11ea-bb37-0242ac130002','2020-04-26 23:18:22','2020-04-26 23:18:22',NULL);

/*!40000 ALTER TABLE `dependency` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla dependency_has_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dependency_has_users`;

CREATE TABLE `dependency_has_users` (
  `id_dependency` varchar(36) NOT NULL,
  `id_user` varchar(36) NOT NULL,
  PRIMARY KEY (`id_dependency`,`id_user`),
  KEY `fk_dependency_has_users_users1_idx` (`id_user`),
  KEY `fk_dependency_has_users_dependency1_idx` (`id_dependency`),
  CONSTRAINT `fk_dependency_has_users_dependency1` FOREIGN KEY (`id_dependency`) REFERENCES `dependency` (`id_dependency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dependency_has_users_users1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla dependencytype
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dependencytype`;

CREATE TABLE `dependencytype` (
  `id_dependencyType` varchar(36) NOT NULL,
  `name_dependencyType` varchar(50) NOT NULL,
  PRIMARY KEY (`id_dependencyType`),
  UNIQUE KEY `name_dependencyType_UNIQUE` (`name_dependencyType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dependencytype` WRITE;
/*!40000 ALTER TABLE `dependencytype` DISABLE KEYS */;

INSERT INTO `dependencytype` (`id_dependencyType`, `name_dependencyType`)
VALUES
	('9d691c58-9896-11ea-bb37-0242ac130002','CARRERA'),
	('5e58036a-bc06-11ea-b3de-0242ac130004','EXTERNA'),
	('9d691eb0-9896-11ea-bb37-0242ac130002','FACULTAD'),
	('9d692040-9896-11ea-bb37-0242ac130002','INSTITUCIONAL'),
	('9d692356-9896-11ea-bb37-0242ac130002','UNIDAD ACADÉMICA'),
	('9d69243c-9896-11ea-bb37-0242ac130002','UNIDAD ADMINISTRATIVA');

/*!40000 ALTER TABLE `dependencytype` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id_group` varchar(36) NOT NULL,
  `code_group` varchar(10) NOT NULL,
  `name_group` varchar(255) NOT NULL,
  `acronym_group` varchar(11) NOT NULL,
  `mission_group` text,
  `vision_group` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_dependency` varchar(36) NOT NULL,
  `id_groupType` varchar(36) NOT NULL,
  `id_researchCenter` int(11) DEFAULT NULL,
  `active_group` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_group`),
  UNIQUE KEY `name_researchGroup_UNIQUE` (`name_group`),
  UNIQUE KEY `code_group_UNIQUE` (`code_group`),
  KEY `fk_researchGroup_researchCenter1_idx` (`id_researchCenter`),
  KEY `fk_group_groupType1_idx` (`id_groupType`),
  KEY `fk_group_dependency1_idx` (`id_dependency`),
  CONSTRAINT `fk_group_dependency1` FOREIGN KEY (`id_dependency`) REFERENCES `dependency` (`id_dependency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_group_groupType1` FOREIGN KEY (`id_groupType`) REFERENCES `grouptype` (`id_groupType`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_researchGroup_researchCenter1` FOREIGN KEY (`id_researchCenter`) REFERENCES `researchcenter` (`id_researchCenter`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id_group`, `code_group`, `name_group`, `acronym_group`, `mission_group`, `vision_group`, `created_at`, `updated_at`, `id_dependency`, `id_groupType`, `id_researchCenter`, `active_group`, `deleted_at`)
VALUES
	('6773020e-c95f-11ea-bdac-6003088c543c','aCEFGI-001','aGRUPO DE INVESTIGACIÓN EN CIENCIAS APLICADAS AL DEPORTE Y A LA EDUCACIÓN FÍSICA','aCIADEF','Es un grupo interdisciplinario, adscrito al Centro de Educación Física de la Escuela Superior Politécnica de Chimborazo, avalado por el Instituto de Investigaciones; que tiene como propósito contribuir al desarrollo del deporte y la Educación Física en la ciudad, provincia y país, pertinentes a los requerimientos del medio; con equidad, responsabilidad social, pluridiversidad y convivencia.','En el año 2022, INCIADEF será un grupo de investigación reconocido y clasificado en los Grupos de Investigación de la Educación Superior, que habrá generado proyectos d investigación con impacto en la comunidad académica, científica, en el deporte y la educación física; con evidencia del mejoramiento del estilo de vida de la población en general.','2020-07-18 20:30:18','2020-07-20 18:03:25','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe3ba6-98b9-11ea-bb37-0242ac130002','CEFGI-001','GRUPO DE INVESTIGACIÓN EN CIENCIAS APLICADAS AL DEPORTE Y A LA EDUCACIÓN FÍSICA','INCIADEF','Es un grupo interdisciplinario, adscrito al Centro de Educación Física de la Escuela Superior Politécnica de Chimborazo, avalado por el Instituto de Investigaciones; que tiene como propósito contribuir al desarrollo del deporte y la Educación Física en la ciudad, provincia y país, pertinentes a los requerimientos del medio; con equidad, responsabilidad social, pluridiversidad y convivencia.','En el año 2022, INCIADEF será un grupo de investigación reconocido y clasificado en los Grupos de Investigación de la Educación Superior, que habrá generado proyectos d investigación con impacto en la comunidad académica, científica, en el deporte y la educación física; con evidencia del mejoramiento del estilo de vida de la población en general.','2020-01-24 16:28:00','2020-07-20 18:03:24','a92c9838-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe3da4-98b9-11ea-bb37-0242ac130002','FIEGI-010','GRUPO DE INVESTIGACIÓN E INTERACCIÓN EN LAS TECNOLOGÍAS DE LA COMUNICACIÓN','IITC','SIN MISIÓN','SIN VISIÓN','2020-01-24 18:17:51','2020-08-13 08:18:34','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe3e94-98b9-11ea-bb37-0242ac130002','FIEGI-011','GRUPO DE INVESTIGACIÓN EN ELECTROMAGNETISMO Y MICROONDAS','GIEM','SIN MISIÓN','SIN VISIÓN','2020-01-24 18:18:19','2020-07-20 18:04:56','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe3f66-98b9-11ea-bb37-0242ac130002','FIEGI-012','GRUPO DE INVESTIGACIÓN DE COMUNICACIONES INALAMBRICAS','GICI','Su objeto es la realización, promoción y difusión de la investigación científica generadora de transferencia de conocimiento, en el ámbito de las Tecnologías de la Información y Comunicación.','La finalidad de la investigación del GICI tiene una vocación aplicada a la generación de proyectos de investigación, publicación de resultados de investigación y trasferencia de conocimientos a los actores en el ámbito de las Telecomunicaciones.','2020-01-24 18:19:08','2020-07-20 18:00:40','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe402e-98b9-11ea-bb37-0242ac130002','FADEGI-001','GRUPO DE INVESTIGACIÓN EN MODELOS Y SISTEMAS DE GESTION','IMSG','Somos un equipo dedicado a la investigación, desarrollo, aplicación, análisis, evaluación de impacto y difusión del conocimiento general a partir de la ejecución de proyectos de investigación en el ámbito de la gestión y administración contemporánea para contribuir al bienestar y evolución de las empresas públicas y/o privadas.','Para el año 2022 seremos un grupo reconocido por la generación de conocimiento en nuestras líneas de investigación y registrado en el Sistema de Ciencia y Tecnología; referentes en la generación y divulgación del conocimiento relacionado con la gestión y administración contemporánea, contribuyendo con el desarrollo regional y nacional.','2020-01-24 18:34:11','2020-07-19 01:02:48','a92c9342-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe422c-98b9-11ea-bb37-0242ac130002','FMGI-001','GRUPO DE INVESTIGACIÓN Y ESTUDIO EN BIOINGENIERÍA','GIEBI','Generar, desarrollar, asimilar y aplicar el conocimiento científico y tecnológico en el desarrollo de tecnología biomecánica aplicada a mejorar las condiciones motrices de las personas con movilidad reducida.','Ser un grupo de investigación dedicado en lo fundamental a la investigación y al desarrollo tecnológico, centrada en la generación de equipos biomecánicos, que ayuden a mejorar las condiciones motrices de las personas con ciertos grados de discapacidad motriz, impulsando acciones orientadas a la generación del conocimiento que permitan buscar solución a las diversas problemáticas de las personas discapacitadas en base a la aplicación de diversas ciencias de la Ingeniería.','2020-01-24 18:36:25','2020-07-19 01:02:48','a92c9428-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe4308-98b9-11ea-bb37-0242ac130002','FIEGI-002','GRUPO DE INVESTIGACION EN INGENIERIA DE SOFTWARE','GRIISOFT','Lograr una participación proactiva directa y permanente de los integrantes del grupo en la ejecución de proyectos de investigación y vinculación con la colectividad en los ámbitos de desarrollo, mantenimiento y operación de sistemas informáticos, que garanticen la gestión, la generación de investigación científica y tecnológica que apoye y difunda los resultados de investigación, con reconocimiento nacional e internacional.','Ser uno de los más importantes grupos de investigación en Ingeniería de Software en América Latina, reconocido a nivel nacional e internacional, cuyo trabajo sea reconocido tanto por la academia como por el sector empresarial; se enfoca en el desarrollo de proyectos de investigación en los que se aplica con éxito la teoría y experimentación de todas las áreas de la ingeniería de software; facilitando la transferencia tecnológica y la adopción de buenas prácticas en el desarrollo de mantenimiento y operación de software y es capaz de socializar sus resultados de impacto a nivel nacional como internacional, buscando siempre colaborar con el desarrollo de una industria competitiva a escala internacional','2020-01-24 16:30:20','2020-07-19 01:02:49','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe4524-98b9-11ea-bb37-0242ac130002','FIEGI-003','GRUPO DE INVESTIGACIÓN KARAY LABORATORIO CREATIVO','GIK','Generar conocimiento científico en los ámbitos de diseño, comunicación, arte y cultura , en entornos educativos, sociales y culturales de la provincia y el país, fomentando experiencias participativas y experimentales.','Ser un grupo de investigación que cuente con el reconocimiento local y nacional en aspectos relacionados con el diseño, comunicación, arte, cultura y creatividad manteniendo una política de mejoramiento continuo de sus integrantes y el desarrollo de sus capacidades investigativas conducentes al aporte de soluciones a problemas del contexto que estén enmarcadas en las líneas de investigación del grupo KARAY laboratorio creativo.','2020-01-24 16:33:03','2020-07-20 18:02:00','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe45e2-98b9-11ea-bb37-0242ac130002','FIEGI-004','GRUPO DE INVESTIGACIÓN TECNOLOGÍAS DE LA INFORMACIÓN PARA LA GESTIÓN DEL CONOCIMIENTO','TIGECON','SIN MISIÓN','SIN VISIÓN','2020-01-24 16:35:11','2020-07-19 01:02:50','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe4696-98b9-11ea-bb37-0242ac130002','FIEGI-005','GRUPO DE INVESTIGACIÓN SEGURIDAD INFORMÁTICA Y TELEMÁTICA','SEGINTE','SIN MISIÓN','SIN VISIÓN','2020-01-24 16:36:00','2020-07-19 01:02:50','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe4754-98b9-11ea-bb37-0242ac130002','FIEGI-006','GRUPO DE INVESTIGACION ENERGÍA EÓLICA','GEE','Desarrollar proyectos de investigación enfocados en la utilización de fuentes alternativas de energía eléctrica, cuyos resultados sirvan para promover la generación de energías limpias para impulsar el desarrollo económico, turístico y social de la provincia de Chimborazo y del país.','Promover proyectos energéticos de origen renovable innovadores y eficientes en el hábitat natural, contribuyendo a satisfacer la demanda energética de la población, asumiendo la responsabilidad de preservar el ecosistema, y de dar respuesta a las necesidades de la provincia y del país, generando valor para nuestros ciudadanos y favoreciendo la difusión del valor del medio natural en el mundo.','2020-01-24 16:37:09','2020-07-19 01:02:51','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe4808-98b9-11ea-bb37-0242ac130002','FIEGI-007','GRUPO DE INVESTIGACIÓN EN TECNOLOGÍAS DE LA ELECTRÓNICA Y AUTOMÁTICA.','GEE2','Constituirse como un grupo élite de trabajo investigativo, centrado en contribuir al desarrollo tecnológico mediante la generación de proyectos de investigación e innovación que den solución a problemáticas locales, nacionales e internacionales en áreas de la Ingeniería Electrónica, Eléctrica y Automatización Industrial, enfocándose en garantizar la excelencia, calidad y originalidad en el desarrollo de actividades que aporten a la sociedad y favorezcan su crecimiento productivo.','Ser un grupo de investigación científica de reconocimiento local, nacional e internacional, enfocado a contribuir al desarrollo tecnológico y productivo de la sociedad en áreas de la Ingeniería Electrónica, Eléctrica y Automatización Industrial, apoyado en un contingente multidisciplinario de personas conformado por académicos, investigadores y estudiantes comprometidos con los procesos de transferencia de conocimientos mediante eventos y publicaciones regionales e internacionales.','2020-01-24 16:38:18','2020-07-20 18:06:30','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe48bc-98b9-11ea-bb37-0242ac130002','FIEGI-008','GRUPO DE INVESTIGACIÓN, MODELADO, ANIMACIÓN Y SIMULACIÓN 3D','MSA3D','Somos un grupo de investigadores dedicados a la búsqueda de soluciones informáticas, electrónicas y de diseño 3D para la academia, investigación y vinculación. Nos comprometemos a producir productos y servicios de calidad a nivel de la ESPOCH y fuera de ella.','Ser un referente en el ámbito universitario a nivel local y nacional, ofreciendo productos con innovación y con un valor agregado referente a soluciones tecnológicas para necesidades sociales. Nos ubicamos en el contexto de utilizar tecnología de punta y estar en constante innovación realizando la “extra milla”.','2020-01-24 18:16:20','2020-07-20 18:06:55','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe497a-98b9-11ea-bb37-0242ac130002','FIEGI-009','GRUPO DE INVESTIGACIÓN LEARNING ENGLISH WITH ICTs','ICTs','Desarrollar Programas Educativos basados en tecnología, aplicaciones y software para mejorar las competencias lingüísticas como Listening, Speaking, Reading y Writing','Desarrollar y validar metodologías educativas innovadoras en el campo de la enseñanza del idioma Ingles a través del uso de la tecnología educativa, TIC, aplicaciones software que puedan ser utilizadas en la enseñanza universitaria pero también en otros niveles educativos','2020-01-24 18:17:19','2020-07-20 18:18:26','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL);

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla groups_has_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups_has_users`;

CREATE TABLE `groups_has_users` (
  `id_group` varchar(36) NOT NULL,
  `id_user` varchar(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_group`,`id_user`),
  KEY `fk_groups_has_users_users1_idx` (`id_user`),
  KEY `fk_groups_has_users_groups1_idx` (`id_group`),
  CONSTRAINT `fk_groups_has_users_groups1` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id_group`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_groups_has_users_users1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



# Volcado de tabla grouptype
# ------------------------------------------------------------

DROP TABLE IF EXISTS `grouptype`;

CREATE TABLE `grouptype` (
  `id_groupType` varchar(36) NOT NULL,
  `name_groupType` varchar(255) NOT NULL,
  PRIMARY KEY (`id_groupType`),
  UNIQUE KEY `name_groupType_UNIQUE` (`name_groupType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `grouptype` WRITE;
/*!40000 ALTER TABLE `grouptype` DISABLE KEYS */;

INSERT INTO `grouptype` (`id_groupType`, `name_groupType`)
VALUES
	('1','INVESTIGACION'),
	('2','VINCULACION');

/*!40000 ALTER TABLE `grouptype` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla impactsector
# ------------------------------------------------------------

DROP TABLE IF EXISTS `impactsector`;

CREATE TABLE `impactsector` (
  `id_impactSector` varchar(36) NOT NULL,
  `name_impactSector` varchar(255) NOT NULL,
  PRIMARY KEY (`id_impactSector`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla impactsector_has_project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `impactsector_has_project`;

CREATE TABLE `impactsector_has_project` (
  `id_impactSector` varchar(36) NOT NULL,
  `id_project` varchar(36) NOT NULL,
  PRIMARY KEY (`id_impactSector`,`id_project`),
  KEY `fk_impactSector_has_project_project1_idx` (`id_project`),
  KEY `fk_impactSector_has_project_impactSector_idx` (`id_impactSector`),
  CONSTRAINT `fk_impactSector_has_project_impactSector` FOREIGN KEY (`id_impactSector`) REFERENCES `impactsector` (`id_impactSector`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_impactSector_has_project_project1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla oauth_access_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_access_tokens`;

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`)
VALUES
	('038fd2fc028581c0cd43c1ba11f122ef5dc6a31980c43e74b41d406a1c987993c40d0bb97b92a5c9','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-01 03:45:54','2020-08-01 03:45:54','2021-08-01 03:45:54'),
	('0aaac0e789301510bb0838fbe3a7d8200d731323775c88c8f5e8fc5ec51c4f4fe2a60e3386abc7da','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 07:56:22','2020-08-13 07:56:22','2021-08-13 07:56:22'),
	('19880e4772c08999da13302558d97180b5906839c1edd6ecbe6213fde34223a41acea46a8ec8f038','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-01 03:44:07','2020-08-01 03:44:07','2021-08-01 03:44:07'),
	('2f61999d7ecbde04aa487e92ad5a13d108e28473e25fc85e4584d1a4b4da9311bfce73e54105b4c5','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-01 04:02:58','2020-08-01 04:02:58','2021-08-01 04:02:58'),
	('4d625b018f10cc93107b63d1721e9c3bd8ad30bec940b79b9199f8016e4fc13928365bd6f4a82442','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 21:22:07','2020-08-13 21:22:07','2021-08-13 21:22:07'),
	('4f6563f65ee745ffb8f54255d674418a8a7e0dff58602ba6f867df28c3f00f217cae492359d7c666','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 07:32:27','2020-08-13 07:32:27','2021-08-13 07:32:27'),
	('5477e8e11b8bf7b71033e7f86868db744079812756c3cb677fd4478bf48cba2d23bddd05d624c4a8','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 22:51:23','2020-08-13 22:51:23','2021-08-13 22:51:23'),
	('6f7ac353054c74065f6a4207f786ac0bf7c4361940be29644cf31021e3ae7fdd19db883590ea20b7','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 07:37:06','2020-08-13 07:37:06','2021-08-13 07:37:06'),
	('789c3cb6b36664f5585c04f6c77434b59ab5b8c1b6f2819d3c508e9f257f7a75ada97177b3246ccf','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 22:34:18','2020-08-13 22:34:18','2021-08-13 22:34:18'),
	('8008e2b77fac9d3b1e195b3403336d914cb654960632a66169c8f5781dbccb70d656f0f11f4ab4d2','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 07:44:45','2020-08-13 07:44:45','2021-08-13 07:44:45'),
	('8f2262a64e44e26e79884be801704c1d66d954bc30c9d0f274c78ca40bed456ee012f53574007783','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 07:49:15','2020-08-13 07:49:15','2021-08-13 07:49:15'),
	('bca5beaae17a9434735d2363ed9c2c28ee3de37919913281cffc74c6d1af1de013e4ae4beceb52e8','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',1,'2020-08-13 22:22:32','2020-08-13 22:22:32','2021-08-13 22:22:32'),
	('c461c65a57fcb473609b1572eb47be9f0b0e41e4ecf1daee97d6c724fc38dc943c0f3576316646a9','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 07:28:20','2020-08-13 07:28:20','2021-08-13 07:28:20'),
	('c8acada83f21c3f070f7673f8deb7e51be5ba44d3ddc1b4d8a0804813dd68a4d10331742bf738ee6','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 21:35:01','2020-08-13 21:35:01','2021-08-13 21:35:01'),
	('cf52cc7ee5668cb4330c732852bf2073b8d14ea76ae3ea7c1bf38c3084808e3cbb43b2273467d9f6','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-01 04:08:20','2020-08-01 04:08:20','2021-08-01 04:08:20'),
	('e2cc482d86c7415a566c56e9e712e38db420b1f341c3238b88d8da287b503939ab1cc8fe0cfc38a7','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 22:23:04','2020-08-13 22:23:04','2021-08-13 22:23:04'),
	('eb903b6498579343c1d8f993c7d8961fd67497bdbae6926f51190f575982a5d833dbf96204121be0','17ffa6c4-98d0-11ea-bb37-0242ac130002',3,NULL,'[]',0,'2020-08-13 19:25:51','2020-08-13 19:25:51','2021-08-13 19:25:51');

/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla oauth_auth_codes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_auth_codes`;

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla oauth_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_clients`;

CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`)
VALUES
	(1,NULL,'Laravel Personal Access Client','wYsbbPJilgKdpZWm7MQKVkQh23whXCucu6kyP1s9',NULL,'http://localhost',1,0,0,'2020-08-01 02:40:52','2020-08-01 02:40:52'),
	(2,NULL,'Laravel Password Grant Client','UE59atGt6YRnERnWL7egAHNDvqYGBQlg2rGXUBTo','users','http://localhost',0,1,0,'2020-08-01 02:40:52','2020-08-01 02:40:52'),
	(3,NULL,'spirit7','r0Ai7t5DcOISmzSAYrqCpPDTb5nQ3WLTtNbhDAtq','users','http://localhost',0,1,0,'2020-08-01 03:15:26','2020-08-01 03:15:26');

/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla oauth_personal_access_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_personal_access_clients`;

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`)
VALUES
	(1,1,'2020-08-01 02:40:52','2020-08-01 02:40:52');

/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla oauth_refresh_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_refresh_tokens`;

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`)
VALUES
	('04aaddfb27276209602581f420e9a99df8ec20db3ba9b260fee84468bd277bbab98bbad70124ba03','0aaac0e789301510bb0838fbe3a7d8200d731323775c88c8f5e8fc5ec51c4f4fe2a60e3386abc7da',0,'2021-08-13 07:56:22'),
	('1ef4ccef1274136fbbee71d8e349264e86cb05037b60ae883616f46de77b42246d91e1c02d72de05','8f2262a64e44e26e79884be801704c1d66d954bc30c9d0f274c78ca40bed456ee012f53574007783',0,'2021-08-13 07:49:15'),
	('1fab5039585a439d97233a8aa36a834c28efb822588422fe55ee9aff072ca4c0cac68a95c409cb8d','6f7ac353054c74065f6a4207f786ac0bf7c4361940be29644cf31021e3ae7fdd19db883590ea20b7',0,'2021-08-13 07:37:06'),
	('2941efdab581f83445c48e4607e6d8927f0ab5c92aa1f1211f5dfc25f5df31de5dfca73ca8063bad','cf52cc7ee5668cb4330c732852bf2073b8d14ea76ae3ea7c1bf38c3084808e3cbb43b2273467d9f6',0,'2021-08-01 04:08:20'),
	('38b42221808ad91f80a975505872ad5072cd33009271d1891b191df3b73efcd5e2bf7d0ea4c07b00','4d625b018f10cc93107b63d1721e9c3bd8ad30bec940b79b9199f8016e4fc13928365bd6f4a82442',0,'2021-08-13 21:22:07'),
	('4bedf220f8e44ff57ce405a3ed9cb754f4034ac3499c4d6245df2266d463f1a15b39d29dc5f56450','19880e4772c08999da13302558d97180b5906839c1edd6ecbe6213fde34223a41acea46a8ec8f038',0,'2021-08-01 03:44:07'),
	('4fa59270df4a97461731a16b195014857c3dd81738a040ce2872dfbe732af874269e626d898daa48','e2cc482d86c7415a566c56e9e712e38db420b1f341c3238b88d8da287b503939ab1cc8fe0cfc38a7',0,'2021-08-13 22:23:04'),
	('4ffe4fff5fa12056447d21b1d066e0c99cd00619ef571a03c858262940487c0dc7b89788cc38e37b','038fd2fc028581c0cd43c1ba11f122ef5dc6a31980c43e74b41d406a1c987993c40d0bb97b92a5c9',0,'2021-08-01 03:45:54'),
	('6d7c54cf426b8a21c794771d1c1e307c6f3013fd550038a0b0cb65d98008ee5e37be628f9df57016','4f6563f65ee745ffb8f54255d674418a8a7e0dff58602ba6f867df28c3f00f217cae492359d7c666',0,'2021-08-13 07:32:27'),
	('77dc9b224c6f5f1f3108786f996e0737ecabbb1feb56b3d064cfc7bcb7388dd8e4ccfee38c7d1ec0','bca5beaae17a9434735d2363ed9c2c28ee3de37919913281cffc74c6d1af1de013e4ae4beceb52e8',0,'2021-08-13 22:22:32'),
	('7ab128434f0bfd0037ca30dad1288c2cf5f90cd82743e9daa493d4114137ab576091ce50c74bc983','eb903b6498579343c1d8f993c7d8961fd67497bdbae6926f51190f575982a5d833dbf96204121be0',0,'2021-08-13 19:25:51'),
	('7d88ebf6860e192632937024ef51df8e99ae0ac625ef611ed5481d9f5afd339cd975b7a3f9901026','c461c65a57fcb473609b1572eb47be9f0b0e41e4ecf1daee97d6c724fc38dc943c0f3576316646a9',0,'2021-08-13 07:28:20'),
	('8921baeec643689e35404094a05876716cc4d40a29c5ad7572d2cbb9285683df16bf37c26f2ae7f7','789c3cb6b36664f5585c04f6c77434b59ab5b8c1b6f2819d3c508e9f257f7a75ada97177b3246ccf',0,'2021-08-13 22:34:18'),
	('a4e596c890226da5d6b72713bcd24ea2138c7cb869eef04f66078b5d274f09939123511d3a6aa500','2f61999d7ecbde04aa487e92ad5a13d108e28473e25fc85e4584d1a4b4da9311bfce73e54105b4c5',0,'2021-08-01 04:02:58'),
	('b8edf08218a3ec9675d6fc7dd08694ca5fa1aae3f88da0e460261ddad2c7fa7b4e2473c6f8615509','c8acada83f21c3f070f7673f8deb7e51be5ba44d3ddc1b4d8a0804813dd68a4d10331742bf738ee6',0,'2021-08-13 21:35:01'),
	('c82b897fa1ca2e19ed3c9b12201969e76cbb3360d6b0b2ad5b53bd7c9bdc083fa0c431e9aef1e770','5477e8e11b8bf7b71033e7f86868db744079812756c3cb677fd4478bf48cba2d23bddd05d624c4a8',0,'2021-08-13 22:51:23'),
	('fc7927426d3799b822761791a1573010dc44dca08cc20dad09fc482113906f875dc970524cfb5ee7','8008e2b77fac9d3b1e195b3403336d914cb654960632a66169c8f5781dbccb70d656f0f11f4ab4d2',0,'2021-08-13 07:44:45');

/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `title`, `body`, `created_at`, `updated_at`)
VALUES
	(68,'Title nuevo','Descrip nuevo.','2020-05-13 01:05:53','2020-05-13 01:05:53'),
	(69,'Title nuevo','Descrip nuevo.','2020-05-13 01:06:01','2020-05-13 01:06:01'),
	(70,'Title nuevo','Descrip nuevo.','2020-05-13 01:38:24','2020-05-13 01:38:24');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `program`;

CREATE TABLE `program` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_program_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `program` WRITE;
/*!40000 ALTER TABLE `program` DISABLE KEYS */;

INSERT INTO `program` (`id`, `name`)
VALUES
	('9f996dec-988d-11ea-bb37-0242ac130002','DISEÑO GRÁFICO'),
	('9f9972d8-988d-11ea-bb37-0242ac130002','DISEÑO, COMUNICACIÓN Y NUEVOS MEDIOS '),
	('9f997404-988d-11ea-bb37-0242ac130002','EFICIENCIA ENERGÉTICA'),
	('9f9974ea-988d-11ea-bb37-0242ac130002','ELECTRÓNICA Y AUTOMATIZACIÓN'),
	('9f997684-988d-11ea-bb37-0242ac130002','ENERGÍA Y AMBIENTE'),
	('9f997742-988d-11ea-bb37-0242ac130002','ENSEÑANZA DE LENGUAS NATIVAS Y EXTRANJERAS'),
	('9f99780a-988d-11ea-bb37-0242ac130002','FÍSICA COMPUTACIONAL'),
	('9f997d28-988d-11ea-bb37-0242ac130002','FÍSICA DE LAS RADIACIONES'),
	('9f997e0e-988d-11ea-bb37-0242ac130002','INGENIERÍA DE PROCESOS INDUSTRIALES'),
	('9f997ed6-988d-11ea-bb37-0242ac130002','INGENIERÍA DE SOFTWARE'),
	('9f997f94-988d-11ea-bb37-0242ac130002','INGENIERÍA Y TECNOLOGÍA DE MATERIALES'),
	('9f998052-988d-11ea-bb37-0242ac130002','INNOVACIÓN EDUCATIVA'),
	('9f998106-988d-11ea-bb37-0242ac130002','INTELIGENCIA ARTIFICIAL'),
	('9f99853e-988d-11ea-bb37-0242ac130002','MANEJO Y APROVECHAMIENTO DE RECURSOS RENOVABLES'),
	('9f99861a-988d-11ea-bb37-0242ac130002','MODELADO Y SIMULACIÓN COMPUTACIONAL DE PROCESOS'),
	('9f9986d8-988d-11ea-bb37-0242ac130002','PROCESAMIENTO DIGITAL DE SEÑALES E IMÁGENES'),
	('9f998796-988d-11ea-bb37-0242ac130002','ROBÓTICA Y CONTROL'),
	('9f998854-988d-11ea-bb37-0242ac130002','SEGURIDAD DE SISTEMAS DE INFORMACIÓN'),
	('9f998ba6-988d-11ea-bb37-0242ac130002','SUSTENTABILIDAD Y NUEVAS TECNOLOGÍAS'),
	('9f998c82-988d-11ea-bb37-0242ac130002','TECNOLOGÍA EDUCATIVA'),
	('9f998d4a-988d-11ea-bb37-0242ac130002','TECNOLOGÍA VEHICULAR'),
	('9f998e08-988d-11ea-bb37-0242ac130002','TELECOMUNICACIONES Y REDES'),
	('d6324148-988e-11ea-bb37-0242ac130002','TELEFONÍA PARA EL DESARROLLO');

/*!40000 ALTER TABLE `program` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla program_has_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `program_has_groups`;

CREATE TABLE `program_has_groups` (
  `id_program` varchar(36) NOT NULL,
  `id_group` varchar(36) NOT NULL,
  PRIMARY KEY (`id_program`,`id_group`),
  KEY `fk_program_has_groups_groups1_idx` (`id_group`),
  KEY `fk_program_has_groups_program1_idx` (`id_program`),
  CONSTRAINT `fk_program_has_groups_groups1` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id_group`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_has_groups_program1` FOREIGN KEY (`id_program`) REFERENCES `program` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `program_has_groups` WRITE;
/*!40000 ALTER TABLE `program_has_groups` DISABLE KEYS */;

INSERT INTO `program_has_groups` (`id_program`, `id_group`)
VALUES
	('9f997404-988d-11ea-bb37-0242ac130002','6773020e-c95f-11ea-bdac-6003088c543c'),
	('9f998854-988d-11ea-bb37-0242ac130002','6773020e-c95f-11ea-bdac-6003088c543c'),
	('9f996dec-988d-11ea-bb37-0242ac130002','a9fe3ba6-98b9-11ea-bb37-0242ac130002'),
	('9f997684-988d-11ea-bb37-0242ac130002','a9fe3ba6-98b9-11ea-bb37-0242ac130002'),
	('9f9974ea-988d-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('9f9974ea-988d-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('9f997ed6-988d-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('9f997f94-988d-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('9f998052-988d-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('9f996dec-988d-11ea-bb37-0242ac130002','a9fe422c-98b9-11ea-bb37-0242ac130002'),
	('9f9974ea-988d-11ea-bb37-0242ac130002','a9fe4308-98b9-11ea-bb37-0242ac130002'),
	('9f99780a-988d-11ea-bb37-0242ac130002','a9fe4308-98b9-11ea-bb37-0242ac130002'),
	('9f9974ea-988d-11ea-bb37-0242ac130002','a9fe4524-98b9-11ea-bb37-0242ac130002'),
	('9f997404-988d-11ea-bb37-0242ac130002','a9fe4696-98b9-11ea-bb37-0242ac130002'),
	('9f997684-988d-11ea-bb37-0242ac130002','a9fe4696-98b9-11ea-bb37-0242ac130002'),
	('9f997684-988d-11ea-bb37-0242ac130002','a9fe4754-98b9-11ea-bb37-0242ac130002'),
	('9f997684-988d-11ea-bb37-0242ac130002','a9fe4808-98b9-11ea-bb37-0242ac130002'),
	('9f997404-988d-11ea-bb37-0242ac130002','a9fe48bc-98b9-11ea-bb37-0242ac130002'),
	('9f997404-988d-11ea-bb37-0242ac130002','a9fe497a-98b9-11ea-bb37-0242ac130002');

/*!40000 ALTER TABLE `program_has_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(500) NOT NULL DEFAULT '',
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `endDateReal` date DEFAULT NULL COMMENT 'Sera la fecha final en la que realmente finalizó',
  `location` varchar(300) DEFAULT NULL,
  `project_type_id` varchar(36) DEFAULT NULL,
  `research_type_id` varchar(36) DEFAULT NULL,
  `coverage_type_id` varchar(36) DEFAULT NULL,
  `program_id` varchar(36) DEFAULT NULL,
  `year` year(4) NOT NULL,
  `father` varchar(36) DEFAULT NULL,
  `id_group` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_project_projectType1_idx` (`project_type_id`),
  KEY `fk_project_researchType1_idx` (`research_type_id`),
  KEY `fk_project_project1_idx` (`father`),
  KEY `fk_project_cverageType_idx` (`coverage_type_id`),
  KEY `fk_project_group1_idx` (`id_group`),
  KEY `project_ibfk_1_idx` (`program_id`),
  CONSTRAINT `fk_project_cverageType` FOREIGN KEY (`coverage_type_id`) REFERENCES `coverage_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_project1` FOREIGN KEY (`father`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_projectType1` FOREIGN KEY (`project_type_id`) REFERENCES `project_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_researchType1` FOREIGN KEY (`research_type_id`) REFERENCES `research_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`id`, `code`, `name`, `startDate`, `endDate`, `endDateReal`, `location`, `project_type_id`, `research_type_id`, `coverage_type_id`, `program_id`, `year`, `father`, `id_group`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	('157b2eb8-c7b6-11ea-ba20-6003088c543c',NULL,'mi proyecto','2020-01-01','2020-12-30',NULL,'Riobamba','f9ae26da-9886-11ea-bb37-0242ac130002','39cfe0ee-986b-11ea-8571-820f047d9840','79a95c42-9882-11ea-bb37-0242ac130002','9f997e0e-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-07-16 17:45:44','2020-08-13 08:17:35',NULL),
	('157c4ccc-9880-11ea-bb37-0242ac130002','pro-alpa','Proyecto para la elaboración y evaluación del Modelo ALPA para la administración Central de la ESPOCH','2020-05-30','2020-12-30','2020-08-28','Riobamba','f9ae248c-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','79a95e54-9882-11ea-bb37-0242ac130002','9f996dec-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-04-23 00:04:36','2020-07-15 17:28:36',NULL),
	('157c4fba-9880-11ea-bb37-0242ac130002','pro-018','Software Iterativo','2020-02-05','2020-12-30',NULL,'Riobamba','f9ae248c-9886-11ea-bb37-0242ac130002','39cfe0ee-986b-11ea-8571-820f047d9840','79a95e54-9882-11ea-bb37-0242ac130002','9f9986d8-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-04-23 00:04:36','2020-07-15 17:28:33',NULL),
	('157c5122-9880-11ea-bb37-0242ac130002',NULL,'Proyecto Galápagos','2020-04-01','2020-04-30',NULL,'Riobamba','f9ae248c-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','79a95a9e-9882-11ea-bb37-0242ac130002','9f997684-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-04-26 23:33:07','2020-07-15 17:28:34',NULL),
	('157c5230-9880-11ea-bb37-0242ac130002',NULL,'Crecimiento de las plantaciones agrícolas en Lican Riobamba','2020-04-28','2020-12-30',NULL,'Riobamba','f9ae248c-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','79a95878-9882-11ea-bb37-0242ac130002','9f9974ea-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-04-27 23:32:22','2020-07-19 03:01:25',NULL),
	('157c5370-9880-11ea-bb37-0242ac130002',NULL,'Evaluación del impacto ambiental','2020-04-01','2020-04-30',NULL,'Cotopaxi','f9ae248c-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','79a95d14-9882-11ea-bb37-0242ac130002','9f997684-988d-11ea-bb37-0242ac130002','2021',NULL,NULL,'2020-04-28 09:09:23','2020-05-17 15:51:53',NULL),
	('157c5456-9880-11ea-bb37-0242ac130002',NULL,'Proyecto covid19','2020-01-01','2020-12-30','2020-05-13','Cc','f9ae248c-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','79a95a9e-9882-11ea-bb37-0242ac130002','9f9974ea-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-05-13 13:55:33','2020-05-17 15:52:04',NULL),
	('4844a62e-bbbf-11ea-a25c-6003088c543c',NULL,'Proyecto de aves','2020-07-01','2020-12-30',NULL,'Riobamba','f9ae248c-9886-11ea-bb37-0242ac130002','39cfe0ee-986b-11ea-8571-820f047d9840','79a95c42-9882-11ea-bb37-0242ac130002','9f998106-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-07-01 12:21:21','2020-07-01 12:21:21',NULL),
	('c71b11ec-c5ea-11ea-a211-6003088c543c',NULL,'Proyecto de REact','2020-07-14','2020-12-30',NULL,'Riobamba','f9ae248c-9886-11ea-bb37-0242ac130002','39cfe0ee-986b-11ea-8571-820f047d9840','79a95a9e-9882-11ea-bb37-0242ac130002','9f997ed6-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-07-14 10:57:54','2020-07-14 10:57:54',NULL),
	('d1ace12a-c5eb-11ea-87e2-6003088c543c',NULL,'Bendita tu luz','2020-07-14','2020-09-24',NULL,'Riobamba','f9ae248c-9886-11ea-bb37-0242ac130002','39cfe0ee-986b-11ea-8571-820f047d9840','79a95a9e-9882-11ea-bb37-0242ac130002','9f997684-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-07-14 11:05:21','2020-07-14 11:05:21',NULL),
	('d46ea82e-987f-11ea-bb37-0242ac130002','pro-02','Eccomerce de proyectos del COCA-CODO- SINCLAIR','2020-01-15','2020-12-17',NULL,'Riobamba31','f9ae26da-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','79a95a9e-9882-11ea-bb37-0242ac130002','9f99861a-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-04-23 00:04:36','2020-05-17 15:49:40',NULL),
	('f7c5375a-c7fc-11ea-8d20-6003088c543c',NULL,'kkkkkkk','2020-07-14','2020-11-24',NULL,'Riobamba','f9ae248c-9886-11ea-bb37-0242ac130002','39cfe0ee-986b-11ea-8571-820f047d9840','79a95c42-9882-11ea-bb37-0242ac130002','9f9986d8-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-07-17 02:13:09','2020-07-17 02:13:09',NULL);

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla project_has_dependency
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_dependency`;

CREATE TABLE `project_has_dependency` (
  `id_project` varchar(36) NOT NULL,
  `id_dependency` varchar(36) NOT NULL,
  `date_projectHasDependency` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type_Dependency` varchar(45) DEFAULT NULL COMMENT 'la dependencia en este proyecto es PRINCIPAL o MIEMBRO',
  PRIMARY KEY (`id_project`,`id_dependency`),
  KEY `fk_project_has_managementArea_project1_idx` (`id_project`),
  KEY `fk_project_has_managementArea_managementArea1_idx` (`id_dependency`),
  CONSTRAINT `fk_project_has_managementArea_managementArea1` FOREIGN KEY (`id_dependency`) REFERENCES `dependency` (`id_dependency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_managementArea_project1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla project_has_researchline
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_researchline`;

CREATE TABLE `project_has_researchline` (
  `id_project` varchar(36) NOT NULL,
  `id_researchLine` varchar(36) NOT NULL,
  PRIMARY KEY (`id_project`,`id_researchLine`),
  KEY `fk_project_has_researchline_researchline1_idx` (`id_researchLine`),
  KEY `fk_project_has_researchline_project1_idx` (`id_project`),
  CONSTRAINT `fk_project_has_researchline_project1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_researchline_researchline1` FOREIGN KEY (`id_researchLine`) REFERENCES `researchline` (`id_researchLine`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla project_has_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_users`;

CREATE TABLE `project_has_users` (
  `project_id` varchar(36) NOT NULL DEFAULT '',
  `user_id` varchar(36) NOT NULL DEFAULT '',
  `role_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `fk_project_has_users_users1_idx` (`user_id`),
  KEY `fk_project_has_users_project1_idx` (`project_id`),
  KEY `fk_project_has_users_role1_idx` (`role_id`),
  CONSTRAINT `fk_project_has_users_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_users_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_users_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project_has_users` WRITE;
/*!40000 ALTER TABLE `project_has_users` DISABLE KEYS */;

INSERT INTO `project_has_users` (`project_id`, `user_id`, `role_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	('157c4ccc-9880-11ea-bb37-0242ac130002','17ffa6c4-98d0-11ea-bb37-0242ac130002',5,NULL,NULL,NULL),
	('157c4ccc-9880-11ea-bb37-0242ac130002','f87e7654-98cf-11ea-bb37-0242ac130002',NULL,NULL,NULL,NULL),
	('157c5122-9880-11ea-bb37-0242ac130002','17ffa6c4-98d0-11ea-bb37-0242ac130002',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `project_has_users` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla project_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_type`;

CREATE TABLE `project_type` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project_type` WRITE;
/*!40000 ALTER TABLE `project_type` DISABLE KEYS */;

INSERT INTO `project_type` (`id`, `name`)
VALUES
	('f9ae248c-9886-11ea-bb37-0242ac130002','INVESTIGACION'),
	('f9ae26da-9886-11ea-bb37-0242ac130002','VINCULACION');

/*!40000 ALTER TABLE `project_type` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla requeriment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `requeriment`;

CREATE TABLE `requeriment` (
  `id_requeriment` int(11) NOT NULL AUTO_INCREMENT,
  `name_requeriment` varchar(255) NOT NULL,
  `id_activity` varchar(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_requeriment`),
  KEY `fk_requeriment_activity1_idx` (`id_activity`),
  CONSTRAINT `fk_requeriment_activity1` FOREIGN KEY (`id_activity`) REFERENCES `activity` (`id_activity`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla researchcenter
# ------------------------------------------------------------

DROP TABLE IF EXISTS `researchcenter`;

CREATE TABLE `researchcenter` (
  `id_researchCenter` int(11) NOT NULL AUTO_INCREMENT,
  `name_researchCenter` varchar(255) NOT NULL,
  PRIMARY KEY (`id_researchCenter`),
  UNIQUE KEY `name_researchGroup_UNIQUE` (`name_researchCenter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla researchline
# ------------------------------------------------------------

DROP TABLE IF EXISTS `researchline`;

CREATE TABLE `researchline` (
  `id_researchLine` varchar(36) NOT NULL,
  `name_researchLine` varchar(255) NOT NULL,
  PRIMARY KEY (`id_researchLine`),
  UNIQUE KEY `name_researchLine_UNIQUE` (`name_researchLine`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `researchline` WRITE;
/*!40000 ALTER TABLE `researchline` DISABLE KEYS */;

INSERT INTO `researchline` (`id_researchLine`, `name_researchLine`)
VALUES
	('ea46431e-98bb-11ea-bb37-0242ac130002','ADMINISTRACIÓN Y ECONOMÍA POPULAR'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','ARTE CULTURA Y PATRIMONIO'),
	('ea4645f8-98bb-11ea-bb37-0242ac130002','CIENCIAS BÁSICAS Y APLICADAS'),
	('ea464800-98bb-11ea-bb37-0242ac130002','ENERGÍAS RENOVABLES Y PROTECCIÓN AMBIENTAL'),
	('ea4648d2-98bb-11ea-bb37-0242ac130002','GESTIÓN Y MANEJO SUSTENTABLES DE LOS RECURSOS NATURALES'),
	('ea46499a-98bb-11ea-bb37-0242ac130002','PROCESOS TECNOLÓGICOS ARTESANALES E INDUSTRIALES'),
	('ea464a58-98bb-11ea-bb37-0242ac130002','SALUD Y NUTRICIÓN'),
	('ea464b16-98bb-11ea-bb37-0242ac130002','TECNOLOGÍAS DE LA INFORMACIÓN, COMUNICACIÓN');

/*!40000 ALTER TABLE `researchline` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla researchline_has_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `researchline_has_groups`;

CREATE TABLE `researchline_has_groups` (
  `id_researchLine` varchar(36) NOT NULL,
  `id_group` varchar(36) NOT NULL,
  PRIMARY KEY (`id_researchLine`,`id_group`),
  KEY `fk_researchline_has_groups_groups1_idx` (`id_group`),
  KEY `fk_researchline_has_groups_researchline1_idx` (`id_researchLine`),
  CONSTRAINT `fk_researchline_has_groups_groups1` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id_group`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_researchline_has_groups_researchline1` FOREIGN KEY (`id_researchLine`) REFERENCES `researchline` (`id_researchLine`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `researchline_has_groups` WRITE;
/*!40000 ALTER TABLE `researchline_has_groups` DISABLE KEYS */;

INSERT INTO `researchline_has_groups` (`id_researchLine`, `id_group`)
VALUES
	('ea46431e-98bb-11ea-bb37-0242ac130002','6773020e-c95f-11ea-bdac-6003088c543c'),
	('ea4645f8-98bb-11ea-bb37-0242ac130002','6773020e-c95f-11ea-bdac-6003088c543c'),
	('ea464800-98bb-11ea-bb37-0242ac130002','6773020e-c95f-11ea-bdac-6003088c543c'),
	('ea46431e-98bb-11ea-bb37-0242ac130002','a9fe3ba6-98b9-11ea-bb37-0242ac130002'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','a9fe3ba6-98b9-11ea-bb37-0242ac130002'),
	('ea4645f8-98bb-11ea-bb37-0242ac130002','a9fe3ba6-98b9-11ea-bb37-0242ac130002'),
	('ea46431e-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea4645f8-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea4648d2-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea46499a-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea464a58-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea464b16-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea4645f8-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea4648d2-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea46499a-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea464a58-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe422c-98b9-11ea-bb37-0242ac130002'),
	('ea4648d2-98bb-11ea-bb37-0242ac130002','a9fe4308-98b9-11ea-bb37-0242ac130002'),
	('ea46499a-98bb-11ea-bb37-0242ac130002','a9fe4308-98b9-11ea-bb37-0242ac130002'),
	('ea46431e-98bb-11ea-bb37-0242ac130002','a9fe4524-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe4696-98b9-11ea-bb37-0242ac130002'),
	('ea464a58-98bb-11ea-bb37-0242ac130002','a9fe4696-98b9-11ea-bb37-0242ac130002'),
	('ea46431e-98bb-11ea-bb37-0242ac130002','a9fe4754-98b9-11ea-bb37-0242ac130002'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','a9fe4754-98b9-11ea-bb37-0242ac130002'),
	('ea46499a-98bb-11ea-bb37-0242ac130002','a9fe4808-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe48bc-98b9-11ea-bb37-0242ac130002'),
	('ea464b16-98bb-11ea-bb37-0242ac130002','a9fe48bc-98b9-11ea-bb37-0242ac130002'),
	('ea464a58-98bb-11ea-bb37-0242ac130002','a9fe497a-98b9-11ea-bb37-0242ac130002');

/*!40000 ALTER TABLE `researchline_has_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla research_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `research_type`;

CREATE TABLE `research_type` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `research_type` WRITE;
/*!40000 ALTER TABLE `research_type` DISABLE KEYS */;

INSERT INTO `research_type` (`id`, `name`)
VALUES
	('16429ac6-9867-11ea-afcb-820f047d9840','INVESTIGACION CIENTIFICA'),
	('39cfe0ee-986b-11ea-8571-820f047d9840','DESARROLLO TECNOLOGICO'),
	('7f212b12-986b-11ea-8d7c-820f047d9840','INNOVACION TECNOLOGICA');

/*!40000 ALTER TABLE `research_type` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_typeUser_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;

INSERT INTO `role` (`id`, `name`)
VALUES
	(1,'CIMOGSYS'),
	(3,'DECANO'),
	(4,'DIRECTOR IDI'),
	(5,'INVESTIGADOR'),
	(6,'INVESTIGADOR RESPONSABLE'),
	(8,'RECTOR'),
	(2,'VICERRECTOR');

/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` varchar(36) CHARACTER SET utf8 NOT NULL,
  `identification_card` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `identification_card`, `name`, `lastname`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	('00209612-98d0-11ea-bb37-0242ac130002','0603997990','Fausto','Cevallos','fausto.cevallos@outlook.com',NULL,'$2y$10$L51NDaiYsbk3JAIKik9Qeehe7L1UDePXFPDpjXQ./tBeX7qcT86fq',NULL,'2020-04-23 00:23:01','2020-05-14 23:10:31',NULL),
	('0807f14a-98d0-11ea-bb37-0242ac130002','0987654321','Fernando','Valladares','fernando.valladares@espoch.edu.ec',NULL,NULL,NULL,'2020-04-30 14:23:34','2020-04-30 18:12:12',NULL),
	('17ffa6c4-98d0-11ea-bb37-0242ac130002','0603997305','Cristian','Guamán','cristian.guaman@espoch.edu.ec',NULL,'$2y$10$9q36OC7acsM2QFS6JmDe0.fWmWUxY/t4Yz8SfSBaIe/uzJrFNPJK2',NULL,'2020-05-16 11:53:04','2020-07-19 12:24:42',NULL),
	('2a078944-c7b6-11ea-9c02-6003088c543c','0603997309','Daniela','Guzman','daniela.guzman@espoch.edu.ec',NULL,'$2y$10$xX5eYceWPuhIDqkP7LuuXOnG/EdMPeQzL9uQy/0T60NpDkIbs98uy',NULL,'2020-07-16 17:46:19','2020-07-16 17:48:00',NULL),
	('7a7e6c8a-ca36-11ea-a2b6-6003088c543c','0601098342','María Clara','Bermeo','mari.bermeo@espoch.edu.ec',NULL,'$2y$10$sJMtxiytPGMNTyjGl6ERO.GsyVJ5Sb2v8EKXJv30u4bQNFt2bpHNS',NULL,'2020-07-19 22:09:52','2020-07-19 22:09:52',NULL),
	('d967b8be-ca36-11ea-a6f3-6003088c543c','0601098343','Sara','Romero','sara.romero@hotmail.com',NULL,'$2y$10$JO0pkWRJ3INBzMwN5XydZeDOucseM59a.AYk2aRZAYvNqW/0G1KWS',NULL,'2020-07-19 22:12:31','2020-07-19 22:12:31',NULL),
	('dc58c64c-c99d-11ea-908e-6003088c543c','3333333339','Usuario','Prueba','usuario0@prueba.com',NULL,'$2y$10$jLd2VOV5Zd88re2h7jzhOOk47AfHUwo5j1Jz0bVENgLAo/Y2nYw7C',NULL,'2020-07-19 03:57:23','2020-07-19 20:08:04',NULL),
	('e22b3612-98cf-11ea-bb37-0242ac130002','0603997300','Cristian','Guamán','cristian.guaman@icloud.com',NULL,'123456',NULL,NULL,'2020-05-18 02:09:16',NULL),
	('f87e7654-98cf-11ea-bb37-0242ac130002','0603189655','Giovanny','Alarcón','geovanny_alarcon@cimogsys.com',NULL,'$2y$10$YQra1aBdaosJWXHnzrfrPu6o.ju8hrB6ha2HmSOm4VJnRE4Nqcx9u',NULL,'2020-04-23 00:22:40','2020-05-14 23:10:42',NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla users_has_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_has_role`;

CREATE TABLE `users_has_role` (
  `user_id` varchar(36) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_users_has_role_role1_idx` (`role_id`),
  KEY `fk_users_has_role_users1_idx` (`user_id`),
  CONSTRAINT `fk_users_has_role_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_role_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users_has_role` WRITE;
/*!40000 ALTER TABLE `users_has_role` DISABLE KEYS */;

INSERT INTO `users_has_role` (`user_id`, `role_id`)
VALUES
	('17ffa6c4-98d0-11ea-bb37-0242ac130002',1),
	('17ffa6c4-98d0-11ea-bb37-0242ac130002',2),
	('17ffa6c4-98d0-11ea-bb37-0242ac130002',3),
	('17ffa6c4-98d0-11ea-bb37-0242ac130002',4);

/*!40000 ALTER TABLE `users_has_role` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
