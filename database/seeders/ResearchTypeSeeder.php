<?php

namespace Database\Seeders;

use App\Models\ResearchType;
use Illuminate\Database\Seeder;

class ResearchTypeSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        ResearchType::Truncate();

        ResearchType::create(['name'=> 'INVESTIGACIÓN CIENTÍFICA']);
        ResearchType::create(['name'=> 'DESARROLLO TECNOLÓGICO']);
        ResearchType::create(['name'=> 'INNOVACIÓN TECNOLÓGICA']);

    }
}
