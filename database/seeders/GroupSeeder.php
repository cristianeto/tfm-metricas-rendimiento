<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\GroupType;
use App\Models\ResearchLine;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        Group::truncate();
        GroupType::truncate();
        ResearchLine::truncate();

        GroupType::create(['name' => 'INVESTIGACIÓN']);
        GroupType::create(['name' => 'VINCULACIÓN']);

        ResearchLine::create(['name' => 'ADMINISTRACIÓN Y ECONOMÍA POPULAR']);
        ResearchLine::create(['name' => 'ARTE CULTURA Y PATRIMONIO']);
        ResearchLine::create(['name' => 'CIENCIAS BÁSICAS Y APLICADAS']);
        ResearchLine::create(['name' => 'ENERGÍAS RENOVABLES Y PROTECCIÓN AMBIENTAL']);
        ResearchLine::create(['name' => 'GESTIÓN Y MANEJO SUSTENTABLE DE LOS RECURSOS NATURALES']);
        ResearchLine::create(['name' => 'PROCESOS TECNOLÓGICOS ARTESANALES E INDUSTRIALES']);
        ResearchLine::create(['name' => 'SALUD Y NUTRICIÓN']);
        ResearchLine::create(['name' => 'TECNOLOGÍAS DE LA INFORMACIÓN, COMUNICACIÓN']);

        Group::create(['code' => 'FIEGI-010', 'name' => 'GRUPO DE INVESTIGACIÓN E INTERACCIÓN EN LAS TECNOLOGÍAS DE LA COMUNICACIÓN', 'acronym' => 'IITC', 'dependency_id' => 2, 'group_type_id' => 1]);




    }
}
