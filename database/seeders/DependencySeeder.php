<?php

namespace Database\Seeders;

use App\Models\Dependency;
use App\Models\DependencyType;
use Illuminate\Database\Seeder;

class DependencySeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DependencyType::Truncate();
        Dependency::Truncate();

        DependencyType::create(['name'=>'CARRERA']);
        DependencyType::create(['name'=>'EXTERNA']);
        DependencyType::create(['name'=>'FACULTAD']);
        DependencyType::create(['name'=>'INSTITUCIONAL']);
        DependencyType::create(['name'=>'UNIDAD ACADÉMICA']);
        DependencyType::create(['name'=>'UNIDAD ADMINISTRATIVA']);

        Dependency::create(['name'=>'ESCUELA SUPERIOR POLITÉCNICA DE CHIMBORAZO', 'acronym' => 'ESPOCH', 'dependency_type_id' => 4, 'email' => 'info@espoch.edu.ec']);
        Dependency::create(['name'=> 'FACULTAD DE INFORMÁTICA Y ELECTRÓNICA', 'acronym' => 'FIE', 'dependency_type_id' => 3, 'email' => 'fie@espoch.edu.ec']);
        Dependency::create(['name'=> 'FACULTAD ADMINISTRACIÓN DE EMPRESAS', 'acronym' => 'FADE', 'dependency_type_id' => 3, 'email' => 'fade@espoch.edu.ec']);

    }
}
