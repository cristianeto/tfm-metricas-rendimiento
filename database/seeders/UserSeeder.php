<?php

namespace Database\Seeders;

use App\Models\User;
use DB;
use Hash;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        Permission::Truncate();
        Role::Truncate();
        User::Truncate();
        DB::table('oauth_clients')->truncate();

        DB::table('oauth_clients')->insert([
            'id'=>1,
            'user_id'=>null,
            'name'=>'Spirit7',
            'secret'=>'HA9whWTof6rD2IipNKAiwzkrhEJ2gGB5SZLooflq',
            'provider' =>'users',
            'redirect'=> 'http://localhost',
            'personal_access_client'=>0,
            'password_client'=> 1,
            'revoked'=>0
        ]);

        $cimogsysRole = Role::create(['name' => 'Cimogsys', 'display_name' => 'Cimogsys']);
        $rectorRole = Role::create(['name' => 'Rector', 'display_name' => 'Rector']);
        $vicerrectorrectorRole = Role::create(['name' => 'Vicerrector', 'display_name' => 'Vicerrector']);
        $directorIDIRole = Role::create(['name' => 'Director IDI', 'display_name' => 'Director IDI']);
        $decanoRole = Role::create(['name' => 'Decano', 'display_name' => 'Decano']);
        $subdecanoRole = Role::create(['name' => 'Subdecano', 'display_name' => 'Subdecano']);
        $investigadorRole = Role::create(['name' => 'Investigador', 'display_name' => 'Investigador']);
        $adminGroupsRole = Role::create(['name' => 'Admin groups', 'display_name' => 'Admin Grupos']);


        $viewProjectsPermission = Permission::create(['name' => 'View projects', 'display_name' => 'Ver proyectos']);
        $createProjectsPermission = Permission::create(['name' => 'Create projects', 'display_name' => 'Crear proyectos']);
        $updateProjectsPermission = Permission::create(['name' => 'Update projects', 'display_name' => 'Actualizar proyectos']);
        $deleteProjectsPermission = Permission::create(['name' => 'Delete projects', 'display_name' => 'Eliminar proyectos']);

        $viewUsersPermission = Permission::create(['name' => 'View users', 'display_name' => 'Ver usuarios']);
        $createUsersPermission = Permission::create(['name' => 'Create users', 'display_name' => 'Crear usuarios']);
        $updateUsersPermission = Permission::create(['name' => 'Update users', 'display_name' => 'Actualizar usuarios']);
        $deleteUsersPermission = Permission::create(['name' => 'Delete users', 'display_name' => 'Eliminar usuarios']);

        $viewRolesPermission = Permission::create(['name' => 'View roles', 'display_name' => 'Ver roles']);
        $createRolesPermission = Permission::create(['name' => 'Create roles', 'display_name' => 'Crear roles']);
        $updateRolesPermission = Permission::create(['name' => 'Update roles', 'display_name' => 'Actualizar roles']);
        $deleteRolesPermission = Permission::create(['name' => 'Delete roles', 'display_name' => 'Eliminar roles']);

        $viewPermissionsPermission = Permission::create(['name' => 'View permissions', 'display_name' => 'Ver permisos']);
        $updatePermissionsPermission = Permission::create(['name' => 'Update permissions', 'display_name' => 'Actualizar permisos']);

        $viewProjectMembersPermission = Permission::create(['name' => 'View project members', 'display_name' => 'Ver miembros de proyecto']);
        $createProjectMembersPermission = Permission::create(['name' => 'Create project members', 'display_name' => 'Crear miembros de proyecto']);
        $updateProjectMembersPermission = Permission::create(['name' => 'Update project members', 'display_name' => 'Actualizar miembros de proyecto']);
        $deleteProjectMembersPermission = Permission::create(['name' => 'Delete project members', 'display_name' => 'Eliminar miembros de proyecto']);

        $viewGroupsPermission = Permission::create(['name' => 'View groups', 'display_name' => 'Ver grupos']);
        $createGroupsPermission = Permission::create(['name' => 'Create groups', 'display_name' => 'Crear grupos']);
        $updateGroupsPermission = Permission::create(['name' => 'Update groups', 'display_name' => 'Actualizar grupos']);
        $deleteGroupsPermission = Permission::create(['name' => 'Delete groups', 'display_name' => 'Eliminar grupos']);

        $investigadorRole->givePermissionTo($createProjectsPermission);
        $investigadorRole->givePermissionTo($viewUsersPermission);
        $investigadorRole->givePermissionTo($viewRolesPermission);
        $investigadorRole->givePermissionTo($viewPermissionsPermission);

        $cimogsys = new User;
        $cimogsys->id = '1d1d9a0a-e2b6-11ea-9775-6003088c543c';
        $cimogsys->identification_card = '0603997305';
        $cimogsys->name = 'Cristian';
        $cimogsys->lastname = 'Guamán';
        $cimogsys->email = 'cristian.guaman@espoch.edu.ec';
        $cimogsys->sex = 'Hombre';
        $cimogsys->password = 'spirit0603997305';
        $cimogsys->save();

        $cimogsys->assignRole($cimogsysRole);


        /*$investigador = new User;
        $investigador->id = '1d29c8e8-e2b6-11ea-abc9-6003088c543c';
        $investigador->identification_card = '0604697425';
        $investigador->name = 'Naizly';
        $investigador->lastname = 'Silva';
        $investigador->email = 'naizly.silva@espoch.edu.ec';
        $investigador->sex = 'Mujer';
        $investigador->password = Hash::make('spirit0604697425');
        $investigador->save();

        $investigador->assignRole($investigadorRole);*/
    }
}
