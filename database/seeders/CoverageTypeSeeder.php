<?php

namespace Database\Seeders;

use App\Models\CoverageType;
use Illuminate\Database\Seeder;

class CoverageTypeSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        CoverageType::Truncate();

        CoverageType::create(['name'=>'INTERNACIONAL']);
        CoverageType::create(['name'=>'NACIONAL']);
        CoverageType::create(['name'=>'REGIONAL']);
        CoverageType::create(['name'=>'PROVINCIAL']);
        CoverageType::create(['name'=>'CANTONAL']);
        CoverageType::create(['name'=>'PARROQUIAL']);
    }
}
