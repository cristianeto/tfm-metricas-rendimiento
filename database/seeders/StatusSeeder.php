<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        Status::Truncate();

        Status::create(['name' => 'Proyecto creado']);
        Status::create(['name' => 'Proyecto actualizado']);
        Status::create(['name' => 'Enviado a decanato']);
        Status::create(['name' => 'Aprobado por decanato']);
        Status::create(['name' => 'Rechazado por decanato']);
        Status::create(['name' => 'Enviado a director IDI']);
        Status::create(['name' => 'Aprobado por director IDI']);
        Status::create(['name' => 'Rechazado por director IDI']);
        Status::create(['name' => 'Requerimiento creado']);
        Status::create(['name' => 'Actividad creada']);
        Status::create(['name' => 'Componente creado']);
    }
}
