<?php

namespace Database\Seeders;

use App\Models\Program;
use Illuminate\Database\Seeder;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        Program::Truncate();

        Program::create(['name'=>'DISEÑO GRÁFICO']);
        Program::create(['name'=>'DISEÑO, COMUNICACIÓN Y NUEVOS MEDIOS']);
        Program::create(['name'=>'EFICIENCIA ENERGÉTICA']);
        Program::create(['name'=>'ELECTRÓNICA Y AUTOMATIZACIÓN']);
        Program::create(['name'=>'ENERGÍA Y AMBIENTE']);
        Program::create(['name'=>'ENSEÑANZA DE LENGUAS NATIVAS Y EXTRANJERAS']);
        Program::create(['name'=>'FÍSICA COMPUTACIONAL']);
        Program::create(['name'=>'FÍSICA DE LAS RADIACIONES']);
        Program::create(['name'=>'INGENIERÍA DE PROCESOS INDUSTRIALES']);
        Program::create(['name'=>'INGENIERÍA DE SOFTWARE']);
        Program::create(['name'=>'INGENIERÍA Y TECNOLOGÍA DE MATERIALES']);
        Program::create(['name'=>'INNOVACIÓN EDUCATIVA']);
        Program::create(['name'=>'INTELIGENCIA ARTIFICIAL']);
        Program::create(['name'=>'MANEJO Y APROVECHAMIENTO DE RECURSOS RENOVABLES']);
        Program::create(['name'=>'MODELADO Y SIMULACIÓN COMPUTACIONAL DE PROCESOS']);
        Program::create(['name'=>'PROCESAMIENTO DIGITAL DE SEÑALES E IMÁGENES']);
        Program::create(['name'=>'ROBÓTICA Y CONTROL']);
        Program::create(['name'=>'SEGURIDAD DE SISTEMAS DE INFORMACIÓN']);
        Program::create(['name'=>'SUSTENTABILIDAD Y NUEVAS TECNOLOGÍAS']);
        Program::create(['name'=>'TECNOLOGÍA EDUCATIVA']);
        Program::create(['name'=>'TECNOLOGÍA VEHICULAR']);
        Program::create(['name'=>'TELECOMUNICACIONES Y REDES']);
        Program::create(['name'=>'TELEFONÍA PARA EL DESARROLLO']);
    }
}
