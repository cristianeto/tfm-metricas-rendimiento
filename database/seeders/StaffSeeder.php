<?php

namespace Database\Seeders;

use App\Models\Staff;
use Illuminate\Database\Seeder;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        Staff::Truncate();

        Staff::create(['name' => 'Investigador responsable', 'display_name' => 'Investigador responsable']);
        Staff::create(['name' => 'Investigador miembro', 'display_name' => 'Investigador miembro']);
        Staff::create(['name' => 'Técnico', 'display_name' => 'Técnico']);
        Staff::create(['name' => 'Administrativo', 'display_name' => 'Administrativo']);
        Staff::create(['name' => 'Tesista', 'display_name' => 'Tesista']);
        Staff::create(['name' => 'Practicante', 'display_name' => 'Practicante']);
    }
}
