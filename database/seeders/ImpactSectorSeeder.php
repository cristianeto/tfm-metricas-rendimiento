<?php

namespace Database\Seeders;

use App\Models\ImpactSector;
use Illuminate\Database\Seeder;

class ImpactSectorSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        ImpactSector::truncate();
        ImpactSector::factory()->create(['name' => 'DESARROLLO HUMANO Y SOCIAL']);
        ImpactSector::factory()->create(['name' => 'RECURSOS NATURALES']);
        ImpactSector::factory()->create(['name' => 'FOMENTO AGROPECUARIO Y DESARR PRODUCTIVO']);
        ImpactSector::factory()->create(['name' => 'ENERGÍA']);
        ImpactSector::factory()->create(['name' => 'BIODIVERSIDAD Y AMBIENTE']);
        ImpactSector::factory()->create(['name' => 'TECNOLOGÍA DE LA INFOMRACIÓN Y COMUNICACIÓN ']);
    }
}
