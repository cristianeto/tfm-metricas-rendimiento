<?php

namespace Database\Seeders;

use App\Models\ProjectType;
use Illuminate\Database\Seeder;

class ProjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        ProjectType::Truncate();

        ProjectType::create(['name'=>'INVESTIGACIÓN']);
        ProjectType::create(['name'=>'VINCULACIÓN']);
    }
}
