<?php

namespace Database\Seeders;

use App\Models\Component;
use Carbon\Carbon;
use App\Models\Project;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        Component::truncate();
        Project::Truncate();

        $miPrimerProyecto = new Project;
        $miPrimerProyecto->name = 'Mi primer proyecto';
        $miPrimerProyecto->slug = Str::slug(Str::limit($miPrimerProyecto->name,  60));
        $miPrimerProyecto->year = Carbon::now()->year;
        $miPrimerProyecto->save();

        $miPrimerProyecto->users()->attach('1d1d9a0a-e2b6-11ea-9775-6003088c543c', ['staff_id' => 1]);
        $miPrimerProyecto->statuses()->attach(1, ['user_id'=>'1d1d9a0a-e2b6-11ea-9775-6003088c543c']);

        $miSegundoProyecto = new Project;
        $miSegundoProyecto->name = 'Mi segundo proyecto';
        $miSegundoProyecto->slug = Str::slug(Str::limit($miSegundoProyecto->name,  60));
        $miSegundoProyecto->year = Carbon::now()->year;
        $miSegundoProyecto->save();

        $miSegundoProyecto->users()->attach('1d1d9a0a-e2b6-11ea-9775-6003088c543c', ['staff_id' => 1]);
        $miSegundoProyecto->statuses()->attach(1, ['user_id'=>'1d1d9a0a-e2b6-11ea-9775-6003088c543c']);

        /*$miTercerProyecto = new Project;
        $miTercerProyecto->name = 'Mi tercer proyecto';
        $miTercerProyecto->slug = Str::slug(Str::limit($miTercerProyecto->name,  60));
        $miTercerProyecto->year = Carbon::now()->year;
        $miTercerProyecto->save();

        $miTercerProyecto->users()->attach('1d29c8e8-e2b6-11ea-abc9-6003088c543c', ['staff_id' => 1]);
        $miTercerProyecto->statuses()->attach(1, ['user_id'=>'1d29c8e8-e2b6-11ea-abc9-6003088c543c']);

        $miCuartoProyecto = new Project;
        $miCuartoProyecto->name = 'Mi Cuarto proyecto';
        $miCuartoProyecto->slug = Str::slug(Str::limit($miCuartoProyecto->name,  60));
        $miCuartoProyecto->year = Carbon::now()->year;
        $miCuartoProyecto->save();

        $miCuartoProyecto->users()->attach('1d29c8e8-e2b6-11ea-abc9-6003088c543c', ['staff_id' => 1]);
        $miCuartoProyecto->statuses()->attach(1, ['user_id'=>'1d29c8e8-e2b6-11ea-abc9-6003088c543c']);*/

        $primerComponente = new Component;
        $primerComponente->name = "1er Componente";
        $primerComponente->project_id = $miPrimerProyecto->id;
        $primerComponente->save();


    }
}
