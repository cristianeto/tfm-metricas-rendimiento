<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(ProjectTypeSeeder::class);
        $this->call(CoverageTypeSeeder::class);
        $this->call(ResearchTypeSeeder::class);
        $this->call(ProgramSeeder::class);

        $this->call(StaffSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ImpactSectorSeeder::class);
        $this->call(ProjectSeeder::class);
        $this->call(StatusSeeder::class);

        $this->call(DependencySeeder::class);
        $this->call(GroupSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
