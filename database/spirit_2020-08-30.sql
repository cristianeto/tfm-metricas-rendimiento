# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.31)
# Base de datos: spirit
# Tiempo de Generación: 2020-08-30 14:39:51 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla activity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `id_activity` varchar(36) NOT NULL,
  `description_activity` varchar(255) NOT NULL,
  `id_component` varchar(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_activity`),
  KEY `fk_activity_component1_idx` (`id_component`),
  CONSTRAINT `fk_activity_component1` FOREIGN KEY (`id_component`) REFERENCES `component` (`id_component`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla component
# ------------------------------------------------------------

DROP TABLE IF EXISTS `component`;

CREATE TABLE `component` (
  `id_component` varchar(36) NOT NULL,
  `name_component` varchar(36) NOT NULL,
  `id_project` varchar(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_component`),
  KEY `fk_component_project_idx` (`id_project`),
  CONSTRAINT `fk_component_project` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla coverage_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coverage_type`;

CREATE TABLE `coverage_type` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `coverage_type` WRITE;
/*!40000 ALTER TABLE `coverage_type` DISABLE KEYS */;

INSERT INTO `coverage_type` (`id`, `name`)
VALUES
	('79a95878-9882-11ea-bb37-0242ac130002','INTERNACIONAL'),
	('79a95a9e-9882-11ea-bb37-0242ac130002','NACIONAL'),
	('79a95c42-9882-11ea-bb37-0242ac130002','REGIONAL'),
	('79a95d14-9882-11ea-bb37-0242ac130002','PROVINCIAL'),
	('79a95e54-9882-11ea-bb37-0242ac130002','CANTONAL'),
	('79a95f1c-9882-11ea-bb37-0242ac130002','PARROQUIAL');

/*!40000 ALTER TABLE `coverage_type` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla dependency
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dependency`;

CREATE TABLE `dependency` (
  `id_dependency` varchar(36) NOT NULL,
  `name_dependency` varchar(500) NOT NULL,
  `acronym_dependency` varchar(10) DEFAULT NULL,
  `contact_dependency` varchar(150) DEFAULT NULL,
  `city_dependency` varchar(100) DEFAULT NULL,
  `email_dependency` varchar(100) DEFAULT NULL,
  `web_dependency` varchar(100) DEFAULT NULL,
  `phone_dependency` varchar(100) DEFAULT NULL,
  `participationType_dependency` text,
  `id_dependencyType` varchar(36) DEFAULT NULL,
  `father_dependency` varchar(36) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_dependency`),
  KEY `fk_dependency_dependency1_idx` (`father_dependency`),
  KEY `fk_dependency_dependencyType_idx` (`id_dependencyType`),
  CONSTRAINT `fk_dependency_dependency1` FOREIGN KEY (`father_dependency`) REFERENCES `dependency` (`id_dependency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dependency_dependencyType` FOREIGN KEY (`id_dependencyType`) REFERENCES `dependencytype` (`id_dependencyType`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dependency` WRITE;
/*!40000 ALTER TABLE `dependency` DISABLE KEYS */;

INSERT INTO `dependency` (`id_dependency`, `name_dependency`, `acronym_dependency`, `contact_dependency`, `city_dependency`, `email_dependency`, `web_dependency`, `phone_dependency`, `participationType_dependency`, `id_dependencyType`, `father_dependency`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	('a92c8ffa-9893-11ea-bb37-0242ac130002','ESCUELA SUPERIOR POLITECNICA DE CHIMBORAZO','ESPOCH','Byron Vaca','Riobamba','byron.vaca@espoch.edu.ec','http://cimogsys.espoch.edu.ec','0998220818 ext 109','as','9d692040-9896-11ea-bb37-0242ac130002',NULL,NULL,'2020-04-30 18:40:31',NULL),
	('a92c9220-9893-11ea-bb37-0242ac130002','FACULTAD DE INFORMÁTICA Y ELECTRÓNICA','FIE',NULL,NULL,'fie@espoch.edu.ec',NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,'2020-04-26 07:41:17',NULL),
	('a92c9342-9893-11ea-bb37-0242ac130002','FACULTAD DE ADMINISTRACIÓN DE EMPRESAS','FADE',NULL,NULL,'fade@espoch.edu.ec',NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,'2020-04-26 07:45:41',NULL),
	('a92c9428-9893-11ea-bb37-0242ac130002','FACULTAD DE MECÁNICA','FM',NULL,'Riobamba','mecanica@espoch.edu.ec',NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,'2020-04-26 10:53:00',NULL),
	('a92c950e-9893-11ea-bb37-0242ac130002','FACULTAD DE RECURSOS NATURALES','FRN',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c95e0-9893-11ea-bb37-0242ac130002','FACULTAD DE SALUD PÚBLICA','FSP',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c96a8-9893-11ea-bb37-0242ac130002','FACULTAD DE CIENCIAS','FC',NULL,NULL,'ciencias@espoch.edu.ec',NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,'2020-04-26 10:54:33',NULL),
	('a92c9770-9893-11ea-bb37-0242ac130002','FACULTAD DE CIENCIAS PECUARIAS','FCP',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c9838-9893-11ea-bb37-0242ac130002','CENTRO DE EDUCACIÓN FÍSICA','CEF',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c9900-9893-11ea-bb37-0242ac130002','SEDE FRANCISCO DE ORELLANA','SFO',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c99c8-9893-11ea-bb37-0242ac130002','SEDE MORONA SANTIAGO','SMS',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c9a86-9893-11ea-bb37-0242ac130002','CENTRO DE IDIOMAS','SMS',NULL,NULL,NULL,NULL,NULL,NULL,'9d691eb0-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,NULL,NULL),
	('a92c9b44-9893-11ea-bb37-0242ac130002','INSTITUTO DE INVESTIGACIONES','IDI',NULL,NULL,'hugo.moreno@espoch.edu.ec',NULL,NULL,NULL,'9d69243c-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002',NULL,'2020-04-25 00:21:10',NULL),
	('a92c9c0c-9893-11ea-bb37-0242ac130002','BODEGA','BDESPOCH','William Naur','Riobamba','bodega@espoch.edu.ec','bodega.espoch.edu.ec','0998220818','Interna','9d69243c-9896-11ea-bb37-0242ac130002','a92c8ffa-9893-11ea-bb37-0242ac130002','2020-04-24 00:15:29','2020-04-24 00:15:29',NULL),
	('a92cce48-9893-11ea-bb37-0242ac130002','CARRERA DE INGENIERIA EN SISTEMAS','EIS',NULL,NULL,'eis@espoch.edu.ec',NULL,NULL,NULL,'9d691c58-9896-11ea-bb37-0242ac130002','a92c9220-9893-11ea-bb37-0242ac130002','2020-04-26 23:18:22','2020-04-26 23:18:22',NULL);

/*!40000 ALTER TABLE `dependency` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla dependency_has_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dependency_has_users`;

CREATE TABLE `dependency_has_users` (
  `id_dependency` varchar(36) NOT NULL,
  `id_user` varchar(36) NOT NULL,
  PRIMARY KEY (`id_dependency`,`id_user`),
  KEY `fk_dependency_has_users_users1_idx` (`id_user`),
  KEY `fk_dependency_has_users_dependency1_idx` (`id_dependency`),
  CONSTRAINT `fk_dependency_has_users_dependency1` FOREIGN KEY (`id_dependency`) REFERENCES `dependency` (`id_dependency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dependency_has_users_users1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla dependencytype
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dependencytype`;

CREATE TABLE `dependencytype` (
  `id_dependencyType` varchar(36) NOT NULL,
  `name_dependencyType` varchar(50) NOT NULL,
  PRIMARY KEY (`id_dependencyType`),
  UNIQUE KEY `name_dependencyType_UNIQUE` (`name_dependencyType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `dependencytype` WRITE;
/*!40000 ALTER TABLE `dependencytype` DISABLE KEYS */;

INSERT INTO `dependencytype` (`id_dependencyType`, `name_dependencyType`)
VALUES
	('9d691c58-9896-11ea-bb37-0242ac130002','CARRERA'),
	('5e58036a-bc06-11ea-b3de-0242ac130004','EXTERNA'),
	('9d691eb0-9896-11ea-bb37-0242ac130002','FACULTAD'),
	('9d692040-9896-11ea-bb37-0242ac130002','INSTITUCIONAL'),
	('9d692356-9896-11ea-bb37-0242ac130002','UNIDAD ACADÉMICA'),
	('9d69243c-9896-11ea-bb37-0242ac130002','UNIDAD ADMINISTRATIVA');

/*!40000 ALTER TABLE `dependencytype` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id_group` varchar(36) NOT NULL,
  `code_group` varchar(10) NOT NULL,
  `name_group` varchar(255) NOT NULL,
  `acronym_group` varchar(11) NOT NULL,
  `mission_group` text,
  `vision_group` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_dependency` varchar(36) NOT NULL,
  `id_groupType` varchar(36) NOT NULL,
  `id_researchCenter` int(11) DEFAULT NULL,
  `active_group` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_group`),
  UNIQUE KEY `name_researchGroup_UNIQUE` (`name_group`),
  UNIQUE KEY `code_group_UNIQUE` (`code_group`),
  KEY `fk_researchGroup_researchCenter1_idx` (`id_researchCenter`),
  KEY `fk_group_groupType1_idx` (`id_groupType`),
  KEY `fk_group_dependency1_idx` (`id_dependency`),
  CONSTRAINT `fk_group_dependency1` FOREIGN KEY (`id_dependency`) REFERENCES `dependency` (`id_dependency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_group_groupType1` FOREIGN KEY (`id_groupType`) REFERENCES `grouptype` (`id_groupType`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_researchGroup_researchCenter1` FOREIGN KEY (`id_researchCenter`) REFERENCES `researchcenter` (`id_researchCenter`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id_group`, `code_group`, `name_group`, `acronym_group`, `mission_group`, `vision_group`, `created_at`, `updated_at`, `id_dependency`, `id_groupType`, `id_researchCenter`, `active_group`, `deleted_at`)
VALUES
	('6773020e-c95f-11ea-bdac-6003088c543c','aCEFGI-001','aGRUPO DE INVESTIGACIÓN EN CIENCIAS APLICADAS AL DEPORTE Y A LA EDUCACIÓN FÍSICA','aCIADEF','Es un grupo interdisciplinario, adscrito al Centro de Educación Física de la Escuela Superior Politécnica de Chimborazo, avalado por el Instituto de Investigaciones; que tiene como propósito contribuir al desarrollo del deporte y la Educación Física en la ciudad, provincia y país, pertinentes a los requerimientos del medio; con equidad, responsabilidad social, pluridiversidad y convivencia.','En el año 2022, INCIADEF será un grupo de investigación reconocido y clasificado en los Grupos de Investigación de la Educación Superior, que habrá generado proyectos d investigación con impacto en la comunidad académica, científica, en el deporte y la educación física; con evidencia del mejoramiento del estilo de vida de la población en general.','2020-07-18 20:30:18','2020-08-20 23:54:53','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe3ba6-98b9-11ea-bb37-0242ac130002','CEFGI-001','miGRUPO DE INVESTIGACIÓN EN CIENCIAS APLICADAS AL DEPORTE Y A LA EDUCACIÓN FÍSICA','INCIADEF','Es un grupo interdisciplinario, adscrito al Centro de Educación Física de la Escuela Superior Politécnica de Chimborazo, avalado por el Instituto de Investigaciones; que tiene como propósito contribuir al desarrollo del deporte y la Educación Física en la ciudad, provincia y país, pertinentes a los requerimientos del medio; con equidad, responsabilidad social, pluridiversidad y convivencia.','En el año 2022, INCIADEF será un grupo de investigación reconocido y clasificado en los Grupos de Investigación de la Educación Superior, que habrá generado proyectos d investigación con impacto en la comunidad académica, científica, en el deporte y la educación física; con evidencia del mejoramiento del estilo de vida de la población en general.','2020-01-24 16:28:00','2020-08-20 23:54:52','a92c9838-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe3da4-98b9-11ea-bb37-0242ac130002','FIEGI-010','GRUPO DE INVESTIGACIÓN E INTERACCIÓN EN LAS TECNOLOGÍAS DE LA COMUNICACIÓN','IITC','SIN MISIÓN','SIN VISIÓN','2020-01-24 18:17:51','2020-08-13 08:18:34','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe3e94-98b9-11ea-bb37-0242ac130002','FIEGI-011','GRUPO DE INVESTIGACIÓN EN ELECTROMAGNETISMO Y MICROONDAS','GIEM','SIN MISIÓN','SIN VISIÓN','2020-01-24 18:18:19','2020-07-20 18:04:56','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe3f66-98b9-11ea-bb37-0242ac130002','FIEGI-012','GRUPO DE INVESTIGACIÓN DE COMUNICACIONES INALAMBRICAS','GICI','Su objeto es la realización, promoción y difusión de la investigación científica generadora de transferencia de conocimiento, en el ámbito de las Tecnologías de la Información y Comunicación.','La finalidad de la investigación del GICI tiene una vocación aplicada a la generación de proyectos de investigación, publicación de resultados de investigación y trasferencia de conocimientos a los actores en el ámbito de las Telecomunicaciones.','2020-01-24 18:19:08','2020-07-20 18:00:40','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe402e-98b9-11ea-bb37-0242ac130002','FADEGI-001','GRUPO DE INVESTIGACIÓN EN MODELOS Y SISTEMAS DE GESTION','IMSG','Somos un equipo dedicado a la investigación, desarrollo, aplicación, análisis, evaluación de impacto y difusión del conocimiento general a partir de la ejecución de proyectos de investigación en el ámbito de la gestión y administración contemporánea para contribuir al bienestar y evolución de las empresas públicas y/o privadas.','Para el año 2022 seremos un grupo reconocido por la generación de conocimiento en nuestras líneas de investigación y registrado en el Sistema de Ciencia y Tecnología; referentes en la generación y divulgación del conocimiento relacionado con la gestión y administración contemporánea, contribuyendo con el desarrollo regional y nacional.','2020-01-24 18:34:11','2020-08-18 01:52:40','a92c9342-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe422c-98b9-11ea-bb37-0242ac130002','FMGI-001','GRUPO DE INVESTIGACIÓN Y ESTUDIO EN BIOINGENIERÍA','GIEBI','Generar, desarrollar, asimilar y aplicar el conocimiento científico y tecnológico en el desarrollo de tecnología biomecánica aplicada a mejorar las condiciones motrices de las personas con movilidad reducida.','Ser un grupo de investigación dedicado en lo fundamental a la investigación y al desarrollo tecnológico, centrada en la generación de equipos biomecánicos, que ayuden a mejorar las condiciones motrices de las personas con ciertos grados de discapacidad motriz, impulsando acciones orientadas a la generación del conocimiento que permitan buscar solución a las diversas problemáticas de las personas discapacitadas en base a la aplicación de diversas ciencias de la Ingeniería.','2020-01-24 18:36:25','2020-08-18 01:46:00','a92c9428-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe4308-98b9-11ea-bb37-0242ac130002','FIEGI-002','GRUPO DE INVESTIGACION EN INGENIERIA DE SOFTWARE','GRIISOFT','Lograr una participación proactiva directa y permanente de los integrantes del grupo en la ejecución de proyectos de investigación y vinculación con la colectividad en los ámbitos de desarrollo, mantenimiento y operación de sistemas informáticos, que garanticen la gestión, la generación de investigación científica y tecnológica que apoye y difunda los resultados de investigación, con reconocimiento nacional e internacional.','Ser uno de los más importantes grupos de investigación en Ingeniería de Software en América Latina, reconocido a nivel nacional e internacional, cuyo trabajo sea reconocido tanto por la academia como por el sector empresarial; se enfoca en el desarrollo de proyectos de investigación en los que se aplica con éxito la teoría y experimentación de todas las áreas de la ingeniería de software; facilitando la transferencia tecnológica y la adopción de buenas prácticas en el desarrollo de mantenimiento y operación de software y es capaz de socializar sus resultados de impacto a nivel nacional como internacional, buscando siempre colaborar con el desarrollo de una industria competitiva a escala internacional','2020-01-24 16:30:20','2020-08-18 01:46:02','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe4524-98b9-11ea-bb37-0242ac130002','FIEGI-003','GRUPO DE INVESTIGACIÓN KARAY LABORATORIO CREATIVO','GIK','Generar conocimiento científico en los ámbitos de diseño, comunicación, arte y cultura , en entornos educativos, sociales y culturales de la provincia y el país, fomentando experiencias participativas y experimentales.','Ser un grupo de investigación que cuente con el reconocimiento local y nacional en aspectos relacionados con el diseño, comunicación, arte, cultura y creatividad manteniendo una política de mejoramiento continuo de sus integrantes y el desarrollo de sus capacidades investigativas conducentes al aporte de soluciones a problemas del contexto que estén enmarcadas en las líneas de investigación del grupo KARAY laboratorio creativo.','2020-01-24 16:33:03','2020-07-20 18:02:00','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe45e2-98b9-11ea-bb37-0242ac130002','FIEGI-004','GRUPO DE INVESTIGACIÓN TECNOLOGÍAS DE LA INFORMACIÓN PARA LA GESTIÓN DEL CONOCIMIENTO','TIGECON','SIN MISIÓN','SIN VISIÓN','2020-01-24 16:35:11','2020-07-19 01:02:50','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe4696-98b9-11ea-bb37-0242ac130002','FIEGI-005','GRUPO DE INVESTIGACIÓN SEGURIDAD INFORMÁTICA Y TELEMÁTICA','SEGINTE','SIN MISIÓN','SIN VISIÓN','2020-01-24 16:36:00','2020-07-19 01:02:50','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe4754-98b9-11ea-bb37-0242ac130002','FIEGI-006','GRUPO DE INVESTIGACION ENERGÍA EÓLICA','GEE','Desarrollar proyectos de investigación enfocados en la utilización de fuentes alternativas de energía eléctrica, cuyos resultados sirvan para promover la generación de energías limpias para impulsar el desarrollo económico, turístico y social de la provincia de Chimborazo y del país.','Promover proyectos energéticos de origen renovable innovadores y eficientes en el hábitat natural, contribuyendo a satisfacer la demanda energética de la población, asumiendo la responsabilidad de preservar el ecosistema, y de dar respuesta a las necesidades de la provincia y del país, generando valor para nuestros ciudadanos y favoreciendo la difusión del valor del medio natural en el mundo.','2020-01-24 16:37:09','2020-07-19 01:02:51','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,0,NULL),
	('a9fe4808-98b9-11ea-bb37-0242ac130002','FIEGI-007','GRUPO DE INVESTIGACIÓN EN TECNOLOGÍAS DE LA ELECTRÓNICA Y AUTOMÁTICA.','GEE2','Constituirse como un grupo élite de trabajo investigativo, centrado en contribuir al desarrollo tecnológico mediante la generación de proyectos de investigación e innovación que den solución a problemáticas locales, nacionales e internacionales en áreas de la Ingeniería Electrónica, Eléctrica y Automatización Industrial, enfocándose en garantizar la excelencia, calidad y originalidad en el desarrollo de actividades que aporten a la sociedad y favorezcan su crecimiento productivo.','Ser un grupo de investigación científica de reconocimiento local, nacional e internacional, enfocado a contribuir al desarrollo tecnológico y productivo de la sociedad en áreas de la Ingeniería Electrónica, Eléctrica y Automatización Industrial, apoyado en un contingente multidisciplinario de personas conformado por académicos, investigadores y estudiantes comprometidos con los procesos de transferencia de conocimientos mediante eventos y publicaciones regionales e internacionales.','2020-01-24 16:38:18','2020-07-20 18:06:30','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe48bc-98b9-11ea-bb37-0242ac130002','FIEGI-008','GRUPO DE INVESTIGACIÓN, MODELADO, ANIMACIÓN Y SIMULACIÓN 3D','MSA3D','Somos un grupo de investigadores dedicados a la búsqueda de soluciones informáticas, electrónicas y de diseño 3D para la academia, investigación y vinculación. Nos comprometemos a producir productos y servicios de calidad a nivel de la ESPOCH y fuera de ella.','Ser un referente en el ámbito universitario a nivel local y nacional, ofreciendo productos con innovación y con un valor agregado referente a soluciones tecnológicas para necesidades sociales. Nos ubicamos en el contexto de utilizar tecnología de punta y estar en constante innovación realizando la “extra milla”.','2020-01-24 18:16:20','2020-07-20 18:06:55','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL),
	('a9fe497a-98b9-11ea-bb37-0242ac130002','FIEGI-009','GRUPO DE INVESTIGACIÓN LEARNING ENGLISH WITH ICTs','ICTs','Desarrollar Programas Educativos basados en tecnología, aplicaciones y software para mejorar las competencias lingüísticas como Listening, Speaking, Reading y Writing','Desarrollar y validar metodologías educativas innovadoras en el campo de la enseñanza del idioma Ingles a través del uso de la tecnología educativa, TIC, aplicaciones software que puedan ser utilizadas en la enseñanza universitaria pero también en otros niveles educativos','2020-01-24 18:17:19','2020-07-20 18:18:26','a92c9220-9893-11ea-bb37-0242ac130002','1',NULL,1,NULL);

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla groups_has_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups_has_users`;

CREATE TABLE `groups_has_users` (
  `id_group` varchar(36) NOT NULL,
  `id_user` varchar(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_group`,`id_user`),
  KEY `fk_groups_has_users_users1_idx` (`id_user`),
  KEY `fk_groups_has_users_groups1_idx` (`id_group`),
  CONSTRAINT `fk_groups_has_users_groups1` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id_group`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_groups_has_users_users1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



# Volcado de tabla grouptype
# ------------------------------------------------------------

DROP TABLE IF EXISTS `grouptype`;

CREATE TABLE `grouptype` (
  `id_groupType` varchar(36) NOT NULL,
  `name_groupType` varchar(255) NOT NULL,
  PRIMARY KEY (`id_groupType`),
  UNIQUE KEY `name_groupType_UNIQUE` (`name_groupType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `grouptype` WRITE;
/*!40000 ALTER TABLE `grouptype` DISABLE KEYS */;

INSERT INTO `grouptype` (`id_groupType`, `name_groupType`)
VALUES
	('1','INVESTIGACION'),
	('2','VINCULACION');

/*!40000 ALTER TABLE `grouptype` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla impact_sector
# ------------------------------------------------------------

DROP TABLE IF EXISTS `impact_sector`;

CREATE TABLE `impact_sector` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `impact_sector` WRITE;
/*!40000 ALTER TABLE `impact_sector` DISABLE KEYS */;

INSERT INTO `impact_sector` (`id`, `name`)
VALUES
	('0b30e358-1a20-3e31-9279-806a73c087c3','RECURSOS NATURALES'),
	('1e908557-26fe-3ffb-bba8-c70ceb621e78','DESARROLLO HUMANO Y SOCIAL'),
	('31bb4a04-7837-316c-9a4f-1681297d6f84','BIODIVERSIDAD Y AMBIENTE'),
	('a3f341ef-78fc-3f56-8fd4-e24d2a25eab6','FOMENTO AGROPECUARIO Y DESARR PRODUCTIVO'),
	('a5d38661-b8ef-30ad-8c4b-8136aabd24ed','ENERGÍA'),
	('ecd4e3e2-2f28-30cc-9824-6259d17ac465','TECNOLOGÍA DE LA INFOMRACIÓN Y COMUNICACIÓN ');

/*!40000 ALTER TABLE `impact_sector` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla impactsector_has_project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `impactsector_has_project`;

CREATE TABLE `impactsector_has_project` (
  `impact_sector_id` varchar(36) NOT NULL DEFAULT '',
  `project_id` varchar(36) NOT NULL DEFAULT '',
  PRIMARY KEY (`impact_sector_id`,`project_id`),
  KEY `fk_impactSector_has_project_project1_idx` (`project_id`),
  KEY `fk_impactSector_has_project_impactSector_idx` (`impact_sector_id`),
  CONSTRAINT `fk_impactSector_has_project_impactSector` FOREIGN KEY (`impact_sector_id`) REFERENCES `impact_sector` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_impactSector_has_project_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `impactsector_has_project` WRITE;
/*!40000 ALTER TABLE `impactsector_has_project` DISABLE KEYS */;

INSERT INTO `impactsector_has_project` (`impact_sector_id`, `project_id`)
VALUES
	('568cdf7b-b4dd-39b7-9697-eeb139fe8b6e','5232058e-e7dc-11ea-90ac-85c2b5111849'),
	('cae1042e-256d-3dd8-b87f-9bc75751f08e','5232058e-e7dc-11ea-90ac-85c2b5111849'),
	('4dec5230-f824-31d7-b38f-a86e44f908d4','b42bad60-e810-11ea-b0be-6003088c543c');

/*!40000 ALTER TABLE `impactsector_has_project` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2016_06_01_000001_create_oauth_auth_codes_table',1),
	(2,'2016_06_01_000002_create_oauth_access_tokens_table',2),
	(3,'2016_06_01_000003_create_oauth_refresh_tokens_table',2),
	(4,'2016_06_01_000004_create_oauth_clients_table',2),
	(5,'2016_06_01_000005_create_oauth_personal_access_clients_table',2),
	(6,'2019_08_19_000000_create_failed_jobs_table',2),
	(7,'2020_08_19_234224_create_permission_tables',2);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla model_has_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model_has_permissions`;

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla model_has_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model_has_roles`;

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`)
VALUES
	(1,'App\\Models\\User','1d1d9a0a-e2b6-11ea-9775-6003088c543c'),
	(7,'App\\Models\\User','1d29c8e8-e2b6-11ea-abc9-6003088c543c');

/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla oauth_access_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_access_tokens`;

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`)
VALUES
	('1436576131975f52af4ed119a0337c5c970123d7fbc33e65c97a6e38638b575753d12bea4620b431','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-24 12:57:09','2020-08-24 12:57:09','2021-08-24 12:57:09'),
	('1683f02af02de7cb9917f1100292091c49f439d920653ac379f0f7f2e5d5ae570d8b6963e5c347a8','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-23 12:51:40','2020-08-23 12:51:40','2021-08-23 12:51:40'),
	('1685ce4a23fa30c6d5c5e42164b856e4d7b61ad77ed4097cff8925e53cd4a48063e045c47e2d919a','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-22 20:42:43','2020-08-22 20:42:43','2021-08-22 20:42:43'),
	('1b7dbb737a9b994930d99d190768f38314b36cc4ebadc40d2866ca4a3177b3cfd94d060c5d3cceaa','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-26 21:59:40','2020-08-26 21:59:40','2021-08-26 21:59:40'),
	('246a27b87b910bdda49bc8acdbb5cb8eec63b7d0eb75197c751393706def7fc11b7efb476ef19a76','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',1,'2020-08-20 12:46:25','2020-08-20 12:46:25','2021-08-20 12:46:25'),
	('2510fe171bfd8c5ca39d24ead2bb7f4f09814c27c1ea46843109308340e4fad3bd104b8b0e139de4','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-24 00:04:14','2020-08-24 00:04:14','2021-08-24 00:04:14'),
	('2618849f2acb2834363a3d476b07434d2493c86a59b288bef9dabb4854327c3a5737ad2e8df37414','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-24 00:09:43','2020-08-24 00:09:43','2021-08-24 00:09:43'),
	('3b19d923c6fb3a6dfb6e6178d2185721a1e1615286bf58b74ef861521adb5257ca0bfa5cb1e8c70d','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-26 12:32:29','2020-08-26 12:32:29','2021-08-26 12:32:29'),
	('3bb01772cd1c7a7ec2b32a4c1b5f4f5eb774e55e5d5b41ff6715d61d93bb0c76e9e61b9be2f43f5a','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-20 09:21:48','2020-08-20 09:21:48','2021-08-20 09:21:48'),
	('45239eccdaa07f649acf1fb8965f6334dd0df14a569c18856855eaceafff42a7b56f1082ff47e115','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-20 02:38:26','2020-08-20 02:38:26','2021-08-20 02:38:26'),
	('460b1a312afdbe6e7fff8a5c50ebe09bd0cc36086b09020bd69a7cfc3a8079978d8f6b8f9a1b3a00','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-26 21:07:41','2020-08-26 21:07:41','2021-08-26 21:07:41'),
	('48174aeb9b5f9d42131816dcbb281a1c299b62797728b9a066804a3702aea041f39ceddfdc479438','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-23 00:37:22','2020-08-23 00:37:22','2021-08-23 00:37:22'),
	('50f443904f72f157e99cd31c5548c8ee438c39b47aa42ef83242e885be0530af6416d71a5c60ad02','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-24 14:37:32','2020-08-24 14:37:32','2021-08-24 14:37:32'),
	('538bc0b9b3bc8f3a47a6184df82601a408f8267bb96aaa318eafbd18fdbeaf3c708d4e571f9f47de','63bd1734-e30f-11ea-9fe7-7b2a60d1cfd8',1,NULL,'[]',0,'2020-08-20 13:03:20','2020-08-20 13:03:20','2021-08-20 13:03:20'),
	('53b6b78deadee9fdad99788d481fd6361f89b5c4e98a2eb3d80a0596d74651115f3603857b3e890a','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-20 12:29:56','2020-08-20 12:29:56','2021-08-20 12:29:56'),
	('5f0cad4f8b517720ac2f8acd7e6a64cd454aff91ee666e06f0df57e5d805297f41ddc4a61d7b19bb','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-27 10:42:24','2020-08-27 10:42:24','2021-08-27 10:42:24'),
	('63f57b0ebbe9754f0c7d7d07730099599fad824c5dd176f4a4a874f3898037c06470e2e5eb5326ed','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-24 14:34:23','2020-08-24 14:34:23','2021-08-24 14:34:23'),
	('6deae382f5d3e9dbc716d1632678df47b0135d9666efa44a6f7a7d19032e3ec7d33165b28fa8c7b3','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',1,'2020-08-24 00:04:54','2020-08-24 00:04:54','2021-08-24 00:04:54'),
	('6ef82d12303583611b7d860c42eb0dd5dcaea234d5b263c3b08d3e372597b895a2e4b49deed4bb8e','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-24 22:37:49','2020-08-24 22:37:49','2021-08-24 22:37:49'),
	('6f68b7aaef9e24d82331bb617418262a6c3284dcac15ffd8b2001211241cd90b9816a955a10d9484','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-29 16:19:03','2020-08-29 16:19:03','2021-08-29 16:19:03'),
	('6f90c610aa2b5d0273df7acc6c0abdc83ca222e394cc28996f86422abe1e9b81289e9b7d41bd197f','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-20 10:47:43','2020-08-20 10:47:43','2021-08-20 10:47:43'),
	('71aaa15a769c273e04cd22055cf1c4163eeb82d2be8cb9447e7c6e45d751c5bf51eaf990d0c97577','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-20 12:52:30','2020-08-20 12:52:30','2021-08-20 12:52:30'),
	('77d8f04e4fc4c010c914532bae427faf721817752a366d20ca6d38a918c0976369c347bf17fe1755','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-26 20:56:29','2020-08-26 20:56:29','2021-08-26 20:56:29'),
	('78134e6a515581f0cc2f4fb7e15168ebfecc460b145ab9807a8e4d83a7e4da41fd54b11a1a2bdc9f','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-26 23:11:00','2020-08-26 23:11:00','2021-08-26 23:11:00'),
	('79038467b3def22a4e7bc01e1f0b4927bb19f4d4da354d901f6b4b9f0327c7b7b40b28720a7d08c0','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-25 16:42:00','2020-08-25 16:42:00','2021-08-25 16:42:00'),
	('7a0a5c0ce2e3b4e039f99639ac1ddcb33dd43d1e1df1a28d784d4efa7b669a4ade146bbe41dc2b2b','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-27 10:51:33','2020-08-27 10:51:33','2021-08-27 10:51:33'),
	('80916cf6f4f6951a0cd76c40825a5afff4dcc918b9deb323e1872250d106519e838f931eed3f5cad','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-28 08:48:24','2020-08-28 08:48:24','2021-08-28 08:48:24'),
	('836fdef08bf3ddf00091618b16e13bace975e939b3da250d4ccaee519ac5977cd5a1b9813b2375c0','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-26 23:08:51','2020-08-26 23:08:51','2021-08-26 23:08:51'),
	('853b1b1a4e277ae234d158897113b53eac03c576e58af17111793b0757234ce582f01c233bc72d63','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-24 11:07:22','2020-08-24 11:07:22','2021-08-24 11:07:22'),
	('86f409b726784a698965187bf069209ba25229d12f14e1f976a530173df00db10c50dc390fc42750','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',1,'2020-08-20 21:16:25','2020-08-20 21:16:25','2021-08-20 21:16:25'),
	('8d42e934e01faf2fc65511a713e326c380e62c8437f7cdbecb141c9ab4188a25372f975200eaa43f','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-26 11:18:21','2020-08-26 11:18:21','2021-08-26 11:18:21'),
	('927cddc78a46641f872bb70d4a7ef5d649b2376560d80ddab2a8f0ec6f8137d927340d4c4a86a19a','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-25 14:34:35','2020-08-25 14:34:35','2021-08-25 14:34:35'),
	('9316fa4c2d44889f3ec431c094af3b07c1e2c069e58deeba0b0aa607a24aa59d138bfac600f84fde','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-24 14:27:39','2020-08-24 14:27:39','2021-08-24 14:27:39'),
	('9460fccdf385b04c99e2fbcfac19c540359b46dfd7bba24e192de30e0d5932600d65a0003504c9eb','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-20 13:45:41','2020-08-20 13:45:41','2021-08-20 13:45:41'),
	('9cbc90bb28b837343c115321a9876eea71633be3bb5674bab537f727a54cd1522c6a8588d270e4d4','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-26 12:29:49','2020-08-26 12:29:49','2021-08-26 12:29:49'),
	('9d919453ca0b448ce2f1871e1f5f148a3e0abd3048475c5fe4c302180b15709e83aed8d3f40b2d4d','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-26 11:13:22','2020-08-26 11:13:22','2021-08-26 11:13:22'),
	('a7fdd5e686cf40d7ce2aec8f57def2901e891019fdfa8ba11b035c4573c1b8f17a53aeb225fbb3f8','17ffa6c4-98d0-11ea-bb37-0242ac130002',1,NULL,'[]',0,'2020-08-20 00:28:35','2020-08-20 00:28:35','2021-08-20 00:28:35'),
	('a829b98484a18fb6e3f1a93e1d5a476bf3a0bc0fa8cf7f976975347f4ca2fd1be6dca0132d526146','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',1,'2020-08-23 22:11:14','2020-08-23 22:11:14','2021-08-23 22:11:14'),
	('af5585a23fb64879c608fe12619bef4d10315772b2d449b004b051a21ac786f5a7f266181d3a57c0','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-24 11:02:32','2020-08-24 11:02:32','2021-08-24 11:02:32'),
	('b64f5724debee6d38ff1b3de720f163c0618df4bc83d66b0455a32aa99db0692d3c9c6f681096156','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-24 00:10:36','2020-08-24 00:10:36','2021-08-24 00:10:36'),
	('bab67d08d9a4632e97a95390df49ef6d8590bb4be8fd3260433712600ecd13b71857fb6f0c617b53','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',1,'2020-08-20 23:35:32','2020-08-20 23:35:32','2021-08-20 23:35:32'),
	('be8204c6ca08cd2e7f990da81a90859b756f9b83af31f793ede33ae5a1b31cbddb3224505c6e1f66','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-24 23:00:30','2020-08-24 23:00:30','2021-08-24 23:00:30'),
	('bee7ef3465e87f591f122aeb7647b0ec8295cbafda43b33bed73ffae2c8ed865becbb0fb5316f569','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-26 20:53:51','2020-08-26 20:53:51','2021-08-26 20:53:51'),
	('c2a873c2027061c8a7aa983023bf07edfd27dbe0c6084e412816e0530215914f3e33f31df1fba1b9','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-26 21:43:26','2020-08-26 21:43:26','2021-08-26 21:43:26'),
	('c2d5f10cd58efdcdd67cafc1e32f29e65724e72edd73998fb120590fd8aa05f6176e7193c0bc3725','1a3ed7e2-e313-11ea-8720-c93841bd0bb8',1,NULL,'[]',0,'2020-08-20 13:30:01','2020-08-20 13:30:01','2021-08-20 13:30:01'),
	('c36288c03b710401e66ad6767f22c1822f25fc757933b36d053514813d1d119bf8fc3073efa1fe02','17ffa6c4-98d0-11ea-bb37-0242ac130002',1,NULL,'[]',0,'2020-08-20 00:36:54','2020-08-20 00:36:54','2021-08-20 00:36:54'),
	('c3efc9d546e55001f7e65cd6cf374689db2526ffd94c911ff3519287b157572488f7b9ba83011cb4','1a3ed7e2-e313-11ea-8720-c93841bd0bb8',1,NULL,'[]',1,'2020-08-20 13:45:09','2020-08-20 13:45:09','2021-08-20 13:45:09'),
	('c48e212d4a96d239845ae53036a43fa9e1272d5431687f5fe859446be6affd629e479d4ceeeba927','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-26 21:03:29','2020-08-26 21:03:29','2021-08-26 21:03:29'),
	('c6e370f89188ec738a57d048c7353de65924a307c02636c77f3c05477edf4006c54b21d5578bdb2e','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-20 22:53:41','2020-08-20 22:53:41','2021-08-20 22:53:41'),
	('c783fe321374d5b88f113fea542f078ee84ba28ea289544b6dca5990b3f1a6727beff6fccb86f5eb','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-26 21:03:52','2020-08-26 21:03:52','2021-08-26 21:03:52'),
	('c913bf121d9a4328363440dfe7063c2f741de5f4ab3703d2a2716f3477463b670ba183a00321078e','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',1,'2020-08-26 22:59:39','2020-08-26 22:59:39','2021-08-26 22:59:39'),
	('cb6441a25557758c9a5f13e5549fee947417a17d40284a83a82f487738ac5e40e75fd1bb412269e6','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-20 17:12:35','2020-08-20 17:12:35','2021-08-20 17:12:35'),
	('cddd95c349c9112f0434dd916279b5184495af18aff99af8641e97493ce020b91154f7ad215f5cca','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-27 10:43:32','2020-08-27 10:43:32','2021-08-27 10:43:32'),
	('d42ad33336699328312f707c56f4573b70ad0a05efc0ede3128040a44a5a4bc43fb283f23c76c5bc','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-23 17:46:38','2020-08-23 17:46:38','2021-08-23 17:46:38'),
	('d448cb6f323f5ba7492c26c39a92eaad2a67098f7efcb87cdecb44619800c988123875058a2037b9','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-23 21:37:46','2020-08-23 21:37:46','2021-08-23 21:37:46'),
	('dacf8507eb00f5b310d68bd2def9e3d349a0996a28675ea7a56f1d5448c1d8c83eebf96f722291b0','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-26 21:39:49','2020-08-26 21:39:49','2021-08-26 21:39:49'),
	('e0c7e0f2fb03025b22078a659f961711c7dd75f648c11fae655859cd993b55c6ec2ec20df075de8c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',1,'2020-08-21 00:05:54','2020-08-21 00:05:54','2021-08-21 00:05:54'),
	('e44ca34d24ad87fb21e7fcfdfba6949b4cf4c1d65c6d9871a038e49544d6560e698fbcc686e60e74','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-26 11:19:16','2020-08-26 11:19:16','2021-08-26 11:19:16'),
	('e717e2aa73c1456257673734741dc902e79138ed9c6275c46b5e6b97e96bc93b48e4ce9078afa94e','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-21 17:05:21','2020-08-21 17:05:21','2021-08-21 17:05:21'),
	('ec816c8979bf70f389cbcc5dd9775b7038e435cb2170b459e68a15594337f46ec42a5e5763e6690d','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-08-21 10:24:25','2020-08-21 10:24:25','2021-08-21 10:24:25'),
	('f25d0c52fc30480f1461d6f7a700c9ddef2462539ff64a59e24aad2b2b1a8039e8d1810f8949e0c6','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-26 10:23:41','2020-08-26 10:23:41','2021-08-26 10:23:41'),
	('f3cdf41339afba0c33009458fb8ffa8417ec781de4ae79d5604ebb75d29eedcf3ff465f5d3bf4f3d','1d29c8e8-e2b6-11ea-abc9-6003088c543c',1,NULL,'[]',0,'2020-08-26 11:38:10','2020-08-26 11:38:10','2021-08-26 11:38:10'),
	('fd4d9c0bfbc2f4e79809758336d6e02d880b3f557627bd8749388e10b6dcf06df7b42f3c7c125fec','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-08-20 02:29:18','2020-08-20 02:29:18','2021-08-20 02:29:18');

/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla oauth_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_clients`;

CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`)
VALUES
	(1,NULL,'spirit7','sfrkKdecnsHk1ZiB3NZr1xvkbOhDUKwUJNQI4Oad','users','http://localhost',0,1,0,'2020-08-20 00:28:17','2020-08-20 00:28:17');

/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla oauth_personal_access_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_personal_access_clients`;

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla oauth_refresh_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_refresh_tokens`;

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`)
VALUES
	('00385155a2c3ba9702011a09e6bdf72c2814d7b70c7c388d39969d6ba635356b85f421b59f62917f','bab67d08d9a4632e97a95390df49ef6d8590bb4be8fd3260433712600ecd13b71857fb6f0c617b53',0,'2021-08-20 23:35:32'),
	('07803c737b3865daeee3e7376f32cab66ee75d4fddb2235a1e3b78485aefe40cc3e757c4a28e6be8','9cbc90bb28b837343c115321a9876eea71633be3bb5674bab537f727a54cd1522c6a8588d270e4d4',0,'2021-08-26 12:29:49'),
	('0902459313f48c98188881899dcb8747cee64e6b4f2b93b4abcbb7db7414b23b9e573010b21ec2f9','538bc0b9b3bc8f3a47a6184df82601a408f8267bb96aaa318eafbd18fdbeaf3c708d4e571f9f47de',0,'2021-08-20 13:03:20'),
	('0c9d49b962f21720f3517f59c09ace5d707f09f846f389fb0c3111f24da08f5bac907e1d3cf52a16','9d919453ca0b448ce2f1871e1f5f148a3e0abd3048475c5fe4c302180b15709e83aed8d3f40b2d4d',0,'2021-08-26 11:13:22'),
	('0cbe10a1d0b943e7eba52e65c54d010a86407362d5da3cdb5601acf436dda43e916fef3fa69f693f','86f409b726784a698965187bf069209ba25229d12f14e1f976a530173df00db10c50dc390fc42750',0,'2021-08-20 21:16:25'),
	('0cd6582aa664602321f4088dfa2c1266274e484a9a176031dcfb64d6b9106c7f4cea843eb8e9fb79','e717e2aa73c1456257673734741dc902e79138ed9c6275c46b5e6b97e96bc93b48e4ce9078afa94e',0,'2021-08-21 17:05:21'),
	('0f1bef782b1949deb3c49ae7caf52a08698bbda4ef6da0176fadcfce9c1636b178cdbef8e772432f','e44ca34d24ad87fb21e7fcfdfba6949b4cf4c1d65c6d9871a038e49544d6560e698fbcc686e60e74',0,'2021-08-26 11:19:16'),
	('1d9492b9a8a0b39029a3c09c27ebc15abada0ed660ab5651030c4b9275caccf240658b272bbbaeca','c783fe321374d5b88f113fea542f078ee84ba28ea289544b6dca5990b3f1a6727beff6fccb86f5eb',0,'2021-08-26 21:03:52'),
	('1f286e8a7d48a87bea1bc5f1cfdf2a5aa0a306325f2ac572de3b0e4b0d0f9c677e76bce97661a7b8','7a0a5c0ce2e3b4e039f99639ac1ddcb33dd43d1e1df1a28d784d4efa7b669a4ade146bbe41dc2b2b',0,'2021-08-27 10:51:33'),
	('213babd0a7bcae64e40a5f567ac1849f1131d975015573c2aae53e0bc355093ca88899f0b2729c32','6ef82d12303583611b7d860c42eb0dd5dcaea234d5b263c3b08d3e372597b895a2e4b49deed4bb8e',0,'2021-08-24 22:37:49'),
	('22c08d13ac9a7a6aa4af2c5574b73b6623729756fcad6ed09d368b070720a2981ee12e734977c9cb','79038467b3def22a4e7bc01e1f0b4927bb19f4d4da354d901f6b4b9f0327c7b7b40b28720a7d08c0',0,'2021-08-25 16:42:00'),
	('22df499f21da82782d94dcd5a7ad4e22797147576fa7e315d339024ca3c4b9906890ea55914d25ba','c3efc9d546e55001f7e65cd6cf374689db2526ffd94c911ff3519287b157572488f7b9ba83011cb4',0,'2021-08-20 13:45:09'),
	('236b6dade1fdc3eacba2e15470edff3af4acb40135b8df596832338308588113aac5855433852d90','246a27b87b910bdda49bc8acdbb5cb8eec63b7d0eb75197c751393706def7fc11b7efb476ef19a76',0,'2021-08-20 12:46:25'),
	('24ada2118b73ac7437ad892e73f853f6f444bc56dbfa8585c217a1b38642d547f64a8e67a085c77f','bee7ef3465e87f591f122aeb7647b0ec8295cbafda43b33bed73ffae2c8ed865becbb0fb5316f569',0,'2021-08-26 20:53:51'),
	('28d734f138190a15c91f4f834a38ca49eb008d467dbe11532e47436096912cddc271c23212fa907b','1436576131975f52af4ed119a0337c5c970123d7fbc33e65c97a6e38638b575753d12bea4620b431',0,'2021-08-24 12:57:09'),
	('35eac4f862494dcd395e7d4b9af10a6e484cb399e8411bf4163e9250da22705f4d62ea3feedacdc9','48174aeb9b5f9d42131816dcbb281a1c299b62797728b9a066804a3702aea041f39ceddfdc479438',0,'2021-08-23 00:37:22'),
	('3806c49f96be49049d5092edd1915b576487c10ed20ae9ce6b0c4d5c975822ad4598cace01cafbf3','3bb01772cd1c7a7ec2b32a4c1b5f4f5eb774e55e5d5b41ff6715d61d93bb0c76e9e61b9be2f43f5a',0,'2021-08-20 09:21:48'),
	('384d5917ae30080f20be6aa332a40fec08ab9d5cde7e0c276b145c44b5236f055ff3a83795783d5c','c913bf121d9a4328363440dfe7063c2f741de5f4ab3703d2a2716f3477463b670ba183a00321078e',0,'2021-08-26 22:59:39'),
	('423fc2698e9f9ab50f5c02a89fa36b66a330583a7c35293fd28d638fdd0d5814aeef26989b667be9','927cddc78a46641f872bb70d4a7ef5d649b2376560d80ddab2a8f0ec6f8137d927340d4c4a86a19a',0,'2021-08-25 14:34:35'),
	('451bc006218ac9ecde9fcdd846d46e4cead5aa705987f1b8ab9e9a21fe492d70b1aab15ab5c9aad4','c36288c03b710401e66ad6767f22c1822f25fc757933b36d053514813d1d119bf8fc3073efa1fe02',0,'2021-08-20 00:36:54'),
	('4842c46e59deb55862eca02875df236cbd11e9ea2a45c605692502052d6acd4011bf5966e52269b8','460b1a312afdbe6e7fff8a5c50ebe09bd0cc36086b09020bd69a7cfc3a8079978d8f6b8f9a1b3a00',0,'2021-08-26 21:07:41'),
	('4a5ed2b0b88e6f10b1a7cb5d8177016a05abace784cdbda5f1c168a2572ccb7ba979ea7ea1f0f21d','cddd95c349c9112f0434dd916279b5184495af18aff99af8641e97493ce020b91154f7ad215f5cca',0,'2021-08-27 10:43:32'),
	('4a71b4271a74c2ff229e01509c9c403b3bebe3bdc10ce0f1b89ca56f62736f4af26bb53cf6223cdb','c6e370f89188ec738a57d048c7353de65924a307c02636c77f3c05477edf4006c54b21d5578bdb2e',0,'2021-08-20 22:53:41'),
	('4f1156d7a73a47b6e4ce50d6d8a46f19557ef1e6471c32749b216d133126d2fdbfd4c170f70b1156','c48e212d4a96d239845ae53036a43fa9e1272d5431687f5fe859446be6affd629e479d4ceeeba927',0,'2021-08-26 21:03:29'),
	('52cc598fb82165bb272bd16fa66a60f47d5de8ff3329088888ef41d003e4db5ed2d4fcbc04ca274f','6f90c610aa2b5d0273df7acc6c0abdc83ca222e394cc28996f86422abe1e9b81289e9b7d41bd197f',0,'2021-08-20 10:47:43'),
	('570604fa34fa273a2405a952ad851d81787364b36fc7af724b5dd5e1c3e42e89d6cb6316b462e79a','dacf8507eb00f5b310d68bd2def9e3d349a0996a28675ea7a56f1d5448c1d8c83eebf96f722291b0',0,'2021-08-26 21:39:49'),
	('5a4f12d8b5bab52a4404390d7c436d391a25f05e0ab7494289c9fff4b71d8794942de6d6f1fdc7f8','77d8f04e4fc4c010c914532bae427faf721817752a366d20ca6d38a918c0976369c347bf17fe1755',0,'2021-08-26 20:56:29'),
	('5d873ca65687f3fc507e65ac5e9fa895cfe17c32c79f6e9c74c3402fb7cb24e7a0721ae110ad51ad','9460fccdf385b04c99e2fbcfac19c540359b46dfd7bba24e192de30e0d5932600d65a0003504c9eb',0,'2021-08-20 13:45:41'),
	('601f41f76741aa1a0d40fb206d2ab25bc802a3c77630a732d614716510374ab6a42e60ce515afec8','2618849f2acb2834363a3d476b07434d2493c86a59b288bef9dabb4854327c3a5737ad2e8df37414',0,'2021-08-24 00:09:43'),
	('62e3186480dd63a659e6f95ee7fbf5bede7d97e5075e3ffbad1d67c7217ac6fb36630a00dacd045e','a829b98484a18fb6e3f1a93e1d5a476bf3a0bc0fa8cf7f976975347f4ca2fd1be6dca0132d526146',0,'2021-08-23 22:11:14'),
	('65c53444e5f3d58ee414c4c5c44d88f9ee02cf7b63fe266e531a3a772fe4b2f20a4cd082996d8fe4','d448cb6f323f5ba7492c26c39a92eaad2a67098f7efcb87cdecb44619800c988123875058a2037b9',0,'2021-08-23 21:37:46'),
	('67b5bcddce729efe3af4def3fc267fcfd60b261746f5e401d24c8d9610b54915f3d8eafb9a73edb7','9316fa4c2d44889f3ec431c094af3b07c1e2c069e58deeba0b0aa607a24aa59d138bfac600f84fde',0,'2021-08-24 14:27:39'),
	('6917cdc501f302c8d93e4a0c297363a599032df859d594f5328ec37a274e237c906d7a0e57d619ca','836fdef08bf3ddf00091618b16e13bace975e939b3da250d4ccaee519ac5977cd5a1b9813b2375c0',0,'2021-08-26 23:08:51'),
	('70fa03c4edd7ea4d283e3c80874a25de0fb54884f1079774fa7cb40ade7812434be2621374a5d10d','5f0cad4f8b517720ac2f8acd7e6a64cd454aff91ee666e06f0df57e5d805297f41ddc4a61d7b19bb',0,'2021-08-27 10:42:24'),
	('783f1fbccf1f657405191eec56809d2af42fc36731fd51da3d23ad503dadde8e53b2a153fd4edb87','45239eccdaa07f649acf1fb8965f6334dd0df14a569c18856855eaceafff42a7b56f1082ff47e115',0,'2021-08-20 02:38:26'),
	('78cca6a162dbffe8757103ab0b9f83928dfb96236dfcbd1c0e593a1a3d263fcb6d59539a6e7f8655','1b7dbb737a9b994930d99d190768f38314b36cc4ebadc40d2866ca4a3177b3cfd94d060c5d3cceaa',0,'2021-08-26 21:59:40'),
	('7dcfe8228f84d7a700a5e1a85533201a2515e836fc18199386f311f121fa9d54eeb1ad6553176992','53b6b78deadee9fdad99788d481fd6361f89b5c4e98a2eb3d80a0596d74651115f3603857b3e890a',0,'2021-08-20 12:29:56'),
	('83aab0b568e4b19229093ff240fa9e3ced152cf988260280e5a38395d0660281feba20d795b320bc','e0c7e0f2fb03025b22078a659f961711c7dd75f648c11fae655859cd993b55c6ec2ec20df075de8c',0,'2021-08-21 00:05:54'),
	('847b89140041c790a1690974150840bfdc3240fab3f0197d4b0e12c906d6054722f11527ad173708','6f68b7aaef9e24d82331bb617418262a6c3284dcac15ffd8b2001211241cd90b9816a955a10d9484',0,'2021-08-29 16:19:03'),
	('866a35d34b887e6f2d09510710fd2b1ca110a7b020dd4a777c01ef8dd27d7221e50c592a73926dc6','b64f5724debee6d38ff1b3de720f163c0618df4bc83d66b0455a32aa99db0692d3c9c6f681096156',0,'2021-08-24 00:10:36'),
	('867405768c71abfe1dedb3a3ee68e84650f3ee13c4836bad62a8007bd104a1d745f268df6c1993d1','63f57b0ebbe9754f0c7d7d07730099599fad824c5dd176f4a4a874f3898037c06470e2e5eb5326ed',0,'2021-08-24 14:34:23'),
	('8b0a09584ecccfde59adee30b5dd554aeff72b10dc4a0d4567754560c243b6b96cd4619ab07c414e','c2d5f10cd58efdcdd67cafc1e32f29e65724e72edd73998fb120590fd8aa05f6176e7193c0bc3725',0,'2021-08-20 13:30:01'),
	('8ca28b345734f043c804b9044056c10512158d5bf8e9cc4f204c9f7c4dfabc90c53b2a4a527bf29e','f3cdf41339afba0c33009458fb8ffa8417ec781de4ae79d5604ebb75d29eedcf3ff465f5d3bf4f3d',0,'2021-08-26 11:38:10'),
	('8e34ed64601b8c56392eac26beee3c6aa3ab9ce3e9ef3d4850a012e92597cf32069389b1fcc545cd','78134e6a515581f0cc2f4fb7e15168ebfecc460b145ab9807a8e4d83a7e4da41fd54b11a1a2bdc9f',0,'2021-08-26 23:11:00'),
	('9069839d98c14789704e98b80255a0670c8dbb13391083f29819f667e267d3097edd7c50f2b710ac','71aaa15a769c273e04cd22055cf1c4163eeb82d2be8cb9447e7c6e45d751c5bf51eaf990d0c97577',0,'2021-08-20 12:52:30'),
	('916b7998ecb6366787831b6c145fd5eb9286d8c1b172255b05afd85879995b4445327046b2f98382','853b1b1a4e277ae234d158897113b53eac03c576e58af17111793b0757234ce582f01c233bc72d63',0,'2021-08-24 11:07:22'),
	('9341be0618eb4a22dce61c28928cc4f3265ec313dd547e798c63bec9f84f8e0955844e7f5be86bff','a7fdd5e686cf40d7ce2aec8f57def2901e891019fdfa8ba11b035c4573c1b8f17a53aeb225fbb3f8',0,'2021-08-20 00:28:35'),
	('935d071ea3beaec799317348438b6ead5b866c681f29053435c9158dbe4ab0f3629c4ff11a500134','50f443904f72f157e99cd31c5548c8ee438c39b47aa42ef83242e885be0530af6416d71a5c60ad02',0,'2021-08-24 14:37:32'),
	('95d32b458acb7a695564da798601cce3d6f8f8a32b8ccc922b1de8589255803b05bb7ee58b181dae','c2a873c2027061c8a7aa983023bf07edfd27dbe0c6084e412816e0530215914f3e33f31df1fba1b9',0,'2021-08-26 21:43:26'),
	('aa5822fb8cd2b69e067cee4d51dd9f3147a3ae2e16411c47f80a76faad3126bd6ef7aa38fb1953f4','d42ad33336699328312f707c56f4573b70ad0a05efc0ede3128040a44a5a4bc43fb283f23c76c5bc',0,'2021-08-23 17:46:38'),
	('b6d7ad31735afba72b168eb10286eb614dbef9e86c2731d00b65d132b55bab47fa4d2b39ae4c67fd','6deae382f5d3e9dbc716d1632678df47b0135d9666efa44a6f7a7d19032e3ec7d33165b28fa8c7b3',0,'2021-08-24 00:04:54'),
	('c32477fa5b9a0f363a5abb34fc2f55279a87e67d4121bf0d1a2db047d03da6d793d7523435e255d7','ec816c8979bf70f389cbcc5dd9775b7038e435cb2170b459e68a15594337f46ec42a5e5763e6690d',0,'2021-08-21 10:24:25'),
	('cb9cc84ddf79056c09acb525c1a5e4119136dd06c28c7219163b930f9431c2b4731b3777b958a9f0','1685ce4a23fa30c6d5c5e42164b856e4d7b61ad77ed4097cff8925e53cd4a48063e045c47e2d919a',0,'2021-08-22 20:42:43'),
	('cc0b295b81dc642e43a37ddab19cc67bc06d482b317ed9e3e7c09a98c6ed87089ff8c584bcddcdd3','3b19d923c6fb3a6dfb6e6178d2185721a1e1615286bf58b74ef861521adb5257ca0bfa5cb1e8c70d',0,'2021-08-26 12:32:29'),
	('cf2a1753b90d813d98adf328b9bbcedf3ca7ba27b7ea30452b791610d6b567b05a30f42446fab937','fd4d9c0bfbc2f4e79809758336d6e02d880b3f557627bd8749388e10b6dcf06df7b42f3c7c125fec',0,'2021-08-20 02:29:18'),
	('d351523449b240fcc8ff24582dd4521d1a0ed0c664a76d53afa1e65c2cd7011f1886df1a3fa2e683','1683f02af02de7cb9917f1100292091c49f439d920653ac379f0f7f2e5d5ae570d8b6963e5c347a8',0,'2021-08-23 12:51:40'),
	('d386161c16f3fea2a4c654dab9e0b2723ba27e14d4bb4ae17cf2d1fed622e24cbe57d72804a919eb','80916cf6f4f6951a0cd76c40825a5afff4dcc918b9deb323e1872250d106519e838f931eed3f5cad',0,'2021-08-28 08:48:24'),
	('e3da1cabe7aa829d496817428ab2f365c5124bb68542e28faff79e932240dce00734e63633524867','f25d0c52fc30480f1461d6f7a700c9ddef2462539ff64a59e24aad2b2b1a8039e8d1810f8949e0c6',0,'2021-08-26 10:23:41'),
	('e925e6e3dd333787ebd6f92d5c37f01fc9f4392c59c46c938018c5c962bb7ad74014a2b1e563b97f','be8204c6ca08cd2e7f990da81a90859b756f9b83af31f793ede33ae5a1b31cbddb3224505c6e1f66',0,'2021-08-24 23:00:30'),
	('ea15bc22f8c97066ea83e328c6a850fceb9075664f76a4fe61b7ea31686c0e1d41e800aeeae71142','cb6441a25557758c9a5f13e5549fee947417a17d40284a83a82f487738ac5e40e75fd1bb412269e6',0,'2021-08-20 17:12:35'),
	('ef91caf7f6f45b4b691137dcd75c982190d52c055322842a2d6e965e63b947093caf0df15f22716f','af5585a23fb64879c608fe12619bef4d10315772b2d449b004b051a21ac786f5a7f266181d3a57c0',0,'2021-08-24 11:02:32'),
	('f1ca0ea578dc7fab5e62eda59319e884bf845f1a89db4f1df460feea0eebb89eee3c74260ca8e3ef','2510fe171bfd8c5ca39d24ead2bb7f4f09814c27c1ea46843109308340e4fad3bd104b8b0e139de4',0,'2021-08-24 00:04:14'),
	('f694ff200b39cf6c4cb916b2bef1ca8410f38a3e5f7d16e6f2ad66ce5ec46ebff66f15b77424dd2b','8d42e934e01faf2fc65511a713e326c380e62c8437f7cdbecb141c9ab4188a25372f975200eaa43f',0,'2021-08-26 11:18:21');

/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`id`, `name`, `display_name`, `guard_name`, `created_at`, `updated_at`)
VALUES
	(1,'View projects','Ver proyectos','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(2,'Create projects','Crear proyectos','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(3,'Update projects','Actualizar proyectos','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(4,'Delete projects','Eliminar proyectos','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(5,'View users','Ver usuarios','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(6,'Create users','Crear usuarios','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(7,'Update users','Actualizar usuarios','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(8,'Delete users','Eliminar usuarios','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(9,'View roles','Ver roles','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(10,'Create roles','Crear roles','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(11,'Update roles','Actualizar roles','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(12,'Delete roles','Eliminar roles','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(13,'View permissions','Ver permisos','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(14,'Update permissions','Actualizar permisos','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(15,'View project members','Ver miembros de proyecto','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(16,'Create project members','Crear miembros de proyecto','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(17,'Update project members','Actualizar miembros de proyecto','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(18,'Delete project members','Eliminar miembros de proyecto','api','2020-08-30 09:38:45','2020-08-30 09:38:45');

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `program`;

CREATE TABLE `program` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_program_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `program` WRITE;
/*!40000 ALTER TABLE `program` DISABLE KEYS */;

INSERT INTO `program` (`id`, `name`)
VALUES
	('9f996dec-988d-11ea-bb37-0242ac130002','DISEÑO GRÁFICO'),
	('9f9972d8-988d-11ea-bb37-0242ac130002','DISEÑO, COMUNICACIÓN Y NUEVOS MEDIOS '),
	('9f997404-988d-11ea-bb37-0242ac130002','EFICIENCIA ENERGÉTICA'),
	('9f9974ea-988d-11ea-bb37-0242ac130002','ELECTRÓNICA Y AUTOMATIZACIÓN'),
	('9f997684-988d-11ea-bb37-0242ac130002','ENERGÍA Y AMBIENTE'),
	('9f997742-988d-11ea-bb37-0242ac130002','ENSEÑANZA DE LENGUAS NATIVAS Y EXTRANJERAS'),
	('9f99780a-988d-11ea-bb37-0242ac130002','FÍSICA COMPUTACIONAL'),
	('9f997d28-988d-11ea-bb37-0242ac130002','FÍSICA DE LAS RADIACIONES'),
	('9f997e0e-988d-11ea-bb37-0242ac130002','INGENIERÍA DE PROCESOS INDUSTRIALES'),
	('9f997ed6-988d-11ea-bb37-0242ac130002','INGENIERÍA DE SOFTWARE'),
	('9f997f94-988d-11ea-bb37-0242ac130002','INGENIERÍA Y TECNOLOGÍA DE MATERIALES'),
	('9f998052-988d-11ea-bb37-0242ac130002','INNOVACIÓN EDUCATIVA'),
	('9f998106-988d-11ea-bb37-0242ac130002','INTELIGENCIA ARTIFICIAL'),
	('9f99853e-988d-11ea-bb37-0242ac130002','MANEJO Y APROVECHAMIENTO DE RECURSOS RENOVABLES'),
	('9f99861a-988d-11ea-bb37-0242ac130002','MODELADO Y SIMULACIÓN COMPUTACIONAL DE PROCESOS'),
	('9f9986d8-988d-11ea-bb37-0242ac130002','PROCESAMIENTO DIGITAL DE SEÑALES E IMÁGENES'),
	('9f998796-988d-11ea-bb37-0242ac130002','ROBÓTICA Y CONTROL'),
	('9f998854-988d-11ea-bb37-0242ac130002','SEGURIDAD DE SISTEMAS DE INFORMACIÓN'),
	('9f998ba6-988d-11ea-bb37-0242ac130002','SUSTENTABILIDAD Y NUEVAS TECNOLOGÍAS'),
	('9f998c82-988d-11ea-bb37-0242ac130002','TECNOLOGÍA EDUCATIVA'),
	('9f998d4a-988d-11ea-bb37-0242ac130002','TECNOLOGÍA VEHICULAR'),
	('9f998e08-988d-11ea-bb37-0242ac130002','TELECOMUNICACIONES Y REDES'),
	('d6324148-988e-11ea-bb37-0242ac130002','TELEFONÍA PARA EL DESARROLLO');

/*!40000 ALTER TABLE `program` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla program_has_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `program_has_groups`;

CREATE TABLE `program_has_groups` (
  `program_id` varchar(36) NOT NULL DEFAULT '',
  `id_group` varchar(36) NOT NULL,
  PRIMARY KEY (`program_id`,`id_group`),
  KEY `fk_program_has_groups_groups1_idx` (`id_group`),
  KEY `fk_program_has_groups_program1_idx` (`program_id`),
  CONSTRAINT `fk_program_has_groups_groups1` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id_group`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_program_has_groups_program1` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `program_has_groups` WRITE;
/*!40000 ALTER TABLE `program_has_groups` DISABLE KEYS */;

INSERT INTO `program_has_groups` (`program_id`, `id_group`)
VALUES
	('9f9972d8-988d-11ea-bb37-0242ac130002','6773020e-c95f-11ea-bdac-6003088c543c'),
	('9f997404-988d-11ea-bb37-0242ac130002','6773020e-c95f-11ea-bdac-6003088c543c'),
	('9f996dec-988d-11ea-bb37-0242ac130002','a9fe3ba6-98b9-11ea-bb37-0242ac130002'),
	('9f997404-988d-11ea-bb37-0242ac130002','a9fe3ba6-98b9-11ea-bb37-0242ac130002'),
	('9f997742-988d-11ea-bb37-0242ac130002','a9fe3ba6-98b9-11ea-bb37-0242ac130002'),
	('9f9974ea-988d-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('9f9974ea-988d-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('9f997ed6-988d-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('9f997f94-988d-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('9f998052-988d-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('9f9972d8-988d-11ea-bb37-0242ac130002','a9fe402e-98b9-11ea-bb37-0242ac130002'),
	('9f996dec-988d-11ea-bb37-0242ac130002','a9fe422c-98b9-11ea-bb37-0242ac130002'),
	('9f9974ea-988d-11ea-bb37-0242ac130002','a9fe4308-98b9-11ea-bb37-0242ac130002'),
	('9f99780a-988d-11ea-bb37-0242ac130002','a9fe4308-98b9-11ea-bb37-0242ac130002'),
	('9f9974ea-988d-11ea-bb37-0242ac130002','a9fe4524-98b9-11ea-bb37-0242ac130002'),
	('9f997404-988d-11ea-bb37-0242ac130002','a9fe4696-98b9-11ea-bb37-0242ac130002'),
	('9f997684-988d-11ea-bb37-0242ac130002','a9fe4696-98b9-11ea-bb37-0242ac130002'),
	('9f997684-988d-11ea-bb37-0242ac130002','a9fe4754-98b9-11ea-bb37-0242ac130002'),
	('9f997684-988d-11ea-bb37-0242ac130002','a9fe4808-98b9-11ea-bb37-0242ac130002'),
	('9f997404-988d-11ea-bb37-0242ac130002','a9fe48bc-98b9-11ea-bb37-0242ac130002'),
	('9f997404-988d-11ea-bb37-0242ac130002','a9fe497a-98b9-11ea-bb37-0242ac130002');

/*!40000 ALTER TABLE `program_has_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(500) NOT NULL,
  `slug` varchar(60) DEFAULT '',
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `endDateReal` date DEFAULT NULL COMMENT 'Sera la fecha final en la que realmente finalizó',
  `location` varchar(300) DEFAULT NULL,
  `project_type_id` varchar(36) DEFAULT NULL,
  `research_type_id` varchar(36) DEFAULT NULL,
  `coverage_type_id` varchar(36) DEFAULT NULL,
  `program_id` varchar(36) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `father` varchar(36) DEFAULT NULL,
  `id_group` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `excerpt` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_project_projectType1_idx` (`project_type_id`),
  KEY `fk_project_researchType1_idx` (`research_type_id`),
  KEY `fk_project_project1_idx` (`father`),
  KEY `fk_project_cverageType_idx` (`coverage_type_id`),
  KEY `fk_project_group1_idx` (`id_group`),
  KEY `project_ibfk_1_idx` (`program_id`),
  CONSTRAINT `fk_project_cverageType` FOREIGN KEY (`coverage_type_id`) REFERENCES `coverage_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_projectType1` FOREIGN KEY (`project_type_id`) REFERENCES `project_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_researchType1` FOREIGN KEY (`research_type_id`) REFERENCES `research_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`id`, `code`, `name`, `slug`, `startDate`, `endDate`, `endDateReal`, `location`, `project_type_id`, `research_type_id`, `coverage_type_id`, `program_id`, `year`, `father`, `id_group`, `created_at`, `updated_at`, `deleted_at`, `excerpt`)
VALUES
	('8290e96c-eace-11ea-9f72-6003088c543c',NULL,'Mi primer proyecto','mi-primer-proyecto',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,'2020-08-30 09:38:46','2020-08-30 09:38:46',NULL,NULL),
	('8296d2d2-eace-11ea-b92c-6003088c543c',NULL,'Mi segundo proyecto','mi-segundo-proyecto',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,'2020-08-30 09:38:46','2020-08-30 09:38:46',NULL,NULL),
	('8297a572-eace-11ea-835f-6003088c543c',NULL,'Mi tercer proyecto','mi-tercer-proyecto',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,'2020-08-30 09:38:46','2020-08-30 09:38:46',NULL,NULL),
	('8297f4be-eace-11ea-899b-6003088c543c',NULL,'Mi Cuarto proyecto','mi-cuarto-proyecto',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,'2020-08-30 09:38:46','2020-08-30 09:38:46',NULL,NULL);

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla project_has_dependency
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_dependency`;

CREATE TABLE `project_has_dependency` (
  `id_project` varchar(36) NOT NULL,
  `id_dependency` varchar(36) NOT NULL,
  `date_projectHasDependency` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type_Dependency` varchar(45) DEFAULT NULL COMMENT 'la dependencia en este proyecto es PRINCIPAL o MIEMBRO',
  PRIMARY KEY (`id_project`,`id_dependency`),
  KEY `fk_project_has_managementArea_project1_idx` (`id_project`),
  KEY `fk_project_has_managementArea_managementArea1_idx` (`id_dependency`),
  CONSTRAINT `fk_project_has_managementArea_managementArea1` FOREIGN KEY (`id_dependency`) REFERENCES `dependency` (`id_dependency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_managementArea_project1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla project_has_researchline
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_researchline`;

CREATE TABLE `project_has_researchline` (
  `id_project` varchar(36) NOT NULL,
  `id_researchLine` varchar(36) NOT NULL,
  PRIMARY KEY (`id_project`,`id_researchLine`),
  KEY `fk_project_has_researchline_researchline1_idx` (`id_researchLine`),
  KEY `fk_project_has_researchline_project1_idx` (`id_project`),
  CONSTRAINT `fk_project_has_researchline_project1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_researchline_researchline1` FOREIGN KEY (`id_researchLine`) REFERENCES `researchline` (`id_researchLine`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla project_has_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_status`;

CREATE TABLE `project_has_status` (
  `project_id` varchar(36) NOT NULL,
  `status_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`project_id`,`status_id`),
  KEY `fk_project_has_status_status1_idx` (`status_id`),
  KEY `fk_project_has_status_project1_idx` (`project_id`),
  CONSTRAINT `fk_project_has_status_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_status_status1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla project_has_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_users`;

CREATE TABLE `project_has_users` (
  `project_id` varchar(36) NOT NULL DEFAULT '',
  `user_id` varchar(36) NOT NULL DEFAULT '',
  `staff_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `fk_project_has_users_users1_idx` (`user_id`),
  KEY `fk_project_has_users_project1_idx` (`project_id`),
  KEY `fk_project_has_users_staff` (`staff_id`),
  CONSTRAINT `fk_project_has_users_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_users_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`),
  CONSTRAINT `fk_project_has_users_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project_has_users` WRITE;
/*!40000 ALTER TABLE `project_has_users` DISABLE KEYS */;

INSERT INTO `project_has_users` (`project_id`, `user_id`, `staff_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	('8290e96c-eace-11ea-9f72-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-08-30 09:38:46','2020-08-30 09:38:46',NULL),
	('8296d2d2-eace-11ea-b92c-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-08-30 09:38:46','2020-08-30 09:38:46',NULL),
	('8297a572-eace-11ea-835f-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-08-30 09:38:46','2020-08-30 09:38:46',NULL),
	('8297f4be-eace-11ea-899b-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-08-30 09:38:46','2020-08-30 09:38:46',NULL);

/*!40000 ALTER TABLE `project_has_users` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla project_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_type`;

CREATE TABLE `project_type` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project_type` WRITE;
/*!40000 ALTER TABLE `project_type` DISABLE KEYS */;

INSERT INTO `project_type` (`id`, `name`)
VALUES
	('f9ae248c-9886-11ea-bb37-0242ac130002','INVESTIGACION'),
	('f9ae26da-9886-11ea-bb37-0242ac130002','VINCULACION');

/*!40000 ALTER TABLE `project_type` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla requeriment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `requeriment`;

CREATE TABLE `requeriment` (
  `id_requeriment` int(11) NOT NULL AUTO_INCREMENT,
  `name_requeriment` varchar(255) NOT NULL,
  `id_activity` varchar(36) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_requeriment`),
  KEY `fk_requeriment_activity1_idx` (`id_activity`),
  CONSTRAINT `fk_requeriment_activity1` FOREIGN KEY (`id_activity`) REFERENCES `activity` (`id_activity`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla research_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `research_type`;

CREATE TABLE `research_type` (
  `id` varchar(36) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `research_type` WRITE;
/*!40000 ALTER TABLE `research_type` DISABLE KEYS */;

INSERT INTO `research_type` (`id`, `name`)
VALUES
	('16429ac6-9867-11ea-afcb-820f047d9840','INVESTIGACION CIENTIFICA'),
	('39cfe0ee-986b-11ea-8571-820f047d9840','DESARROLLO TECNOLOGICO'),
	('7f212b12-986b-11ea-8d7c-820f047d9840','INNOVACION TECNOLOGICA');

/*!40000 ALTER TABLE `research_type` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla researchcenter
# ------------------------------------------------------------

DROP TABLE IF EXISTS `researchcenter`;

CREATE TABLE `researchcenter` (
  `id_researchCenter` int(11) NOT NULL AUTO_INCREMENT,
  `name_researchCenter` varchar(255) NOT NULL,
  PRIMARY KEY (`id_researchCenter`),
  UNIQUE KEY `name_researchGroup_UNIQUE` (`name_researchCenter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla researchline
# ------------------------------------------------------------

DROP TABLE IF EXISTS `researchline`;

CREATE TABLE `researchline` (
  `id_researchLine` varchar(36) NOT NULL,
  `name_researchLine` varchar(255) NOT NULL,
  PRIMARY KEY (`id_researchLine`),
  UNIQUE KEY `name_researchLine_UNIQUE` (`name_researchLine`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `researchline` WRITE;
/*!40000 ALTER TABLE `researchline` DISABLE KEYS */;

INSERT INTO `researchline` (`id_researchLine`, `name_researchLine`)
VALUES
	('ea46431e-98bb-11ea-bb37-0242ac130002','ADMINISTRACIÓN Y ECONOMÍA POPULAR'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','ARTE CULTURA Y PATRIMONIO'),
	('ea4645f8-98bb-11ea-bb37-0242ac130002','CIENCIAS BÁSICAS Y APLICADAS'),
	('ea464800-98bb-11ea-bb37-0242ac130002','ENERGÍAS RENOVABLES Y PROTECCIÓN AMBIENTAL'),
	('ea4648d2-98bb-11ea-bb37-0242ac130002','GESTIÓN Y MANEJO SUSTENTABLES DE LOS RECURSOS NATURALES'),
	('ea46499a-98bb-11ea-bb37-0242ac130002','PROCESOS TECNOLÓGICOS ARTESANALES E INDUSTRIALES'),
	('ea464a58-98bb-11ea-bb37-0242ac130002','SALUD Y NUTRICIÓN'),
	('ea464b16-98bb-11ea-bb37-0242ac130002','TECNOLOGÍAS DE LA INFORMACIÓN, COMUNICACIÓN');

/*!40000 ALTER TABLE `researchline` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla researchline_has_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `researchline_has_groups`;

CREATE TABLE `researchline_has_groups` (
  `id_researchLine` varchar(36) NOT NULL,
  `id_group` varchar(36) NOT NULL,
  PRIMARY KEY (`id_researchLine`,`id_group`),
  KEY `fk_researchline_has_groups_groups1_idx` (`id_group`),
  KEY `fk_researchline_has_groups_researchline1_idx` (`id_researchLine`),
  CONSTRAINT `fk_researchline_has_groups_groups1` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id_group`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_researchline_has_groups_researchline1` FOREIGN KEY (`id_researchLine`) REFERENCES `researchline` (`id_researchLine`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `researchline_has_groups` WRITE;
/*!40000 ALTER TABLE `researchline_has_groups` DISABLE KEYS */;

INSERT INTO `researchline_has_groups` (`id_researchLine`, `id_group`)
VALUES
	('ea46431e-98bb-11ea-bb37-0242ac130002','6773020e-c95f-11ea-bdac-6003088c543c'),
	('ea4645f8-98bb-11ea-bb37-0242ac130002','6773020e-c95f-11ea-bdac-6003088c543c'),
	('ea464800-98bb-11ea-bb37-0242ac130002','6773020e-c95f-11ea-bdac-6003088c543c'),
	('ea46431e-98bb-11ea-bb37-0242ac130002','a9fe3ba6-98b9-11ea-bb37-0242ac130002'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','a9fe3ba6-98b9-11ea-bb37-0242ac130002'),
	('ea4645f8-98bb-11ea-bb37-0242ac130002','a9fe3ba6-98b9-11ea-bb37-0242ac130002'),
	('ea46431e-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea4645f8-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea4648d2-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea46499a-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea464a58-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea464b16-98bb-11ea-bb37-0242ac130002','a9fe3da4-98b9-11ea-bb37-0242ac130002'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea4645f8-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea4648d2-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea46499a-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea464a58-98bb-11ea-bb37-0242ac130002','a9fe3e94-98b9-11ea-bb37-0242ac130002'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe3f66-98b9-11ea-bb37-0242ac130002'),
	('ea46431e-98bb-11ea-bb37-0242ac130002','a9fe402e-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe422c-98b9-11ea-bb37-0242ac130002'),
	('ea4648d2-98bb-11ea-bb37-0242ac130002','a9fe4308-98b9-11ea-bb37-0242ac130002'),
	('ea46499a-98bb-11ea-bb37-0242ac130002','a9fe4308-98b9-11ea-bb37-0242ac130002'),
	('ea46431e-98bb-11ea-bb37-0242ac130002','a9fe4524-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe4696-98b9-11ea-bb37-0242ac130002'),
	('ea464a58-98bb-11ea-bb37-0242ac130002','a9fe4696-98b9-11ea-bb37-0242ac130002'),
	('ea46431e-98bb-11ea-bb37-0242ac130002','a9fe4754-98b9-11ea-bb37-0242ac130002'),
	('ea4644fe-98bb-11ea-bb37-0242ac130002','a9fe4754-98b9-11ea-bb37-0242ac130002'),
	('ea46499a-98bb-11ea-bb37-0242ac130002','a9fe4808-98b9-11ea-bb37-0242ac130002'),
	('ea464800-98bb-11ea-bb37-0242ac130002','a9fe48bc-98b9-11ea-bb37-0242ac130002'),
	('ea464b16-98bb-11ea-bb37-0242ac130002','a9fe48bc-98b9-11ea-bb37-0242ac130002'),
	('ea464a58-98bb-11ea-bb37-0242ac130002','a9fe497a-98b9-11ea-bb37-0242ac130002');

/*!40000 ALTER TABLE `researchline_has_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla role_has_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role_has_permissions`;

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`)
VALUES
	(2,7),
	(3,7),
	(5,7),
	(9,7),
	(13,7);

/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `display_name`, `guard_name`, `created_at`, `updated_at`)
VALUES
	(1,'Cimogsys','Cimogsys','api','2020-08-30 09:38:44','2020-08-30 09:38:44'),
	(2,'Rector','Rector','api','2020-08-30 09:38:44','2020-08-30 09:38:44'),
	(3,'Vicerrector','Vicerrector','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(4,'Director IDI','Director IDI','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(5,'Decano','Decano','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(6,'Subdecano','Subdecano','api','2020-08-30 09:38:45','2020-08-30 09:38:45'),
	(7,'Investigador','Investigador','api','2020-08-30 09:38:45','2020-08-30 09:38:45');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla staff
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `display_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;

INSERT INTO `staff` (`id`, `name`, `display_name`)
VALUES
	(1,'Investigador responsable','Investigador responsable'),
	(2,'Investigador miembro','Investigador miembro'),
	(3,'Técnico','Técnico'),
	(4,'Administrativo','Administrativo'),
	(5,'Tesista','Tesista'),
	(6,'Practicante','Practicante');

/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;

INSERT INTO `status` (`id`, `name`)
VALUES
	(1,'Iniciado'),
	(2,'Modificado'),
	(3,'Enviado a director'),
	(4,'Retornado a modificar'),
	(5,'Enviado a decanato'),
	(6,'Enviado a IDI');

/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` varchar(36) CHARACTER SET utf8 NOT NULL,
  `identification_card` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `identification_card`, `name`, `lastname`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	('1d1d9a0a-e2b6-11ea-9775-6003088c543c','0603997305','Cristian','Guamán','cristian.guaman@espoch.edu.ec',NULL,'$2y$10$0lYl18.RssmOsYEdfUw1HejlSp9JZNtCo23FrHeCAjdE3BvEwfoCS',NULL,'2020-08-30 09:38:45','2020-08-30 09:38:45',NULL),
	('1d29c8e8-e2b6-11ea-abc9-6003088c543c','1701098212','Naizly','Silva','naizly.silva@espoch.edu.ec',NULL,'$2y$10$87GWpYitJeNYkIFwDkFSa.jZ3v1zz8uwNIllal5sMTCixXtgGVAJu',NULL,'2020-08-30 09:38:45','2020-08-30 09:38:45',NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
