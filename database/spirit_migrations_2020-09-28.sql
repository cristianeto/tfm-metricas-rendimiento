# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.31)
# Base de datos: spirit_migrations
# Tiempo de Generación: 2020-09-28 15:16:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla activities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `activities`;

CREATE TABLE `activities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `component_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activities_component_id_foreign` (`component_id`),
  CONSTRAINT `activities_component_id_foreign` FOREIGN KEY (`component_id`) REFERENCES `components` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;

INSERT INTO `activities` (`id`, `name`, `start_date`, `end_date`, `component_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(288,'Ea aut at itaque porro voluptatem similique fuga.','2020-09-27','2020-09-27',1,'2020-09-27 12:52:48','2020-09-27 12:52:48',NULL),
	(289,'In enim nihil aut incidunt totam temporibus ipsa voluptas nam amet aut voluptates animi soluta omnis tempora.','2020-09-27','2020-09-27',1,'2020-09-27 12:52:48','2020-09-27 12:52:48',NULL),
	(290,'Iure rem impedit aut iure sint et atque doloribus sapiente dolor asperiores.','2020-09-27','2020-09-27',1,'2020-09-27 12:52:48','2020-09-27 12:52:48',NULL),
	(291,'Voluptas quaerat perferendis eaque architecto quo possimus repellendus ab explicabo ducimus mollitia.','2020-09-27','2020-09-27',1,'2020-09-27 12:52:48','2020-09-27 12:52:48',NULL),
	(292,'Ipsam cum est omnis laboriosam aliquid omnis exercitationem non nostrum mollitia ad adipisci quis dolor fuga.','2020-09-27','2020-09-27',1,'2020-09-27 12:52:48','2020-09-27 12:52:48',NULL),
	(293,'Facilis at cumque et illo corrupti dicta maxime ex est sed.','2020-09-27','2020-09-27',1,'2020-09-27 12:52:48','2020-09-27 12:52:48',NULL),
	(294,'Maiores ea laboriosam necessitatibus velit ad aut quia et inventore repellat porro ut.','2020-09-27','2020-09-27',54,'2020-09-27 12:53:02','2020-09-27 12:53:02',NULL),
	(295,'Cumque sit repellendus voluptates possimus provident consequatur pariatur.','2020-09-27','2020-09-27',54,'2020-09-27 12:53:02','2020-09-27 12:53:02',NULL),
	(296,'Soluta impedit sed quis fugit dicta molestias reiciendis vel minus labore explicabo autem.','2020-09-27','2020-09-27',55,'2020-09-27 12:53:06','2020-09-27 12:53:06',NULL),
	(297,'Eaque optio ad velit quis eaque ea ut voluptate qui quam est provident aspernatur asperiores optio quod.','2020-09-27','2020-09-27',55,'2020-09-27 12:53:06','2020-09-27 12:53:06',NULL);

/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla components
# ------------------------------------------------------------

DROP TABLE IF EXISTS `components`;

CREATE TABLE `components` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `components_project_id_foreign` (`project_id`),
  CONSTRAINT `components_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `components` WRITE;
/*!40000 ALTER TABLE `components` DISABLE KEYS */;

INSERT INTO `components` (`id`, `name`, `project_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'1er Componente','171118f4-002e-11eb-83d4-6003088c543c','2020-09-26 14:25:52','2020-09-26 14:25:52',NULL),
	(2,'1st component','171cd734-002e-11eb-bb2a-6003088c543c','2020-09-26 16:48:01','2020-09-26 16:48:01',NULL),
	(3,'2do component','171cd734-002e-11eb-bb2a-6003088c543c','2020-09-26 16:50:02','2020-09-26 16:50:02',NULL),
	(4,'Conseguir multiples capacitadores en el area de','171cd734-002e-11eb-bb2a-6003088c543c','2020-09-26 16:55:19','2020-09-26 16:55:19',NULL),
	(5,'ESCUELA SUPERIOR POLITECNICA DE CHIMBORAZO','171cd734-002e-11eb-bb2a-6003088c543c','2020-09-26 16:55:31','2020-09-26 16:55:31',NULL),
	(6,'matrix','171cd734-002e-11eb-bb2a-6003088c543c','2020-09-26 18:04:38','2020-09-26 18:04:38',NULL),
	(7,'Liberación de productos en el servidor','171cd734-002e-11eb-bb2a-6003088c543c','2020-09-26 18:05:44','2020-09-26 18:05:44',NULL),
	(8,'meterorio','171cd734-002e-11eb-bb2a-6003088c543c','2020-09-26 18:05:54','2020-09-26 18:05:54',NULL),
	(54,'2do componente','171118f4-002e-11eb-83d4-6003088c543c','2020-09-26 22:00:13','2020-09-26 22:00:13',NULL),
	(55,'3er componente','171118f4-002e-11eb-83d4-6003088c543c','2020-09-26 22:09:43','2020-09-26 22:09:43',NULL);

/*!40000 ALTER TABLE `components` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla coverage_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coverage_type`;

CREATE TABLE `coverage_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `coverage_type` WRITE;
/*!40000 ALTER TABLE `coverage_type` DISABLE KEYS */;

INSERT INTO `coverage_type` (`id`, `name`)
VALUES
	(1,'INTERNACIONAL'),
	(2,'NACIONAL'),
	(3,'REGIONAL'),
	(4,'PROVINCIAL'),
	(5,'CANTONAL'),
	(6,'PARROQUIAL');

/*!40000 ALTER TABLE `coverage_type` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla dependency
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dependency`;

CREATE TABLE `dependency` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acronym` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dependency_type_id` bigint(20) unsigned NOT NULL,
  `father` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dependency_father_foreign` (`father`),
  KEY `dependency_dependency_type_id_foreign` (`dependency_type_id`),
  CONSTRAINT `dependency_dependency_type_id_foreign` FOREIGN KEY (`dependency_type_id`) REFERENCES `dependency_type` (`id`),
  CONSTRAINT `dependency_father_foreign` FOREIGN KEY (`father`) REFERENCES `dependency` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `dependency` WRITE;
/*!40000 ALTER TABLE `dependency` DISABLE KEYS */;

INSERT INTO `dependency` (`id`, `name`, `acronym`, `contact`, `city`, `email`, `web`, `phone`, `dependency_type_id`, `father`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'ESCUELA SUPERIOR POLITÉCNICA DE CHIMBORAZO','ESPOCH',NULL,NULL,'info@espoch.edu.ec',NULL,NULL,4,NULL,'2020-09-26 14:25:52','2020-09-26 14:25:52',NULL),
	(2,'FACULTAD DE INFORMÁTICA Y ELECTRÓNICA','FIE',NULL,NULL,'fie@espoch.edu.ec',NULL,NULL,3,NULL,'2020-09-26 14:25:52','2020-09-26 14:25:52',NULL),
	(3,'FACULTAD ADMINISTRACIÓN DE EMPRESAS','FADE',NULL,NULL,'fade@espoch.edu.ec',NULL,NULL,3,NULL,'2020-09-26 14:25:52','2020-09-26 14:25:52',NULL);

/*!40000 ALTER TABLE `dependency` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla dependency_has_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dependency_has_users`;

CREATE TABLE `dependency_has_users` (
  `dependency_id` bigint(20) unsigned NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`dependency_id`,`user_id`),
  KEY `dependency_has_users_user_id_foreign` (`user_id`),
  CONSTRAINT `dependency_has_users_dependency_id_foreign` FOREIGN KEY (`dependency_id`) REFERENCES `dependency` (`id`),
  CONSTRAINT `dependency_has_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla dependency_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dependency_type`;

CREATE TABLE `dependency_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `dependency_type` WRITE;
/*!40000 ALTER TABLE `dependency_type` DISABLE KEYS */;

INSERT INTO `dependency_type` (`id`, `name`)
VALUES
	(1,'CARRERA'),
	(2,'EXTERNA'),
	(3,'FACULTAD'),
	(4,'INSTITUCIONAL'),
	(5,'UNIDAD ACADÉMICA'),
	(6,'UNIDAD ADMINISTRATIVA');

/*!40000 ALTER TABLE `dependency_type` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla group_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_type`;

CREATE TABLE `group_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `group_type` WRITE;
/*!40000 ALTER TABLE `group_type` DISABLE KEYS */;

INSERT INTO `group_type` (`id`, `name`)
VALUES
	(1,'INVESTIGACIÓN'),
	(2,'VINCULACIÓN');

/*!40000 ALTER TABLE `group_type` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acronym` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mission` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Sin misión',
  `vision` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Sin visión',
  `dependency_id` bigint(20) unsigned NOT NULL,
  `group_type_id` bigint(20) unsigned NOT NULL,
  `research_center_id` bigint(20) unsigned DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `groups_dependency_id_foreign` (`dependency_id`),
  KEY `groups_group_type_id_foreign` (`group_type_id`),
  KEY `groups_research_center_id_foreign` (`research_center_id`),
  CONSTRAINT `groups_dependency_id_foreign` FOREIGN KEY (`dependency_id`) REFERENCES `dependency` (`id`),
  CONSTRAINT `groups_group_type_id_foreign` FOREIGN KEY (`group_type_id`) REFERENCES `group_type` (`id`),
  CONSTRAINT `groups_research_center_id_foreign` FOREIGN KEY (`research_center_id`) REFERENCES `research_center` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `code`, `name`, `acronym`, `mission`, `vision`, `dependency_id`, `group_type_id`, `research_center_id`, `active`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	('174b382c-002e-11eb-9f7d-6003088c543c','FIEGI-010','GRUPO DE INVESTIGACIÓN E INTERACCIÓN EN LAS TECNOLOGÍAS DE LA COMUNICACIÓN','IITC','Sin misión','Sin visión',2,1,NULL,1,'2020-09-26 14:25:52','2020-09-26 14:25:52',NULL);

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla groups_has_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups_has_users`;

CREATE TABLE `groups_has_users` (
  `group_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`group_id`,`user_id`),
  KEY `groups_has_users_user_id_foreign` (`user_id`),
  CONSTRAINT `groups_has_users_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  CONSTRAINT `groups_has_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla impact_sector
# ------------------------------------------------------------

DROP TABLE IF EXISTS `impact_sector`;

CREATE TABLE `impact_sector` (
  `id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `impact_sector` WRITE;
/*!40000 ALTER TABLE `impact_sector` DISABLE KEYS */;

INSERT INTO `impact_sector` (`id`, `name`)
VALUES
	('343a84ca-aec9-3658-8c9c-e4e4b5ebe4b1','ENERGÍA'),
	('483892c6-2de6-30d8-9295-231b87ddd68f','DESARROLLO HUMANO Y SOCIAL'),
	('8c37c923-4dbb-327c-bb8a-d257ea774b82','TECNOLOGÍA DE LA INFOMRACIÓN Y COMUNICACIÓN '),
	('b3aaa72b-b152-38f9-aa5a-f8f53944e5d8','FOMENTO AGROPECUARIO Y DESARR PRODUCTIVO'),
	('b8655987-a90b-3a60-8110-fbb8ad444e73','RECURSOS NATURALES'),
	('f7c6bffa-dec3-371a-a66d-64dfcc4b884b','BIODIVERSIDAD Y AMBIENTE');

/*!40000 ALTER TABLE `impact_sector` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla impactsector_has_project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `impactsector_has_project`;

CREATE TABLE `impactsector_has_project` (
  `impact_sector_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `project_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`impact_sector_id`,`project_id`),
  KEY `impactsector_has_project_project_id_foreign` (`project_id`),
  CONSTRAINT `impactsector_has_project_impact_sector_id_foreign` FOREIGN KEY (`impact_sector_id`) REFERENCES `impact_sector` (`id`),
  CONSTRAINT `impactsector_has_project_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `impactsector_has_project` WRITE;
/*!40000 ALTER TABLE `impactsector_has_project` DISABLE KEYS */;

INSERT INTO `impactsector_has_project` (`impact_sector_id`, `project_id`)
VALUES
	('483892c6-2de6-30d8-9295-231b87ddd68f','171118f4-002e-11eb-83d4-6003088c543c'),
	('8c37c923-4dbb-327c-bb8a-d257ea774b82','171118f4-002e-11eb-83d4-6003088c543c'),
	('343a84ca-aec9-3658-8c9c-e4e4b5ebe4b1','171cd734-002e-11eb-bb2a-6003088c543c'),
	('b3aaa72b-b152-38f9-aa5a-f8f53944e5d8','171cd734-002e-11eb-bb2a-6003088c543c'),
	('6c032f63-b44a-3c58-abd8-0586b04a3f99','69c21064-fe97-11ea-9ac2-6003088c543c'),
	('7f11cfaf-faa6-3861-8eda-e92003acc863','69c21064-fe97-11ea-9ac2-6003088c543c'),
	('9540f6f1-ff83-3e64-adc8-c1749a74ab3e','69c21064-fe97-11ea-9ac2-6003088c543c'),
	('6089d744-c959-3d4b-9f6d-e1d2aa5033e1','d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea'),
	('64777a02-9a61-36e9-bce2-1aa694a02a10','d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea');

/*!40000 ALTER TABLE `impactsector_has_project` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2016_06_01_000001_create_oauth_auth_codes_table',1),
	(2,'2016_06_01_000002_create_oauth_access_tokens_table',1),
	(3,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),
	(4,'2016_06_01_000004_create_oauth_clients_table',1),
	(5,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),
	(6,'2019_08_19_000000_create_failed_jobs_table',1),
	(7,'2020_08_19_234224_create_permission_tables',1),
	(8,'2020_09_03_166864_create_coverage_type_table',1),
	(9,'2020_09_03_166899_create_dependencytype_table',1),
	(10,'2020_09_03_166921_create_grouptype_table',1),
	(11,'2020_09_03_166945_create_impact_sector_table',1),
	(12,'2020_09_03_166991_create_program_table',1),
	(13,'2020_09_03_167065_create_project_type_table',1),
	(14,'2020_09_03_167092_create_research_type_table',1),
	(15,'2020_09_03_167105_create_researchcenter_table',1),
	(16,'2020_09_03_167120_create_researchline_table',1),
	(17,'2020_09_03_167139_create_staff_table',1),
	(18,'2020_09_03_167185_create_users_table',1),
	(19,'2020_09_03_167318_create_dependency_table',1),
	(20,'2020_09_03_167412_create_dependency_has_users_table',1),
	(21,'2020_09_03_167572_create_groups_table',1),
	(22,'2020_09_03_167605_create_groups_has_users_table',1),
	(23,'2020_09_03_167647_create_program_has_groups_table',1),
	(24,'2020_09_03_167759_create_project_table',1),
	(25,'2020_09_03_168052_create_impactsector_has_project_table',1),
	(26,'2020_09_03_168084_create_project_has_dependency_table',1),
	(27,'2020_09_03_168098_create_project_has_researchline_table',1),
	(28,'2020_09_03_168134_create_project_has_users_table',1),
	(29,'2020_09_03_168177_create_researchline_has_groups_table',1),
	(30,'2020_09_03_220116_create_components_table',1),
	(31,'2020_09_03_233952_create_status_table',1),
	(32,'2020_09_03_234154_create_project_status_table',1),
	(36,'2020_09_26_202954_create_activities_table',2),
	(37,'2020_09_27_002425_create_requirements_table',3);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla model_has_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model_has_permissions`;

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla model_has_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `model_has_roles`;

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`)
VALUES
	(1,'App\\Models\\User','1d1d9a0a-e2b6-11ea-9775-6003088c543c'),
	(7,'App\\Models\\User','1d29c8e8-e2b6-11ea-abc9-6003088c543c');

/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla oauth_access_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_access_tokens`;

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`)
VALUES
	('00662e3c73d77b8b61346dbe05a1f49b368bf6467e5a77e13460bd44daa95e48841cd64175e11f1b','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-25 18:03:20','2020-09-25 18:03:20','2021-09-25 18:03:20'),
	('0b3d2015124ecaf98ad7d98b36a841d186ef594f0d14909ba558e3894a2c38fa4d0de1c4d5f974e3','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 15:32:53','2020-09-24 15:32:53','2021-09-24 15:32:53'),
	('0b9df789c5afb074afd30e1ea770f9919bb90dce8af7c0ec3039e42231a8b853b4402a05acee2cfd','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 15:38:54','2020-09-24 15:38:54','2021-09-24 15:38:54'),
	('1c6eec7a57414b344984f86f7d53bb7344131e0663489908b4e2dd33422a7adbe16c936bcb7fce2b','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 19:33:32','2020-09-24 19:33:32','2021-09-24 19:33:32'),
	('22948c78bbf6796d231f46f4d4f98eaa3ca9493c888115075264d5ab04e9e632ae414c4418771f61','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 20:47:34','2020-09-24 20:47:34','2021-09-24 20:47:34'),
	('248429687b90d47845e24bb0f59ca282933a8186ce4df4025ef0b61300adac0cdcbd8c456d360022','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-26 12:27:59','2020-09-26 12:27:59','2021-09-26 12:27:59'),
	('3643fa1f68891386bd4b88bef39db5df06f247e4ff24af37b81da346b5155489ae009cb1d9b35e4f','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-26 13:12:34','2020-09-26 13:12:34','2021-09-26 13:12:34'),
	('3dd2fc8ad193a4fd2a5a3beb153a902f820121734a3ca6e989f128c03fd1079d9dcd2acb5afff598','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 20:43:18','2020-09-24 20:43:18','2021-09-24 20:43:18'),
	('44e8a8b256f74ac9f60b476f66fd8079f5abc820001d995a74e447c14264bfe0dc7cc1cc88222af6','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-26 19:56:47','2020-09-26 19:56:47','2021-09-26 19:56:47'),
	('46c1305044ec220aa35c87bce9ed53db7869411ea86c6bf104067b34cf4e3c0a32e1d35aa979cbcc','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-28 09:53:42','2020-09-28 09:53:42','2021-09-28 09:53:42'),
	('55291c1b7461b1a93dd2470c465f61ac6e8ca84b9e7e3536fd7ac1db1a927b2492d6ced4fdac349b','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-27 00:08:14','2020-09-27 00:08:14','2021-09-27 00:08:14'),
	('552a1deb9be9ca83b6fd7ef17ccf5adf059d17cb8ff940d92eecddf801088580a32217559367b9ba','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-27 00:07:49','2020-09-27 00:07:49','2021-09-27 00:07:49'),
	('5b179c68ff88b1791e78c47252491d4eb175bede61baea352b4b8ebc0d1a9e99d53a3633d1a3dd39','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 20:48:41','2020-09-24 20:48:41','2021-09-24 20:48:41'),
	('5f9bc993898fa491dffb77c5a7329a016a5c2a0583b15f5dbbd5b69bde0550367779e9a5cd7024c7','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 15:38:37','2020-09-24 15:38:37','2021-09-24 15:38:37'),
	('6f903164432da60a3b18dd72dffc36d056c00814c88c069bdefc01275a29d734deba23ef137c60d9','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-26 12:28:20','2020-09-26 12:28:20','2021-09-26 12:28:20'),
	('79d0872e759026d8be98ffc33b881086e70c23b085650fa05b4d8cd0c0e00b28f1854362fb62adb1','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',1,'2020-09-24 11:53:35','2020-09-24 11:53:35','2021-09-24 11:53:35'),
	('81835e667a5dd2ae29551fed6fde86fd04eb94761bd5ba1b61567a3e49793ca627b16b35d92620b7','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-27 13:58:19','2020-09-27 13:58:19','2021-09-27 13:58:19'),
	('83b33195b92feff0147e38479b932ba49ff9929b991c356f9677f3129b29121923d3559daac96e31','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-26 16:18:18','2020-09-26 16:18:18','2021-09-26 16:18:18'),
	('8530d8eb37533c830a9d5e91fa0e2ea8d3fb33d72d64374d5b932908a0cdfbf301460611259b19ea','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 20:39:42','2020-09-24 20:39:42','2021-09-24 20:39:42'),
	('9e77c23b27f2325f3ec37810728e6632068117cb1b6ad99a7aa856633001a8454f0211854f3f6bf2','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-26 22:03:58','2020-09-26 22:03:58','2021-09-26 22:03:58'),
	('a10885c548f824ba24eef139fa09fb88f8f4bcd52406fd57ee2b40aa269c4d246e238fd90315a05d','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-27 00:08:44','2020-09-27 00:08:44','2021-09-27 00:08:44'),
	('a644eb621b000863b04d77ded0e503a78cea3548842852486d563e0f9211745e558051abc16c57c4','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-26 10:50:27','2020-09-26 10:50:27','2021-09-26 10:50:27'),
	('aab9c64c0000974914cc9c6c8cd147cd2eb547fc472ae07ef47de0da55c12b192f83d15d014f80fb','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 15:33:20','2020-09-24 15:33:20','2021-09-24 15:33:20'),
	('b42d423e5ef3f806e6a31720371fcbb1eb32e2143955158d7223eb53b206da7efdab028c395cd058','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 15:33:14','2020-09-24 15:33:14','2021-09-24 15:33:14'),
	('bf25bb04ff218d7936f565f7b901d73c7c40e7bbe096b022620d78de5fb18de0e88c4b294778ba3f','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-25 22:03:58','2020-09-25 22:03:58','2021-09-25 22:03:58'),
	('c1b3026e152df8d2abb3157e1090673b89ea40d257afd9a301709b4cc85b962b4ced688023695da3','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 21:06:44','2020-09-24 21:06:44','2021-09-24 21:06:44'),
	('e6593762f04dcd1e98a099c386dc74d4f0ad04f1068bbdbaf775afe124335f91c5edaf16d76bd02d','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-27 00:07:10','2020-09-27 00:07:10','2021-09-27 00:07:10'),
	('ff836f40c71f07619b95ba8d0f4bf89c9c052d37395344a9e2fa25fc5d9060a921d344523cf0f3d8','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-26 14:26:53','2020-09-26 14:26:53','2021-09-26 14:26:53'),
	('ffe5dd2d028b8d1e823669d33e21f6c82453f3c7937819eebe559fa216eb6931ac01afd61e7b6126','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,NULL,'[]',0,'2020-09-24 19:26:59','2020-09-24 19:26:59','2021-09-24 19:26:59');

/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla oauth_auth_codes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_auth_codes`;

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla oauth_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_clients`;

CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`)
VALUES
	(1,NULL,'Spirit7','HA9whWTof6rD2IipNKAiwzkrhEJ2gGB5SZLooflq','users','http://localhost',0,1,0,NULL,NULL);

/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla oauth_personal_access_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_personal_access_clients`;

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla oauth_refresh_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_refresh_tokens`;

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`)
VALUES
	('05a894d52be674e50a17051208d86e493cc54ba9dffc191c0eee1632194d97f6864cfc4d3e3d7812','a644eb621b000863b04d77ded0e503a78cea3548842852486d563e0f9211745e558051abc16c57c4',0,'2021-09-26 10:50:27'),
	('0bba5606ee1f71f16ee642f0badc6fa9645ece2200a6769b8ef94f6bf40a1968892e1b76a8df5b86','aab9c64c0000974914cc9c6c8cd147cd2eb547fc472ae07ef47de0da55c12b192f83d15d014f80fb',0,'2021-09-24 15:33:20'),
	('12bda961aaf4b05991fc34b449ed8bd1a13a0202c29fcbc01df557c01ed6acb10ac6751a717580fd','6f903164432da60a3b18dd72dffc36d056c00814c88c069bdefc01275a29d734deba23ef137c60d9',0,'2021-09-26 12:28:20'),
	('2aab353171c84c26e0df11e698a0807dc7e89a0c6edb771efe040b1f6d7f5e5ba24be27bf1165c2c','83b33195b92feff0147e38479b932ba49ff9929b991c356f9677f3129b29121923d3559daac96e31',0,'2021-09-26 16:18:18'),
	('2accf43c4baf742378b407c578161da247ad9fed7d0ba35c25fe6322430bc2b08819dd9013e60dec','552a1deb9be9ca83b6fd7ef17ccf5adf059d17cb8ff940d92eecddf801088580a32217559367b9ba',0,'2021-09-27 00:07:49'),
	('2c2773cabd263a28232b88624306d6112f8316bfe86925fffe56ec7beadd4f9369e373dc6668999b','8530d8eb37533c830a9d5e91fa0e2ea8d3fb33d72d64374d5b932908a0cdfbf301460611259b19ea',0,'2021-09-24 20:39:42'),
	('347b59f01951eacfdf1d052fc66fd437149a4f5b79555cef098d73f359c09febb64ea1ec55843aa1','3dd2fc8ad193a4fd2a5a3beb153a902f820121734a3ca6e989f128c03fd1079d9dcd2acb5afff598',0,'2021-09-24 20:43:18'),
	('37217d76c15317521af21517191de90e81b6ec1d75b237dc4b5d744137b68033fb66f21feb891eae','0b9df789c5afb074afd30e1ea770f9919bb90dce8af7c0ec3039e42231a8b853b4402a05acee2cfd',0,'2021-09-24 15:38:54'),
	('468ea5f42be0ced40bad11b57c73897b0b9e4cfea0a9a13277fff9e7b47977b9f3c5e91932901943','c1b3026e152df8d2abb3157e1090673b89ea40d257afd9a301709b4cc85b962b4ced688023695da3',0,'2021-09-24 21:06:44'),
	('6448bcdbc9f48762a89f6e13db870d576da2b01f5542a7ecaacacda5f26cbdf0957d9e5909fc2927','ffe5dd2d028b8d1e823669d33e21f6c82453f3c7937819eebe559fa216eb6931ac01afd61e7b6126',0,'2021-09-24 19:26:59'),
	('6d716706a8bbeef31c00289332ec8d66208c75f3a20ef22db85fb303cc9090b0ecd71350d0837ccd','79d0872e759026d8be98ffc33b881086e70c23b085650fa05b4d8cd0c0e00b28f1854362fb62adb1',0,'2021-09-24 11:53:35'),
	('6e02b2a180a1b92aeef86e011170904e4133838075b2fc458a877500425987bf75e18228e4e4263b','81835e667a5dd2ae29551fed6fde86fd04eb94761bd5ba1b61567a3e49793ca627b16b35d92620b7',0,'2021-09-27 13:58:19'),
	('70f3755c940559a3d197b55750e23bb644fd2ce72678d56a77b0d15cc7ec6c8243780e14ab6eecb3','248429687b90d47845e24bb0f59ca282933a8186ce4df4025ef0b61300adac0cdcbd8c456d360022',0,'2021-09-26 12:27:59'),
	('76fff668a7a641199e362ba024aaa131688ec76b443e6e5aea424430ad22dc759a6eeca111069000','44e8a8b256f74ac9f60b476f66fd8079f5abc820001d995a74e447c14264bfe0dc7cc1cc88222af6',0,'2021-09-26 19:56:47'),
	('7a35b855942e42f3501437bd5f1fa63014a72fb67b559e8764189f2ed5d6483466c466aa32625999','b42d423e5ef3f806e6a31720371fcbb1eb32e2143955158d7223eb53b206da7efdab028c395cd058',0,'2021-09-24 15:33:14'),
	('7a3e331de65dd1103f6f5fbff60d4615a115fa946292a9569e97fe6eb6bee4bddf421c54f15f3d67','9e77c23b27f2325f3ec37810728e6632068117cb1b6ad99a7aa856633001a8454f0211854f3f6bf2',0,'2021-09-26 22:03:58'),
	('8761aef31eb9d3eb1fd3390b986deeeeb92ae9bdb98eb6103e3efdaa1b689bd90c2cafdce7ab3c22','22948c78bbf6796d231f46f4d4f98eaa3ca9493c888115075264d5ab04e9e632ae414c4418771f61',0,'2021-09-24 20:47:34'),
	('90a492671ccd8e212bc266f9f451aacf9835fce8073ce05b5d4d3ec93450fdaef88a162a84620bf3','ff836f40c71f07619b95ba8d0f4bf89c9c052d37395344a9e2fa25fc5d9060a921d344523cf0f3d8',0,'2021-09-26 14:26:53'),
	('98f5162fbd75b4312cc29bb830def78c100be4bd30520c145dde0055bffe6d01fc7109bff7eddf99','a10885c548f824ba24eef139fa09fb88f8f4bcd52406fd57ee2b40aa269c4d246e238fd90315a05d',0,'2021-09-27 00:08:44'),
	('9d26bab8332c8040d5ae0fb0146f892a5c464767228445a61e8b20f72225fa00cbe3b8425bf2cb5c','1c6eec7a57414b344984f86f7d53bb7344131e0663489908b4e2dd33422a7adbe16c936bcb7fce2b',0,'2021-09-24 19:33:32'),
	('a64f2d9178df1914f31fa46ea71a49cd1f8f58cbd1d9ee40961507f344acef220093f8a511bdc32f','55291c1b7461b1a93dd2470c465f61ac6e8ca84b9e7e3536fd7ac1db1a927b2492d6ced4fdac349b',0,'2021-09-27 00:08:14'),
	('a671b4f1c8e5dc00a6ca309807f0eea0a9c230c48e927357bc31941dc9ed24e749b4592eac890820','00662e3c73d77b8b61346dbe05a1f49b368bf6467e5a77e13460bd44daa95e48841cd64175e11f1b',0,'2021-09-25 18:03:20'),
	('a726d919bcf0e09a174ee6729dd100b940649cb13a191978a43502eb56d76fefaff2e024664181eb','0b3d2015124ecaf98ad7d98b36a841d186ef594f0d14909ba558e3894a2c38fa4d0de1c4d5f974e3',0,'2021-09-24 15:32:53'),
	('b55ca46c28506f35cae24736c97ed3edf1084fdf329a0f02a279a6435555c21d85f02329ecf2c2bc','5f9bc993898fa491dffb77c5a7329a016a5c2a0583b15f5dbbd5b69bde0550367779e9a5cd7024c7',0,'2021-09-24 15:38:37'),
	('d7cc505ff5e3a748c91241423e9529330f423f13a5cc3f83a1af310c0be09b5dc03780d02b74f16d','e6593762f04dcd1e98a099c386dc74d4f0ad04f1068bbdbaf775afe124335f91c5edaf16d76bd02d',0,'2021-09-27 00:07:10'),
	('d8931ba87c9b87d2ffd5ab480bd9af9ac6036e57a2a3a673cb697e97f81eb7ad2cbe6db697b8ee88','bf25bb04ff218d7936f565f7b901d73c7c40e7bbe096b022620d78de5fb18de0e88c4b294778ba3f',0,'2021-09-25 22:03:58'),
	('d9e361ae84cf6bb05fffde016fc5c0ea8f5d6c0d2254088f22012dce7375cd0f1a314f829f41545d','5b179c68ff88b1791e78c47252491d4eb175bede61baea352b4b8ebc0d1a9e99d53a3633d1a3dd39',0,'2021-09-24 20:48:41'),
	('e8428b7eb25fef7c2efbb05b3a622b1af3982a4bda5d62ff02aa2acf82b3e00c1fa257c26f3c4a0b','3643fa1f68891386bd4b88bef39db5df06f247e4ff24af37b81da346b5155489ae009cb1d9b35e4f',0,'2021-09-26 13:12:34'),
	('f1883159a4ecf18c32e976f39ac3b0b4002ec7f762ce2a9b9d159d2b204250ca7bac6c7375ad4325','46c1305044ec220aa35c87bce9ed53db7869411ea86c6bf104067b34cf4e3c0a32e1d35aa979cbcc',0,'2021-09-28 09:53:42');

/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`id`, `name`, `display_name`, `guard_name`, `created_at`, `updated_at`)
VALUES
	(1,'View projects','Ver proyectos','api','2020-09-26 14:25:50','2020-09-26 14:25:50'),
	(2,'Create projects','Crear proyectos','api','2020-09-26 14:25:50','2020-09-26 14:25:50'),
	(3,'Update projects','Actualizar proyectos','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(4,'Delete projects','Eliminar proyectos','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(5,'View users','Ver usuarios','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(6,'Create users','Crear usuarios','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(7,'Update users','Actualizar usuarios','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(8,'Delete users','Eliminar usuarios','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(9,'View roles','Ver roles','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(10,'Create roles','Crear roles','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(11,'Update roles','Actualizar roles','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(12,'Delete roles','Eliminar roles','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(13,'View permissions','Ver permisos','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(14,'Update permissions','Actualizar permisos','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(15,'View project members','Ver miembros de proyecto','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(16,'Create project members','Crear miembros de proyecto','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(17,'Update project members','Actualizar miembros de proyecto','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(18,'Delete project members','Eliminar miembros de proyecto','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(19,'View groups','Ver grupos','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(20,'Create groups','Crear grupos','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(21,'Update groups','Actualizar grupos','api','2020-09-26 14:25:51','2020-09-26 14:25:51'),
	(22,'Delete groups','Eliminar grupos','api','2020-09-26 14:25:51','2020-09-26 14:25:51');

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `program`;

CREATE TABLE `program` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `program` WRITE;
/*!40000 ALTER TABLE `program` DISABLE KEYS */;

INSERT INTO `program` (`id`, `name`)
VALUES
	(1,'DISEÑO GRÁFICO'),
	(2,'DISEÑO, COMUNICACIÓN Y NUEVOS MEDIOS'),
	(3,'EFICIENCIA ENERGÉTICA'),
	(4,'ELECTRÓNICA Y AUTOMATIZACIÓN'),
	(5,'ENERGÍA Y AMBIENTE'),
	(6,'ENSEÑANZA DE LENGUAS NATIVAS Y EXTRANJERAS'),
	(7,'FÍSICA COMPUTACIONAL'),
	(8,'FÍSICA DE LAS RADIACIONES'),
	(9,'INGENIERÍA DE PROCESOS INDUSTRIALES'),
	(10,'INGENIERÍA DE SOFTWARE'),
	(11,'INGENIERÍA Y TECNOLOGÍA DE MATERIALES'),
	(12,'INNOVACIÓN EDUCATIVA'),
	(13,'INTELIGENCIA ARTIFICIAL'),
	(14,'MANEJO Y APROVECHAMIENTO DE RECURSOS RENOVABLES'),
	(15,'MODELADO Y SIMULACIÓN COMPUTACIONAL DE PROCESOS'),
	(16,'PROCESAMIENTO DIGITAL DE SEÑALES E IMÁGENES'),
	(17,'ROBÓTICA Y CONTROL'),
	(18,'SEGURIDAD DE SISTEMAS DE INFORMACIÓN'),
	(19,'SUSTENTABILIDAD Y NUEVAS TECNOLOGÍAS'),
	(20,'TECNOLOGÍA EDUCATIVA'),
	(21,'TECNOLOGÍA VEHICULAR'),
	(22,'TELECOMUNICACIONES Y REDES'),
	(23,'TELEFONÍA PARA EL DESARROLLO');

/*!40000 ALTER TABLE `program` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla program_has_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `program_has_groups`;

CREATE TABLE `program_has_groups` (
  `program_id` bigint(20) unsigned NOT NULL,
  `group_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`program_id`,`group_id`),
  KEY `program_has_groups_group_id_foreign` (`group_id`),
  CONSTRAINT `program_has_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  CONSTRAINT `program_has_groups_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `program_has_groups` WRITE;
/*!40000 ALTER TABLE `program_has_groups` DISABLE KEYS */;

INSERT INTO `program_has_groups` (`program_id`, `group_id`)
VALUES
	(2,'575af76e-fe98-11ea-ba23-9fc837c15a07'),
	(3,'575af76e-fe98-11ea-ba23-9fc837c15a07'),
	(2,'69cd355c-fe97-11ea-b9a7-6003088c543c'),
	(4,'69cd355c-fe97-11ea-b9a7-6003088c543c'),
	(7,'69cd355c-fe97-11ea-b9a7-6003088c543c'),
	(8,'69cd355c-fe97-11ea-b9a7-6003088c543c');

/*!40000 ALTER TABLE `program_has_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kind` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `endDateReal` date DEFAULT NULL,
  `location` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_type_id` bigint(20) unsigned DEFAULT NULL,
  `coverage_type_id` bigint(20) unsigned DEFAULT NULL,
  `research_type_id` bigint(20) unsigned DEFAULT NULL,
  `program_id` bigint(20) unsigned DEFAULT NULL,
  `year` year(4) NOT NULL,
  `father` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_group` varchar(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `background` text COLLATE utf8mb4_unicode_ci,
  `justification` text COLLATE utf8mb4_unicode_ci,
  `current_situation` text COLLATE utf8mb4_unicode_ci,
  `general_objective` text COLLATE utf8mb4_unicode_ci,
  `specific_objectives` text COLLATE utf8mb4_unicode_ci,
  `sustainability` text COLLATE utf8mb4_unicode_ci,
  `methodology` text COLLATE utf8mb4_unicode_ci,
  `expected_results` text COLLATE utf8mb4_unicode_ci,
  `transference_results` text COLLATE utf8mb4_unicode_ci,
  `beneficiaries` text COLLATE utf8mb4_unicode_ci,
  `impacts` text COLLATE utf8mb4_unicode_ci,
  `aspects` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `project_project_type_id_foreign` (`project_type_id`),
  KEY `project_coverage_type_id_foreign` (`coverage_type_id`),
  KEY `project_research_type_id_foreign` (`research_type_id`),
  KEY `project_program_id_foreign` (`program_id`),
  CONSTRAINT `project_coverage_type_id_foreign` FOREIGN KEY (`coverage_type_id`) REFERENCES `coverage_type` (`id`),
  CONSTRAINT `project_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`),
  CONSTRAINT `project_project_type_id_foreign` FOREIGN KEY (`project_type_id`) REFERENCES `project_type` (`id`),
  CONSTRAINT `project_research_type_id_foreign` FOREIGN KEY (`research_type_id`) REFERENCES `research_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`id`, `code`, `name`, `slug`, `kind`, `startDate`, `endDate`, `endDateReal`, `location`, `project_type_id`, `coverage_type_id`, `research_type_id`, `program_id`, `year`, `father`, `id_group`, `created_at`, `updated_at`, `deleted_at`, `summary`, `background`, `justification`, `current_situation`, `general_objective`, `specific_objectives`, `sustainability`, `methodology`, `expected_results`, `transference_results`, `beneficiaries`, `impacts`, `aspects`)
VALUES
	('171118f4-002e-11eb-83d4-6003088c543c',NULL,'Mi primer proyecto','mi-primer-proyecto','Institucional','2020-01-01','2020-12-30',NULL,'Quito',1,1,1,12,'2020',NULL,NULL,'2020-09-26 14:25:52','2020-09-26 14:38:27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('171cd734-002e-11eb-bb2a-6003088c543c',NULL,'Mi segundo proyecto','mi-segundo-proyecto','Institucional','2020-09-01','2020-11-30',NULL,'Riobamba4',1,3,1,7,'2020',NULL,NULL,'2020-09-26 14:25:52','2020-09-26 15:36:20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('171d7338-002e-11eb-a2e1-6003088c543c',NULL,'Mi tercer proyecto','mi-tercer-proyecto',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,'2020-09-26 14:25:52','2020-09-26 14:25:52',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('171e1e78-002e-11eb-b043-6003088c543c',NULL,'Mi Cuarto proyecto','mi-cuarto-proyecto',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020',NULL,NULL,'2020-09-26 14:25:52','2020-09-26 14:25:52',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla project_has_dependency
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_dependency`;

CREATE TABLE `project_has_dependency` (
  `project_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dependency_id` bigint(20) unsigned NOT NULL,
  `dependency_type` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `participation_type` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`project_id`,`dependency_id`),
  KEY `project_has_dependency_dependency_id_foreign` (`dependency_id`),
  CONSTRAINT `project_has_dependency_dependency_id_foreign` FOREIGN KEY (`dependency_id`) REFERENCES `dependency` (`id`),
  CONSTRAINT `project_has_dependency_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla project_has_researchline
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_researchline`;

CREATE TABLE `project_has_researchline` (
  `project_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `researchline_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`project_id`,`researchline_id`),
  KEY `project_has_researchline_researchline_id_foreign` (`researchline_id`),
  CONSTRAINT `project_has_researchline_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `project_has_researchline_researchline_id_foreign` FOREIGN KEY (`researchline_id`) REFERENCES `researchline` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla project_has_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_status`;

CREATE TABLE `project_has_status` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_has_status_user_id_foreign` (`user_id`),
  CONSTRAINT `project_has_status_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `project_has_status` WRITE;
/*!40000 ALTER TABLE `project_has_status` DISABLE KEYS */;

INSERT INTO `project_has_status` (`id`, `project_id`, `status_id`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,'1690aba0-fe86-11ea-8708-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-24 11:50:44','2020-09-24 11:50:44'),
	(2,'169cf8ba-fe86-11ea-871b-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-24 11:50:44','2020-09-24 11:50:44'),
	(3,'169d7524-fe86-11ea-8c10-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-24 11:50:44','2020-09-24 11:50:44'),
	(4,'169dee5a-fe86-11ea-ac47-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-24 11:50:44','2020-09-24 11:50:44'),
	(5,'5a3602b0-fe86-11ea-a742-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-24 11:52:38','2020-09-24 11:52:38'),
	(6,'5a3cf3b8-fe86-11ea-84c6-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-24 11:52:38','2020-09-24 11:52:38'),
	(7,'5a3d6a96-fe86-11ea-b216-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-24 11:52:38','2020-09-24 11:52:38'),
	(8,'5a3dd328-fe86-11ea-a7bb-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-24 11:52:38','2020-09-24 11:52:38'),
	(9,'52241b00-fe97-11ea-87e7-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-24 13:54:06','2020-09-24 13:54:06'),
	(10,'52314a00-fe97-11ea-bb35-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-24 13:54:06','2020-09-24 13:54:06'),
	(11,'5231fcf2-fe97-11ea-adca-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-24 13:54:06','2020-09-24 13:54:06'),
	(12,'523271b4-fe97-11ea-8e55-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-24 13:54:06','2020-09-24 13:54:06'),
	(13,'69c21064-fe97-11ea-9ac2-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-24 13:54:45','2020-09-24 13:54:45'),
	(14,'69c35834-fe97-11ea-bed9-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-24 13:54:45','2020-09-24 13:54:45'),
	(15,'69c3d110-fe97-11ea-8a21-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-24 13:54:45','2020-09-24 13:54:45'),
	(16,'69c44f96-fe97-11ea-a351-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-24 13:54:45','2020-09-24 13:54:45'),
	(38,'69c21064-fe97-11ea-9ac2-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-25 15:16:11','2020-09-25 15:16:11'),
	(39,'69c21064-fe97-11ea-9ac2-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-25 15:18:15','2020-09-25 15:18:15'),
	(40,'69c21064-fe97-11ea-9ac2-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-25 15:20:32','2020-09-25 15:20:32'),
	(41,'69c21064-fe97-11ea-9ac2-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-25 15:22:57','2020-09-25 15:22:57'),
	(42,'69c21064-fe97-11ea-9ac2-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-25 15:23:42','2020-09-25 15:23:42'),
	(43,'69c21064-fe97-11ea-9ac2-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-25 15:24:34','2020-09-25 15:24:34'),
	(44,'69c21064-fe97-11ea-9ac2-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-25 15:24:41','2020-09-25 15:24:41'),
	(45,'69c21064-fe97-11ea-9ac2-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-25 15:25:09','2020-09-25 15:25:09'),
	(46,'69c21064-fe97-11ea-9ac2-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-25 15:25:32','2020-09-25 15:25:32'),
	(51,'d52f6360-ffa5-11ea-942d-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-25 22:10:29','2020-09-25 22:10:29'),
	(52,'d53b3820-ffa5-11ea-b263-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-25 22:10:30','2020-09-25 22:10:30'),
	(53,'d53bafe4-ffa5-11ea-b9ec-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-25 22:10:30','2020-09-25 22:10:30'),
	(54,'d53d1942-ffa5-11ea-a545-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-25 22:10:30','2020-09-25 22:10:30'),
	(55,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 00:13:35','2020-09-26 00:13:35'),
	(56,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 00:22:09','2020-09-26 00:22:09'),
	(57,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 00:22:52','2020-09-26 00:22:52'),
	(58,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 01:06:58','2020-09-26 01:06:58'),
	(67,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 13:26:00','2020-09-26 13:26:00'),
	(68,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 13:27:28','2020-09-26 13:27:28'),
	(69,'352a33b4-0026-11eb-a70e-31737fc9d533',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 13:29:26','2020-09-26 13:29:26'),
	(70,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 13:40:11','2020-09-26 13:40:11'),
	(71,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 13:40:23','2020-09-26 13:40:23'),
	(72,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 13:59:41','2020-09-26 13:59:41'),
	(73,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 13:59:42','2020-09-26 13:59:42'),
	(74,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 13:59:45','2020-09-26 13:59:45'),
	(75,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 13:59:46','2020-09-26 13:59:46'),
	(76,'d668dfb3-f4a3-32b2-be98-21bc0ab5c2ea',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 13:59:47','2020-09-26 13:59:47'),
	(77,'171118f4-002e-11eb-83d4-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 14:25:52','2020-09-26 14:25:52'),
	(78,'171cd734-002e-11eb-bb2a-6003088c543c',1,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 14:25:52','2020-09-26 14:25:52'),
	(79,'171d7338-002e-11eb-a2e1-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-26 14:25:52','2020-09-26 14:25:52'),
	(80,'171e1e78-002e-11eb-b043-6003088c543c',1,'1d29c8e8-e2b6-11ea-abc9-6003088c543c','2020-09-26 14:25:52','2020-09-26 14:25:52'),
	(81,'171118f4-002e-11eb-83d4-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 14:38:27','2020-09-26 14:38:27'),
	(82,'171cd734-002e-11eb-bb2a-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 15:32:57','2020-09-26 15:32:57'),
	(83,'171cd734-002e-11eb-bb2a-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 15:33:38','2020-09-26 15:33:38'),
	(84,'171cd734-002e-11eb-bb2a-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 15:36:20','2020-09-26 15:36:20'),
	(85,'171cd734-002e-11eb-bb2a-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 15:38:51','2020-09-26 15:38:51'),
	(86,'171cd734-002e-11eb-bb2a-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 15:39:02','2020-09-26 15:39:02'),
	(89,'171118f4-002e-11eb-83d4-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 19:40:07','2020-09-26 19:40:07'),
	(90,'171118f4-002e-11eb-83d4-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 19:40:13','2020-09-26 19:40:13'),
	(91,'171118f4-002e-11eb-83d4-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 19:57:09','2020-09-26 19:57:09'),
	(92,'171118f4-002e-11eb-83d4-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-26 22:07:36','2020-09-26 22:07:36'),
	(101,'171118f4-002e-11eb-83d4-6003088c543c',2,'1d1d9a0a-e2b6-11ea-9775-6003088c543c','2020-09-27 13:37:18','2020-09-27 13:37:18');

/*!40000 ALTER TABLE `project_has_status` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla project_has_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_has_users`;

CREATE TABLE `project_has_users` (
  `project_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `staff_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `project_has_users_user_id_foreign` (`user_id`),
  KEY `project_has_users_staff_id_foreign` (`staff_id`),
  CONSTRAINT `project_has_users_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `project_has_users_staff_id_foreign` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`),
  CONSTRAINT `project_has_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `project_has_users` WRITE;
/*!40000 ALTER TABLE `project_has_users` DISABLE KEYS */;

INSERT INTO `project_has_users` (`project_id`, `user_id`, `staff_id`, `created_at`, `updated_at`)
VALUES
	('1690aba0-fe86-11ea-8708-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-09-24 11:50:44','2020-09-24 11:50:44'),
	('169cf8ba-fe86-11ea-871b-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-09-24 11:50:44','2020-09-24 11:50:44'),
	('169d7524-fe86-11ea-8c10-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-24 11:50:44','2020-09-24 11:50:44'),
	('169dee5a-fe86-11ea-ac47-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-24 11:50:44','2020-09-24 11:50:44'),
	('171118f4-002e-11eb-83d4-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-09-26 14:25:52','2020-09-26 14:25:52'),
	('171cd734-002e-11eb-bb2a-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-09-26 14:25:52','2020-09-26 14:25:52'),
	('171d7338-002e-11eb-a2e1-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-26 14:25:52','2020-09-26 14:25:52'),
	('171e1e78-002e-11eb-b043-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-26 14:25:52','2020-09-26 14:25:52'),
	('352a33b4-0026-11eb-a70e-31737fc9d533','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,'2020-09-26 13:29:26','2020-09-26 13:29:26'),
	('52241b00-fe97-11ea-87e7-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-09-24 13:54:06','2020-09-24 13:54:06'),
	('52314a00-fe97-11ea-bb35-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-09-24 13:54:06','2020-09-24 13:54:06'),
	('5231fcf2-fe97-11ea-adca-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-24 13:54:06','2020-09-24 13:54:06'),
	('523271b4-fe97-11ea-8e55-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-24 13:54:06','2020-09-24 13:54:06'),
	('5a3602b0-fe86-11ea-a742-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-09-24 11:52:38','2020-09-24 11:52:38'),
	('5a3cf3b8-fe86-11ea-84c6-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-09-24 11:52:38','2020-09-24 11:52:38'),
	('5a3d6a96-fe86-11ea-b216-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-24 11:52:38','2020-09-24 11:52:38'),
	('5a3dd328-fe86-11ea-a7bb-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-24 11:52:38','2020-09-24 11:52:38'),
	('69c21064-fe97-11ea-9ac2-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',1,'2020-09-24 13:54:45','2020-09-25 16:08:32'),
	('69c21064-fe97-11ea-9ac2-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-25 16:08:33','2020-09-25 16:08:33'),
	('69c35834-fe97-11ea-bed9-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-09-24 13:54:45','2020-09-24 13:54:45'),
	('69c3d110-fe97-11ea-8a21-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-24 13:54:45','2020-09-24 13:54:45'),
	('69c44f96-fe97-11ea-a351-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-24 13:54:45','2020-09-24 13:54:45'),
	('d52f6360-ffa5-11ea-942d-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-09-25 22:10:29','2020-09-25 22:10:29'),
	('d53b3820-ffa5-11ea-b263-6003088c543c','1d1d9a0a-e2b6-11ea-9775-6003088c543c',2,'2020-09-25 22:10:30','2020-09-25 22:10:30'),
	('d53bafe4-ffa5-11ea-b9ec-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-25 22:10:30','2020-09-25 22:10:30'),
	('d53d1942-ffa5-11ea-a545-6003088c543c','1d29c8e8-e2b6-11ea-abc9-6003088c543c',2,'2020-09-25 22:10:30','2020-09-25 22:10:30');

/*!40000 ALTER TABLE `project_has_users` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla project_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_type`;

CREATE TABLE `project_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `project_type` WRITE;
/*!40000 ALTER TABLE `project_type` DISABLE KEYS */;

INSERT INTO `project_type` (`id`, `name`)
VALUES
	(1,'INVESTIGACIÓN'),
	(2,'VINCULACIÓN');

/*!40000 ALTER TABLE `project_type` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla requirements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `requirements`;

CREATE TABLE `requirements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `activity_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `requirements_activity_id_foreign` (`activity_id`),
  CONSTRAINT `requirements_activity_id_foreign` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `requirements` WRITE;
/*!40000 ALTER TABLE `requirements` DISABLE KEYS */;

INSERT INTO `requirements` (`id`, `name`, `quantity`, `price`, `total_price`, `activity_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(441,'quo',8,6.81,54.48,288,'2020-09-27 12:56:37','2020-09-27 12:56:37',NULL),
	(442,'sequi',10,5.90,59.00,288,'2020-09-27 12:56:37','2020-09-27 12:56:37',NULL),
	(443,'voluptatem',2,4.46,8.92,289,'2020-09-27 12:56:52','2020-09-27 12:56:52',NULL),
	(444,'aut',4,8.37,33.48,289,'2020-09-27 12:56:52','2020-09-27 12:56:52',NULL),
	(445,'et',3,6.26,18.78,289,'2020-09-27 12:56:52','2020-09-27 12:56:52',NULL),
	(446,'eligendi',10,7.21,72.10,289,'2020-09-27 12:56:52','2020-09-27 12:56:52',NULL),
	(447,'repellendus',3,7.59,22.77,289,'2020-09-27 12:56:52','2020-09-27 12:56:52',NULL),
	(448,'autem',9,9.48,85.32,290,'2020-09-27 12:57:06','2020-09-27 12:57:06',NULL),
	(449,'consequatur',10,5.33,53.30,291,'2020-09-27 12:57:11','2020-09-27 12:57:11',NULL),
	(450,'excepturi',9,7.34,66.06,292,'2020-09-27 12:57:16','2020-09-27 12:57:16',NULL),
	(451,'harum',1,8.18,8.18,292,'2020-09-27 12:57:20','2020-09-27 12:57:20',NULL),
	(452,'quos',3,8.66,25.98,292,'2020-09-27 12:57:20','2020-09-27 12:57:20',NULL),
	(453,'reiciendis',5,4.70,23.50,294,'2020-09-27 12:57:43','2020-09-27 12:57:43',NULL),
	(454,'ut',6,8.38,50.28,294,'2020-09-27 12:57:43','2020-09-27 12:57:43',NULL),
	(455,'corrupti',9,6.05,54.45,297,'2020-09-27 12:57:59','2020-09-27 12:57:59',NULL),
	(456,'neque',2,7.96,15.92,297,'2020-09-27 12:57:59','2020-09-27 12:57:59',NULL),
	(457,'neque',9,5.62,50.58,297,'2020-09-27 12:57:59','2020-09-27 12:57:59',NULL);

/*!40000 ALTER TABLE `requirements` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla research_center
# ------------------------------------------------------------

DROP TABLE IF EXISTS `research_center`;

CREATE TABLE `research_center` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Volcado de tabla research_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `research_type`;

CREATE TABLE `research_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `research_type` WRITE;
/*!40000 ALTER TABLE `research_type` DISABLE KEYS */;

INSERT INTO `research_type` (`id`, `name`)
VALUES
	(1,'INVESTIGACIÓN CIENTÍFICA'),
	(2,'DESARROLLO TECNOLÓGICO'),
	(3,'INNOVACIÓN TECNOLÓGICA');

/*!40000 ALTER TABLE `research_type` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla researchline
# ------------------------------------------------------------

DROP TABLE IF EXISTS `researchline`;

CREATE TABLE `researchline` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `researchline` WRITE;
/*!40000 ALTER TABLE `researchline` DISABLE KEYS */;

INSERT INTO `researchline` (`id`, `name`)
VALUES
	(1,'ADMINISTRACIÓN Y ECONOMÍA POPULAR'),
	(2,'ARTE CULTURA Y PATRIMONIO'),
	(3,'CIENCIAS BÁSICAS Y APLICADAS'),
	(4,'ENERGÍAS RENOVABLES Y PROTECCIÓN AMBIENTAL'),
	(5,'GESTIÓN Y MANEJO SUSTENTABLE DE LOS RECURSOS NATURALES'),
	(6,'PROCESOS TECNOLÓGICOS ARTESANALES E INDUSTRIALES'),
	(7,'SALUD Y NUTRICIÓN'),
	(8,'TECNOLOGÍAS DE LA INFORMACIÓN, COMUNICACIÓN');

/*!40000 ALTER TABLE `researchline` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla researchline_has_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `researchline_has_groups`;

CREATE TABLE `researchline_has_groups` (
  `researchline_id` bigint(20) unsigned NOT NULL,
  `group_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`researchline_id`,`group_id`),
  KEY `researchline_has_groups_group_id_foreign` (`group_id`),
  CONSTRAINT `researchline_has_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  CONSTRAINT `researchline_has_groups_researchline_id_foreign` FOREIGN KEY (`researchline_id`) REFERENCES `researchline` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `researchline_has_groups` WRITE;
/*!40000 ALTER TABLE `researchline_has_groups` DISABLE KEYS */;

INSERT INTO `researchline_has_groups` (`researchline_id`, `group_id`)
VALUES
	(1,'69cd355c-fe97-11ea-b9a7-6003088c543c'),
	(2,'69cd355c-fe97-11ea-b9a7-6003088c543c'),
	(4,'69cd355c-fe97-11ea-b9a7-6003088c543c'),
	(5,'69cd355c-fe97-11ea-b9a7-6003088c543c');

/*!40000 ALTER TABLE `researchline_has_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla role_has_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role_has_permissions`;

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`)
VALUES
	(2,7),
	(5,7),
	(9,7),
	(13,7);

/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `display_name`, `guard_name`, `created_at`, `updated_at`)
VALUES
	(1,'Cimogsys','Cimogsys','api','2020-09-26 14:25:50','2020-09-26 14:25:50'),
	(2,'Rector','Rector','api','2020-09-26 14:25:50','2020-09-26 14:25:50'),
	(3,'Vicerrector','Vicerrector','api','2020-09-26 14:25:50','2020-09-26 14:25:50'),
	(4,'Director IDI','Director IDI','api','2020-09-26 14:25:50','2020-09-26 14:25:50'),
	(5,'Decano','Decano','api','2020-09-26 14:25:50','2020-09-26 14:25:50'),
	(6,'Subdecano','Subdecano','api','2020-09-26 14:25:50','2020-09-26 14:25:50'),
	(7,'Investigador','Investigador','api','2020-09-26 14:25:50','2020-09-26 14:25:50'),
	(8,'Admin groups','Admin Grupos','api','2020-09-26 14:25:50','2020-09-26 14:25:50');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla staff
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'NULL',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;

INSERT INTO `staff` (`id`, `name`, `display_name`)
VALUES
	(1,'Investigador responsable','Investigador responsable'),
	(2,'Investigador miembro','Investigador miembro'),
	(3,'Técnico','Técnico'),
	(4,'Administrativo','Administrativo'),
	(5,'Tesista','Tesista'),
	(6,'Practicante','Practicante');

/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;

INSERT INTO `status` (`id`, `name`)
VALUES
	(1,'Proyecto creado'),
	(2,'Proyecto actualizado'),
	(3,'Enviado a decanato'),
	(4,'Aprobado por decanato'),
	(5,'Rechazado por decanato'),
	(6,'Enviado a director IDI'),
	(7,'Aprobado por director IDI'),
	(8,'Rechazado por director IDI');

/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identification_card` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `identification_card`, `name`, `lastname`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	('1d1d9a0a-e2b6-11ea-9775-6003088c543c','0603997305','Cristian','Guamán','cristian.guaman@espoch.edu.ec',NULL,'$2y$10$oz56qU28y9IqW5t/qV6VYO9E6iX7B5aFS6yx2vkrQUQYOcrQ9WKX.',NULL,'2020-09-26 14:25:51','2020-09-26 14:25:51',NULL),
	('1d29c8e8-e2b6-11ea-abc9-6003088c543c','1701098212','Naizly','Silva','naizly.silva@espoch.edu.ec',NULL,'$2y$10$4am3ZfJve0pjV0enwqzV..piYk2sw2I2Daamx79NS3NKuxr9N3TyC',NULL,'2020-09-26 14:25:51','2020-09-26 14:25:51',NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
