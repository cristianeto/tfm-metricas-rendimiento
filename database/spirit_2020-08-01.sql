# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.31)
# Base de datos: spirit
# Tiempo de Generación: 2020-08-02 01:31:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id_project` varchar(36) NOT NULL,
  `code_project` varchar(50) DEFAULT NULL,
  `name_project` varchar(500) NOT NULL,
  `startDate_project` date DEFAULT NULL,
  `endDate_project` date DEFAULT NULL,
  `endDateReal_project` date DEFAULT NULL COMMENT 'Sera la fecha final en la que realmente finalizó',
  `location_project` varchar(300) DEFAULT NULL,
  `id_coverageType` varchar(36) DEFAULT NULL,
  `id_projectType` varchar(36) DEFAULT NULL,
  `id_researchType` varchar(36) DEFAULT NULL,
  `id_program` varchar(36) DEFAULT NULL,
  `year_project` year(4) NOT NULL,
  `father_project` varchar(36) DEFAULT NULL,
  `id_group` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_project`),
  KEY `fk_project_projectType1_idx` (`id_projectType`),
  KEY `fk_project_researchType1_idx` (`id_researchType`),
  KEY `fk_project_project1_idx` (`father_project`),
  KEY `fk_project_cverageType_idx` (`id_coverageType`),
  KEY `fk_project_group1_idx` (`id_group`),
  KEY `project_ibfk_1_idx` (`id_program`),
  CONSTRAINT `fk_project_cverageType` FOREIGN KEY (`id_coverageType`) REFERENCES `coveragetype` (`id_coverageType`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_project1` FOREIGN KEY (`father_project`) REFERENCES `project` (`id_project`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_projectType1` FOREIGN KEY (`id_projectType`) REFERENCES `projecttype` (`id_projectType`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_researchType1` FOREIGN KEY (`id_researchType`) REFERENCES `researchtype` (`id_researchType`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`id_program`) REFERENCES `program` (`id_program`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`id_project`, `code_project`, `name_project`, `startDate_project`, `endDate_project`, `endDateReal_project`, `location_project`, `id_coverageType`, `id_projectType`, `id_researchType`, `id_program`, `year_project`, `father_project`, `id_group`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	('157b2eb8-c7b6-11ea-ba20-6003088c543c',NULL,'mi proyecto','2020-01-01','2020-12-30',NULL,'Riobamba','79a95878-9882-11ea-bb37-0242ac130002','f9ae248c-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','9f997d28-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-07-16 17:45:44','2020-07-19 18:35:15',NULL),
	('157c4ccc-9880-11ea-bb37-0242ac130002','pro-alpa','Proyecto para la elaboración y evaluación del Modelo ALPA para la administración Central de la ESPOCH','2020-05-30','2020-12-30','2020-08-28','Riobamba','79a95e54-9882-11ea-bb37-0242ac130002','f9ae248c-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','9f996dec-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-04-23 00:04:36','2020-07-15 17:28:36',NULL),
	('157c4fba-9880-11ea-bb37-0242ac130002','pro-018','Software Iterativo','2020-02-05','2020-12-30',NULL,'Riobamba','79a95e54-9882-11ea-bb37-0242ac130002','f9ae248c-9886-11ea-bb37-0242ac130002','39cfe0ee-986b-11ea-8571-820f047d9840','9f9986d8-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-04-23 00:04:36','2020-07-15 17:28:33',NULL),
	('157c5122-9880-11ea-bb37-0242ac130002',NULL,'Proyecto Galápagos','2020-04-01','2020-04-30',NULL,'Riobamba','79a95a9e-9882-11ea-bb37-0242ac130002','f9ae248c-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','9f997684-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-04-26 23:33:07','2020-07-15 17:28:34',NULL),
	('157c5230-9880-11ea-bb37-0242ac130002',NULL,'Crecimiento de las plantaciones agrícolas en Lican Riobamba','2020-04-28','2020-12-30',NULL,'Riobamba','79a95878-9882-11ea-bb37-0242ac130002','f9ae248c-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','9f9974ea-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-04-27 23:32:22','2020-07-19 03:01:25',NULL),
	('157c5370-9880-11ea-bb37-0242ac130002',NULL,'Evaluación del impacto ambiental','2020-04-01','2020-04-30',NULL,'Cotopaxi','79a95d14-9882-11ea-bb37-0242ac130002','f9ae248c-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','9f997684-988d-11ea-bb37-0242ac130002','2021',NULL,NULL,'2020-04-28 09:09:23','2020-05-17 15:51:53',NULL),
	('157c5456-9880-11ea-bb37-0242ac130002',NULL,'Proyecto covid19','2020-01-01','2020-12-30','2020-05-13','Cc','79a95a9e-9882-11ea-bb37-0242ac130002','f9ae248c-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','9f9974ea-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-05-13 13:55:33','2020-05-17 15:52:04',NULL),
	('4844a62e-bbbf-11ea-a25c-6003088c543c',NULL,'Proyecto de aves','2020-07-01','2020-12-30',NULL,'Riobamba','79a95c42-9882-11ea-bb37-0242ac130002','f9ae248c-9886-11ea-bb37-0242ac130002','39cfe0ee-986b-11ea-8571-820f047d9840','9f998106-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-07-01 12:21:21','2020-07-01 12:21:21',NULL),
	('c71b11ec-c5ea-11ea-a211-6003088c543c',NULL,'Proyecto de REact','2020-07-14','2020-12-30',NULL,'Riobamba','79a95a9e-9882-11ea-bb37-0242ac130002','f9ae248c-9886-11ea-bb37-0242ac130002','39cfe0ee-986b-11ea-8571-820f047d9840','9f997ed6-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-07-14 10:57:54','2020-07-14 10:57:54',NULL),
	('d1ace12a-c5eb-11ea-87e2-6003088c543c',NULL,'Bendita tu luz','2020-07-14','2020-09-24',NULL,'Riobamba','79a95a9e-9882-11ea-bb37-0242ac130002','f9ae248c-9886-11ea-bb37-0242ac130002','39cfe0ee-986b-11ea-8571-820f047d9840','9f997684-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-07-14 11:05:21','2020-07-14 11:05:21',NULL),
	('d46ea82e-987f-11ea-bb37-0242ac130002','pro-02','Eccomerce de proyectos del COCA-CODO- SINCLAIR','2020-01-15','2020-12-17',NULL,'Riobamba31','79a95a9e-9882-11ea-bb37-0242ac130002','f9ae26da-9886-11ea-bb37-0242ac130002','16429ac6-9867-11ea-afcb-820f047d9840','9f99861a-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-04-23 00:04:36','2020-05-17 15:49:40',NULL),
	('f7c5375a-c7fc-11ea-8d20-6003088c543c',NULL,'kkkkkkk','2020-07-14','2020-11-24',NULL,'Riobamba','79a95c42-9882-11ea-bb37-0242ac130002','f9ae248c-9886-11ea-bb37-0242ac130002','39cfe0ee-986b-11ea-8571-820f047d9840','9f9986d8-988d-11ea-bb37-0242ac130002','2020',NULL,NULL,'2020-07-17 02:13:09','2020-07-17 02:13:09',NULL);

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
