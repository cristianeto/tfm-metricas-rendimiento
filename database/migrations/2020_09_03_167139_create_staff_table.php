<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffTable extends Migration
{
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {

		$table->increments('id');
		$table->string('name',100)->default('');
		$table->string('display_name',100)->nullable()->default('NULL');
        });
    }

    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
