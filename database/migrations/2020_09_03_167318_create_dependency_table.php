<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDependencyTable extends Migration
{
	public function up()
	{
		Schema::create('dependency', function (Blueprint $table) {

			$table->id('id');
			$table->string('name', 500);
			$table->string('acronym', 10)->nullable();
			$table->string('contact', 150)->nullable();
			$table->string('city', 100)->nullable();
			$table->string('email', 100)->nullable();
			$table->string('web', 100)->nullable();
			$table->string('phone', 100)->nullable();
			$table->foreignId('dependency_type_id');
			$table->foreignId('father')->nullable();

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('father')->references('id')->on('dependency');
			$table->foreign('dependency_type_id')->references('id')->on('dependency_type');
		});
	}

	public function down()
	{
		Schema::dropIfExists('dependency', function (Blueprint $table) {
			$table->dropSoftDeletes();
		});
	}
}
