<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
	public function up()
	{
		Schema::create('groups', function (Blueprint $table) {

			$table->uuid('id');
			$table->string('code', 10);
			$table->string('name');
			$table->string('acronym', 11);
			$table->string('mission', 500)->default('Sin misión');
			$table->string('vision', 500)->default('Sin visión');
			$table->foreignId('dependency_id');
			$table->foreignId('group_type_id');
			$table->foreignId('research_center_id')->nullable();
			$table->tinyInteger('active')->default(1);
			$table->timestamps();
			$table->softDeletes();

			$table->primary('id');
			$table->foreign('dependency_id')->references('id')->on('dependency');
			$table->foreign('group_type_id')->references('id')->on('group_type');
			$table->foreign('research_center_id')->references('id')->on('research_center');
		});
	}

	public function down()
	{
		Schema::dropIfExists('groups', function (Blueprint $table) {
			$table->dropSoftDeletes();
		});
	}
}
