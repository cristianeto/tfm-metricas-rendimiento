<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramHasGroupsTable extends Migration
{
    public function up()
    {
        Schema::create('program_has_groups', function (Blueprint $table) {

            $table->foreignId('program_id');
            $table->foreignUuid('group_id');
            $table->primary(['program_id','group_id']);
            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('program_id')->references('id')->on('program');

        });
    }

    public function down()
    {
        Schema::dropIfExists('program_has_groups');
    }
}
