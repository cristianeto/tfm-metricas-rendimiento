<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsHasUsersTable extends Migration
{
    public function up()
    {
        Schema::create('groups_has_users', function (Blueprint $table) {

            $table->foreignUuid('group_id');
            $table->foreignUuid('user_id');
            $table->timestamps();

            $table->primary(['group_id','user_id']);
            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    public function down()
    {
        Schema::dropIfExists('groups_has_users');
    }
}
