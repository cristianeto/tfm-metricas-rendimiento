<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchTypeTable extends Migration
{
    public function up()
    {
        Schema::create('research_type', function (Blueprint $table) {

		$table->id('id');
		$table->string('name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('research_type');
    }
}
