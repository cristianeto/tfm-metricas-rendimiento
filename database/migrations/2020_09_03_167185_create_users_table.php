<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->string('id', 36);
            $table->string('identification_card', 10)->nullable();
            $table->string('name', 100);
            $table->string('lastname', 100);
            $table->string('email');
            $table->string('sex');
            $table->tinyInteger('belongs_espoch')->default(1);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('remember_token', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->primary('id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
