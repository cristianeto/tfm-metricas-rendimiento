<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectHasResearchlineTable extends Migration
{
    public function up()
    {
        Schema::create('project_has_researchline', function (Blueprint $table) {

            $table->foreignUuid('project_id');
            $table->foreignId('researchline_id');

            $table->primary(['project_id','researchline_id']);
            $table->foreign('project_id')->references('id')->on('project');
            $table->foreign('researchline_id')->references('id')->on('researchline');
        });
    }

    public function down()
    {
        Schema::dropIfExists('project_has_researchline');
    }
}
