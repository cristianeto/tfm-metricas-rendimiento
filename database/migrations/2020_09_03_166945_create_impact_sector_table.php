<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImpactSectorTable extends Migration
{
    public function up()
    {
        Schema::create('impact_sector', function (Blueprint $table) {

		$table->string('id',36)->default('');
		$table->string('name')->default('');
		$table->primary('id');

        });
    }

    public function down()
    {
        Schema::dropIfExists('impact_sector');
    }
}