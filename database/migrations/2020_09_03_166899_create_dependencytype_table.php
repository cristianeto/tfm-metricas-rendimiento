<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDependencytypeTable extends Migration
{
    public function up()
    {
        Schema::create('dependency_type', function (Blueprint $table) {

		$table->id('id');
		$table->string('name',50);

        });
    }

    public function down()
    {
        Schema::dropIfExists('dependency_type');
    }

}
