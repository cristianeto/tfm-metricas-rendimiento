<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectTable extends Migration
{
	public function up()
	{
		Schema::create('project', function (Blueprint $table) {

			$table->string('id', 36);
			$table->string('code', 50)->nullable();
			$table->string('name', 500);
			$table->string('slug', 60);
			$table->string('kind', 15)->nullable();
			$table->string('resolution', 60)->nullable();
			$table->date('startDate')->nullable();
			$table->date('endDate')->nullable();
			$table->date('endDateReal')->nullable();
			$table->string('location', 300)->nullable();
			$table->tinyInteger('multi_disciplinary')->nullable();
			$table->foreignId('project_type_id')->nullable();
			$table->foreignId('coverage_type_id')->nullable();
			$table->foreignId('research_type_id')->nullable();
			$table->foreignId('program_id')->nullable();
			$table->year('year');
			$table->string('father', 36)->nullable();
			$table->string('id_group', 36)->nullable();

			$table->timestamps();
			$table->softDeletes();

			$table->text('summary')->nullable();
			$table->text('background')->nullable();
			$table->text('justification')->nullable();
			$table->text('current_situation')->nullable();
			$table->text('general_objective')->nullable();
			$table->text('specific_objectives')->nullable();
			$table->text('sustainability')->nullable();
			$table->text('methodology')->nullable();
			$table->text('expected_results')->nullable();
			$table->text('transference_results')->nullable();
			$table->text('beneficiaries')->nullable();
			$table->text('impacts')->nullable();
			$table->text('aspects')->nullable();
			$table->primary('id');

			$table->foreign('project_type_id')->references('id')->on('project_type');
			$table->foreign('coverage_type_id')->references('id')->on('coverage_type');
			$table->foreign('research_type_id')->references('id')->on('research_type');
			$table->foreign('program_id')->references('id')->on('program');
		});
	}

	public function down()
	{
		Schema::dropIfExists('project', function (Blueprint $table) {
			$table->dropSoftDeletes();
		});
	}
}
