<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchlineTable extends Migration
{
    public function up()
    {
        Schema::create('researchline', function (Blueprint $table) {

            $table->id('id');
            $table->string('name');

        });
    }

    public function down()
    {
        Schema::dropIfExists('researchline');
    }
}
