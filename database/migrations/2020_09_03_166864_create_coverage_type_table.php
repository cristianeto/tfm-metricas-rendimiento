<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoverageTypeTable extends Migration
{
    public function up()
    {
        Schema::create('coverage_type', function (Blueprint $table) {

		$table->id('id');
		$table->string('name',100);
        });
    }

    public function down()
    {
        Schema::dropIfExists('coverage_type');
    }
}
