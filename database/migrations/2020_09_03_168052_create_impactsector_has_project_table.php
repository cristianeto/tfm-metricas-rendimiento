<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImpactsectorHasProjectTable extends Migration
{
    public function up()
    {
        Schema::create('impactsector_has_project', function (Blueprint $table) {

		$table->string('impact_sector_id',36)->default('');
		$table->string('project_id',36)->default('');
		$table->primary(['impact_sector_id','project_id']);
		$table->foreign('impact_sector_id')->references('id')->on('impact_sector');		$table->foreign('project_id')->references('id')->on('project');
        });
    }

    public function down()
    {
        Schema::dropIfExists('impactsector_has_project');
    }
}