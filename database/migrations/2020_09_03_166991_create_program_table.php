<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramTable extends Migration
{
    public function up()
    {
        Schema::create('program', function (Blueprint $table) {

		$table->id('id',36);
		$table->string('name');

        });
    }

    public function down()
    {
        Schema::dropIfExists('program');
    }
}
