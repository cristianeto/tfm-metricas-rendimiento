<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectHasDependencyTable extends Migration
{
    public function up()
    {
        Schema::create('project_has_dependency', function (Blueprint $table) {

		$table->uuid('project_id');
		$table->foreignId('dependency_id');
		$table->string('dependency_type',45)->nullable();
		$table->text('participation_type')->nullable();
		$table->primary(['project_id','dependency_id']);
		$table->timestamps();

		$table->foreign('dependency_id')->references('id')->on('dependency');
		$table->foreign('project_id')->references('id')->on('project');
        });
    }

    public function down()
    {
        Schema::dropIfExists('project_has_dependency');
    }
}
