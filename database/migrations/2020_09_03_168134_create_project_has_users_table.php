<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectHasUsersTable extends Migration
{
    public function up()
    {
        Schema::create('project_has_users', function (Blueprint $table) {

		$table->string('project_id',36);
		$table->string('user_id',36);
		$table->integer('staff_id')->unsigned()->nullable();
		$table->timestamps();
		$table->primary(['project_id','user_id']);
		$table->foreign('project_id')->references('id')->on('project');
		$table->foreign('user_id')->references('id')->on('users');
		$table->foreign('staff_id')->references('id')->on('staff');
        });
    }

    public function down()
    {
        Schema::dropIfExists('project_has_users');
    }
}
