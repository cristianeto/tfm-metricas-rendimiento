<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrouptypeTable extends Migration
{
    public function up()
    {
        Schema::create('group_type', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('group_type');
    }
}
