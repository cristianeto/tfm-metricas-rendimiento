<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchcenterTable extends Migration
{
    public function up()
    {
        Schema::create('research_center', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('research_center');
    }
}
