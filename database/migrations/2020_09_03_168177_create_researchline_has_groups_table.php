<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchlineHasGroupsTable extends Migration
{
    public function up()
    {
        Schema::create('researchline_has_groups', function (Blueprint $table) {

            $table->foreignId('researchline_id');
            $table->foreignUuid('group_id');

            $table->primary(['researchline_id','group_id']);
            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('researchline_id')->references('id')->on('researchline');

        });
    }

    public function down()
    {
        Schema::dropIfExists('researchline_has_groups');
    }
}
