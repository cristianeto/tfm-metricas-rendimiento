<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDependencyHasUsersTable extends Migration
{
    public function up()
    {
        Schema::create('dependency_has_users', function (Blueprint $table) {

		$table->foreignId('dependency_id');
		$table->uuid('user_id');
		$table->primary(['dependency_id','user_id']);

		$table->foreign('dependency_id')->references('id')->on('dependency');
		$table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::dropIfExists('dependency_has_users');
    }
}
