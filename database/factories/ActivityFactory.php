<?php

namespace Database\Factories;

use App\Models\Activity;
use App\Models\Component;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActivityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Activity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => $name = $this->faker->sentence(12, true),
            "responsable" => $this->faker->title.' '.$this->faker->firstName.' '.$this->faker->lastName,
            "start_date" => Carbon::now()->format('Y/m/d'),
            "end_date" => Carbon::now()->format('Y/m/d'),
            "component_id" => Component::factory(),
        ];
    }
}
