<?php

namespace Database\Factories;

use App\Models\Activity;
use App\Models\Requirement;
use Illuminate\Database\Eloquent\Factories\Factory;

class RequirementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Requirement::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => $this->faker->word(),
            "quantity" => $quantity = $this->faker->numberBetween(1, 20),
            "price" => $price = $this->faker->randomFloat($nbMaxDecimals = 2, $min = 500, $max = 1000), // 48.8932(1, 50),
            "total_price" => $quantity * $price,
            "activity_id" => Activity::factory()
        ];
    }
}
