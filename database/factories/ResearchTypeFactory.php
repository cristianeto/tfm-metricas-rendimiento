<?php

namespace Database\Factories;

use App\Models\ResearchType;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResearchTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ResearchType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" =>$this->faker->word,
        ];
    }
}
