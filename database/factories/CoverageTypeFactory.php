<?php

namespace Database\Factories;

use App\Models\CoverageType;
use Illuminate\Database\Eloquent\Factories\Factory;

class CoverageTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CoverageType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => $this->faker->word,
        ];
    }
}
