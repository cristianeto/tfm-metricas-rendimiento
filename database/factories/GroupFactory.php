<?php

namespace Database\Factories;

use App\Models\Dependency;
use App\Models\Group;
use App\Models\GroupType;
use App\Models\ResearchCenter;
use Illuminate\Database\Eloquent\Factories\Factory;

class GroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Group::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->uuid,
            'code' => $this->faker->text('5'),
            'name' => $this->faker->sentence,
            'acronym' => $this->faker->text('5'),
            'mission' => $this->faker->paragraph,
            'vision' => $this->faker->paragraph,
            'dependency_id' => Dependency::factory(),
            'group_type_id' => GroupType::factory(),
            'research_center_id'=> ResearchCenter::factory(),
        ];
    }
}
