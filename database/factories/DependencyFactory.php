<?php

namespace Database\Factories;

use App\Models\Dependency;
use App\Models\DependencyType;
use Illuminate\Database\Eloquent\Factories\Factory;

class DependencyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Dependency::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'acronym' =>$this->faker->unique()->text('5'),
            'contact' => $this->faker->name,
            'city' => $this->faker->city,
            'email' => $this->faker->unique()->safeEmail,
            'web' => $this->faker->domainName,
            'phone' => $this->faker->phoneNumber,
            //'participation_type' => $faker->text,
            'dependency_type_id' => DependencyType::factory(),
        ];
    }
}
