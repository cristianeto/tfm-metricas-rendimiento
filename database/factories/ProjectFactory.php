<?php

namespace Database\Factories;

use App\Models\CoverageType;
use App\Models\Program;
use App\Models\ProjectType;
use App\Models\ResearchType;
use Carbon\Carbon;
use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "id" => $this->faker->uuid,
            "name" => $name = $this->faker->sentence(12, true),
            "slug" => Str::slug(Str::limit($name, 60, '')),
            "kind" => $this->faker->word(),
            "startDate" => Carbon::now()->format('Y-m-d'),
            "endDate" => Carbon::now()->format('Y-m-d'),
            "location" => $this->faker->city,
            "multi_disciplinary" => false,
            "coverage_type_id" => CoverageType::factory(),
            "project_type_id" => ProjectType::factory(),
            "research_type_id" => ResearchType::factory(),
            "program_id" => Program::factory(),
            "year" => Carbon::now()->year,
        ];
    }
}
