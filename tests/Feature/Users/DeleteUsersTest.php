<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class DeleteUsersTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function auth_user_can_down_his_profile()
    {
        $user = User::factory()->create();

        $user->assignRole('Cimogsys');
        Passport::actingAs($user);

        $this->deleteJson(route('users.destroy', $user))
            ->assertStatus(204);

    }
}
