<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class UpdateUsersTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function auth_user_can_update_his_profile()
    {
        //$this->withoutExceptionHandling();
        $user = User::factory()->create();
        Passport::actingAs($user);

        $this->putJson(route('users.update', $user), [
            'identification_card'=>'0123456789',
            'name'=>'Name has been changed',
            'lastname'=>'Lastname has been changed',
            'email'=>'new@email.com',
            'sex'=>'Hombre',
            'belongs_espoch'=> true,
        ])->assertStatus(200);

        $this->assertDatabaseHas('users', [
            'identification_card'=>'0123456789',
            'name'=>'Name has been changed',
            'lastname'=>'Lastname has been changed',
            'email'=>'new@email.com',
            'sex'=>'hombre',
            'belongs_espoch'=> '1',
        ]);
    }
}
