<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ListUsersTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function auth_users_can_fetch_users()
    {
        $authUser = User::factory()->create();
        $users = User::factory()->times(3)->create();
        Passport::actingAs($authUser);
        $response = $this->getJson(route('users.index'));

        $response->assertStatus(200)
                ->assertSee($users[0]->name);
    }

    /** @test */
    public function auth_users_can_fetch_his_profile()
    {
        $authUser = User::factory()->create();
        Passport::actingAs($authUser);
        $response = $this->getJson(route('users.show', $authUser));

        $response->assertStatus(200)
                ->assertSee($authUser->name);
    }
}
