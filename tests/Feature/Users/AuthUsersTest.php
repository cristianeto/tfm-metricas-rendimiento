<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AuthUsersTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_can_login()
    {
        $user = User::FirstOrFail();
        $userLogin = User::factory()->raw(['email' => $user->email]);

        $response = $this->postJson(route('auth.login', $userLogin));

        $response->assertStatus(200)
                ->assertJsonFragment(['token_type'=>'Bearer']);
    }

    /** @test */
    public function auth_users_can_logout()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $response = $this->getJson(route('auth.logout'));

        $response->assertStatus(200)
                ->assertSee('Has sido desconectado!');
    }

    /** @test */
    public function email_is_required()
    {
        $email = '';

        $response = $this->postJson(route('auth.login', $email));

        $response->assertStatus(422)
                ->assertSee('El campo correo electr\u00f3nico es obligatorio.');
    }

    /** @test */
    public function guests_can_sing_up()
    {

        $user = User::factory()->raw();
        $response = $this->postJson(route('users.store', $user));

        $response->assertStatus(201)
            ->assertJsonFragment(['email'=>$user['email']]);
    }
}
