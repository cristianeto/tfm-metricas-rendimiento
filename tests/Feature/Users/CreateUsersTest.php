<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CreateUsersTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_can_register()
    {
        $user = User::factory()->raw();
        $this->assertDatabaseMissing('users', $user);

        $this->postJson(route('users.store', $user))->assertCreated();
    }

    /** @test */
    public function cimogsys_role_can_create_a_espoch_user()
    {
        $user = User::factory()->raw(['belongs_espoch'=> true]);
        $this->assertDatabaseMissing('users', $user);

        $response = $this->postJson(route('users.store', $user))->dump();

        $response->assertCreated()
                ->assertJsonFragment(['belongs_espoch'=>'1']);
    }

    /** @test */
    public function cimogsys_role_can_create_a_external_espoch_user()
    {
        $user = User::factory()->raw(['belongs_espoch'=> false]);
        $this->assertDatabaseMissing('users', $user);

        $response = $this->postJson(route('users.store', $user))->dump();

        $response->assertCreated()
            ->assertJsonFragment(['belongs_espoch'=>'0']);
    }

    /** @test */
    public function identification_card_is_required()
    {
        $user = User::factory()->raw(['identification_card'=>'']);
        $this->assertDatabaseMissing('users', $user);

        $response = $this->postJson(route('users.store', $user));

        $response->assertStatus(422)
            ->assertJsonFragment(['El campo cédula es obligatorio.']);

        $this->assertDatabaseMissing('users', $user);
    }

    /** @test */
    public function name_is_required()
    {
        $user = User::factory()->raw(['name'=>'']);
        $this->assertDatabaseMissing('users', $user);

        $response = $this->postJson(route('users.store', $user));

        $response->assertStatus(422)
            ->assertJsonFragment(['El campo nombre es obligatorio.']);

        $this->assertDatabaseMissing('users', $user);
    }

    /** @test */
    public function lastname_is_required()
    {
        $user = User::factory()->raw(['lastname'=>'']);
        $this->assertDatabaseMissing('users', $user);

        $response = $this->postJson(route('users.store', $user));

        $response->assertStatus(422)
            ->assertJsonFragment(['El campo apellido es obligatorio.']);

        $this->assertDatabaseMissing('users', $user);
    }

    /** @test */
    public function email_is_required()
    {
        $user = User::factory()->raw(['email'=>'']);
        $this->assertDatabaseMissing('users', $user);

        $response = $this->postJson(route('users.store', $user));

        $response->assertStatus(422)
            ->assertJsonFragment(['El campo correo electrónico es obligatorio.']);

        $this->assertDatabaseMissing('users', $user);
    }

    /** @test */
    public function sex_is_required()
    {
        $user = User::factory()->raw(['sex'=>'']);
        $this->assertDatabaseMissing('users', $user);

        $response = $this->postJson(route('users.store', $user));

        $response->assertStatus(422)
            ->assertJsonFragment(['El campo sexo es obligatorio.']);

        $this->assertDatabaseMissing('users', $user);
    }

    /** @test */
    public function belongs_espoch_attribute_is_required()
    {
        $user = User::factory()->raw(['belongs_espoch'=>'']);
        $this->assertDatabaseMissing('users', $user);

        $response = $this->postJson(route('users.store', $user));

        $response->assertStatus(422)
            ->assertJsonFragment(['El campo pertenece espoch es obligatorio.']);

        $this->assertDatabaseMissing('users', $user);
    }
}
