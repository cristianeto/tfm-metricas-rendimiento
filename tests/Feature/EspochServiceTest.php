<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class EspochServiceTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function can_fetch_a_single_espoch_user_by_document()
    {
        $identification_card= '0603997305';

        $response = $this->getJson(route('espochService.userByDocument',$identification_card));

        $response->assertStatus(200)
            ->assertSee('per_email')
            ->assertSee('per_nombres')
            ->assertSee('per_primerApellido')
            ->assertSee('per_segundoApellido')
            ->assertSee('sex_id');
    }
}
