<?php

namespace Tests\Feature\Groups;

use App\Models\Group;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ListMembersTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function can_fetch_members_of_group()
    {
        $group = Group::factory()->create();
        $users = User::factory()->times(3)->create();
        $group->users()->attach($users);
        $this->assertDatabaseHas('groups_has_users', ['group_id'=>$group->id]);
        Passport::actingAs(User::factory()->create());

        $response = $this->getJson(route('groups.users.show', $group->id));

        $response->assertJsonCount(3)
        ->assertStatus(200);
    }
}
