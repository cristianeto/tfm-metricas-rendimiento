<?php

namespace Tests\Feature\Groups;

use App\Models\Group;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ListGroupsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_cannot_fetch_groups()
    {
        Group::factory()->times(3)->create();

        $response = $this->getJson(route('groups.index'));

        $response->assertExactJson([
            'message' => "Usuario no autenticado!"
        ])->assertStatus(401);
    }

    /** @test */
    public function admin_groups_role_can_fetch_single_group()
    {
        $user = User::factory()->create();
        $user->assignRole('Admin groups');
        $group = Group::factory()->create();
        Passport::actingAs($user);

        $response = $this->getJson(route('groups.show', $group->id));

        $response->assertJsonFragment([
            "id" => $group->id,
            "name" => $group->name,
        ])->assertStatus(200);
    }

    /** @test */
    public function admin_groups_role_can_fetch_all_groups()
    {
        $user = User::factory()->create();
        $user->assignRole('Admin groups');
        $groups = Group::factory()->times(3)->create();
        Passport::actingAs($user);

        $response = $this->getJson(route('groups.index'));

        $response->assertSee($groups[0]->name)
            ->assertSee($groups[1]->name)
            ->assertSee($groups[2]->name)
            ->assertStatus(200);
    }

    /** @test */
    public function cimogsys_role_can_fetch_all_groups()
    {
        $user = User::factory()->create();
        $groups = Group::factory()->times(3)->create();
        $user->assignRole('Cimogsys');
        Passport::actingAs($user);

        $response = $this->getJson(route('groups.index'));

        $response->assertSee($groups[0]->name)
            ->assertSee($groups[1]->name)
            ->assertSee($groups[2]->name);
    }

    /** @test */
    public function another_role_cannot_fetch_all_groups()
    {
        $user = User::factory()->create();
        $groups = Group::factory()->times(3)->create();
        $user->assignRole('Rector');
        Passport::actingAs($user);

        $response = $this->getJson(route('groups.index'));

        $response->assertExactJson([
            "message" => "Esta acción no está autorizada.",
        ])->assertStatus(403);
    }
}
