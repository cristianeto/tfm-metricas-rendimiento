<?php

namespace Tests\Feature\Activities;

use App\Models\Activity;
use App\Models\Component;
use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CreateActivitiesTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_cannot_create_activities()
    {
        $activity = Activity::factory()->raw();

        $response = $this->postJson(route('activities.store', $activity));

        $response->assertStatus(401)
                ->assertSee('Usuario no autenticado!');

        $this->assertDatabaseMissing('activities', $activity);
    }

    /** @test */
    public function member_project_can_create_activities()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create(['project_id'=>$project->id]);
        $activity = Activity::factory()->raw(['component_id' => $component->id]);
        Passport::actingAs($user);
        $this->assertDatabaseMissing('activities', $activity);
        //dd($activity);
        $this->postJson(route('activities.store', $activity))
            ->assertCreated();

        $this->assertDatabaseHas('activities', $activity);
    }

    /** @test */
    public function another_users_cannot_create_activities()
    {
        $activity = Activity::factory()->raw();
        Passport::actingAs(User::factory()->create());
        $this->assertDatabaseMissing('activities', $activity);

        $response = $this->postJson(route('activities.store', $activity));

        $response->assertStatus(403)
                ->assertJson(["message"=>"Esta acción no está autorizada."]);
        $this->assertDatabaseMissing('activities', $activity);
    }

    /** @test */
    public function name_is_required()
    {
        $activity = Activity::factory()->raw(['name'=> '']);
        $this->assertDatabaseMissing('activities', $activity);
        Passport::actingAs(User::factory()->create());

        $response = $this->postJson(route('activities.store', $activity));

        $response->assertStatus(422)
            ->assertSee('El campo nombre es obligatorio.');
        $this->assertDatabaseMissing('activities', $activity);
    }

    /** @test */
    public function responsable_is_required()
    {
        $activity = Activity::factory()->raw(['responsable'=> '']);
        $this->assertDatabaseMissing('activities', $activity);
        Passport::actingAs(User::factory()->create());

        $response = $this->postJson(route('activities.store', $activity));

        $response->assertStatus(422)
            ->assertSee('El campo responsable es obligatorio.');
        $this->assertDatabaseMissing('activities', $activity);
    }
}
