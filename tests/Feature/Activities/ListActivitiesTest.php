<?php

namespace Tests\Feature\Activities;

use App\Models\Activity;
use App\Models\Component;
use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ListActivitiesTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_users_cannot_fetch_activities_by_component()
    {
        $project = Project::factory()->create();
        $component = Component::factory()->create(['project_id'=>$project->id]);

        $response = $this->getJson(route('components.activities.show', $component->id));

        $response->assertStatus(401)
            ->assertExactJson([
                "message" => "Usuario no autenticado!"]);
    }

    /** @test */
    public function members_of_project_can_fetch_activities_by_component()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create(['project_id'=>$project->id]);
        Activity::factory()->times(3)->create(['component_id'=>$component->id]);

        Activity::factory()->times(2)->create();
        Passport::actingAs($user);

        $response = $this->getJson(route('components.activities.show', $component->id));

        $response->assertJsonCount(3)
            ->assertStatus(200);
    }
    /** @test */
    public function another_users_can_fetch_activities_by_component()
    {
        $project = Project::factory()->create();
        $component = Component::factory()->create(['project_id'=>$project->id]);
        Activity::factory()->times(3)->create(['component_id'=>$component->id]);
        Activity::factory()->times(2)->create();
        Passport::actingAs(User::factory()->create());

        $response = $this->getJson(route('components.activities.show', $component->id));

        $response->assertJsonCount(3)
            ->assertStatus(200);
    }

    /** @test */
    public function another_users_can_fetch_a_single_activity()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $component = Component::factory()->create(['project_id'=>$project->id]);
        $activity = Activity::factory()->create(['component_id'=>$component->id]);
        Passport::actingAs($user);

        $response = $this->getJson(route('activities.show', $activity->id));

        $response->assertJsonFragment([
          'name' => $activity->name,
        ])->assertStatus(200);
    }
}
