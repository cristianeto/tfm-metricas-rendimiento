<?php

namespace Tests\Feature\Projects;

use App\Models\Project;
use App\Models\Status;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ListProjectTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_cannot_fetch_single_project()
    {
        $project = Project::factory()->create();
        $response = $this->getJson(route('projects.show', $project->slug));
        $response->assertExactJson([
            "message" => "Usuario no autenticado!",
        ])
            ->assertStatus(401);
    }

    /** @test */
    public function members_of_project_can_fetch_single_project()
    {
        $userAuthenticated = Passport::actingAs(User::factory()->create());
        $project = Project::factory()->create();
        $project->users()->sync($userAuthenticated->id);
        $response = $this->getJson(route('projects.show', $project->slug));
        //$this->withoutExceptionHandling();
        $response->assertJsonFragment([
            "id" => $project->id,
            "name" => $project->name,
        ])
            ->assertStatus(200);
    }

    /** @test */
    public function another_users_cannot_fetch_single_project()
    {
        $user = User::factory()->create();
        Passport::actingAs(User::factory()->create());
        $project = Project::factory()->create();
        $project->users()->sync($user->id);
        $response = $this->getJson(route('projects.show', $project->slug));
        //$this->withoutExceptionHandling();
        $response->assertExactJson([
            "message" => "Esta acción no está autorizada.",
        ])
            ->assertStatus(403);
    }

    /** @test */
    public function cimogsys_role_can_fetch_all_projects()
    {

        //$this->withoutExceptionHandling();
        $user = User::factory()->create();
        //$role = Factory(Role::class)->create(['name' => 'Cimogsys']);
        $user->assignRole('Cimogsys');
        Passport::actingAs($user);
        $projects = Project::factory()->times(2)->create();
        $response = $this->getJson(route('projects.index'));

        /*        $response->assertExactJson([
            [
                "id" => $projects[0]->id,
                "code" => $projects[0]->code,
                "name" => $projects[0]->name,
                "slug" => $projects[0]->slug,
                "kind" => $projects[0]->kind,
                "startDate" => $projects[0]->startDate,
                "endDate" => $projects[0]->endDate,
                "endDateReal" => $projects[0]->endDateReal,
                "location" => $projects[0]->location,
                "project_type_id" => $projects[0]->project_type_id,
                "research_type_id" => $projects[0]->research_type_id,
                "coverage_type_id" => $projects[0]->coverage_type_id,
                "program_id" => $projects[0]->program_id,
                "year" => $projects[0]->year,
                "father" => $projects[0]->father,
                "id_group" => $projects[0]->id_group,
                "created_at" => Carbon::parse($projects[0]->created_at)->format('d-M-Y H:i:s'),
                "updated_at" => Carbon::parse($projects[0]->updated_at)->format('d-M-Y H:i:s'),
                "deleted_at" => $projects[0]->deleted_at,
                "human_updated_at" => $projects[0]->human_updated_at,
                "project_type" => [
                    "id" => $projects[0]->projectType->id,
                    "name" => $projects[0]->projectType->name,
                ],
                "research_type" => [
                    "id" => $projects[0]->researchType->id,
                    "name" => $projects[0]->researchType->name,
                ],
                "coverage_type" => [
                    "id" => $projects[0]->coverageType->id,
                    "name" => $projects[0]->coverageType->name,
                ],
                "program" => [
                    "id" => $projects[0]->program->id,
                    "name" => $projects[0]->program->name,
                ],
                "impact_sectors" => [],
                "users" => [],

                "summary" => $projects[0]->summary,
                "background" => $projects[0]->background,
                "justification" => $projects[0]->justification,
                'current_situation' => $projects[0]->current_situation,
                'general_objective' => $projects[0]->general_objective,
                'specific_objectives' => $projects[0]->specific_objectives,
                'sustainability' => $projects[0]->sustainability,
                'methodology' => $projects[0]->methodology,
                'expected_results' => $projects[0]->expected_results,
                'transference_results' => $projects[0]->transference_results,
                'beneficiaries' => $projects[0]->beneficiaries,
                'impacts' => $projects[0]->impacts,
                'aspects' => $projects[0]->aspects,
            ],
            [
                "id" => $projects[1]->id,
                "code" => $projects[1]->code,
                "name" => $projects[1]->name,
                "slug" => $projects[1]->slug,
                "kind" => $projects[1]->kind,
                "startDate" => $projects[1]->startDate,
                "endDate" => $projects[1]->endDate,
                "endDateReal" => $projects[1]->endDateReal,
                "location" => $projects[1]->location,
                "project_type_id" => $projects[1]->project_type_id,
                "research_type_id" => $projects[1]->research_type_id,
                "coverage_type_id" => $projects[1]->coverage_type_id,
                "program_id" => $projects[1]->program_id,
                "year" => $projects[1]->year,
                "father" => $projects[1]->father,
                "id_group" => $projects[1]->id_group,
                "created_at" =>Carbon::parse($projects[1]->created_at)->format('d-M-Y H:i:s'),
                "updated_at" =>Carbon::parse($projects[1]->updated_at)->format('d-M-Y H:i:s'),
                "deleted_at" => $projects[1]->deleted_at,
                "human_updated_at" => $projects[1]->human_updated_at,
                "project_type" => [
                    "id" => $projects[1]->projectType->id,
                    "name" => $projects[1]->projectType->name,
                ],
                "research_type" => [
                    "id" => $projects[1]->researchType->id,
                    "name" => $projects[1]->researchType->name,
                ],
                "coverage_type" => [
                    "id" => $projects[1]->coverageType->id,
                    "name" => $projects[1]->coverageType->name,
                ],
                "program" => [
                    "id" => $projects[1]->program->id,
                    "name" => $projects[1]->program->name,
                ],
                "impact_sectors" => [],
                "users" => [],

                "summary" => $projects[1]->summary,
                "background" => $projects[1]->background,
                "justification" => $projects[1]->justification,
                'current_situation' => $projects[1]->current_situation,
                'general_objective' => $projects[1]->general_objective,
                'specific_objectives' => $projects[1]->specific_objectives,
                'sustainability' => $projects[1]->sustainability,
                'methodology' => $projects[1]->methodology,
                'expected_results' => $projects[1]->expected_results,
                'transference_results' => $projects[1]->transference_results,
                'beneficiaries' => $projects[1]->beneficiaries,
                'impacts' => $projects[1]->impacts,
                'aspects' => $projects[1]->aspects,
            ],
        ])
            ->assertStatus(200);*/
        $response->assertSee($projects[0]->name)
                ->assertSee($projects[1]->name)
                ->assertStatus(200);
    }

    /** @test */
    public function director_IDI_role_can_fetch_all_projects()
    {

        //$this->withoutExceptionHandling();
        $user = User::factory()->create();
        //$role = Factory(Role::class)->create(['name' => 'Cimogsys']);
        $user->assignRole('Director IDI');
        Passport::actingAs($user);
        $projects = Project::factory()->times(3)->create();

        $response = $this->getJson(route('projects.index'));

        $response->assertSee($projects[0]->name)
                ->assertSee($projects[1]->name)
                ->assertSee($projects[2]->name)
                ->assertStatus(200);
    }

    /** @test */
    public function view_projects_permission_can_fetch_all_projects()
    {
        $user = User::factory()->create();
        $projects = Project::factory()->times(3)->create();
        $user->givePermissionTo('View projects');
        Passport::actingAs($user);
        $response = $this->getJson(route('projects.index'));
        //$this->withoutExceptionHandling();
        $response->assertSee($projects[0]->name)
            ->assertSee($projects[1]->name)
            ->assertSee($projects[2]->name)
            ->assertStatus(200);
    }
}
