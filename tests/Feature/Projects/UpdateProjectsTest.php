<?php

namespace Tests\Feature\Projects;

use App\Models\CoverageType;
use App\Models\ImpactSector;
use App\Models\Program;
use App\Models\Project;
use App\Models\ProjectType;
use App\Models\ResearchType;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class UpdateProjectsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_users_cannot_update_a_project()
    {
        $project = Project::factory()->create();

        $this->putJson(route('projects.update', $project))
            ->assertStatus(401)
            ->assertSee('Usuario no autenticado!');

    }

    /** @test */
    public function members_of_project_can_update_a_project()
    {
        //$this->withoutExceptionHandling();
        $user = User::factory()->create();
        $project = Project::factory()->create();
        //dd($project);
        $user->projects()->attach($project->id, ['staff_id'=>2]);
        $user->assignRole('Investigador');
        Passport::actingAs($user);

        $this->putJson(route('projects.update', $project), [
            'name'=>'Name has been changed',
            'slug'=>'name',
            'kind'=>'Institucional',
            'startDate'=> '2020-08-01',
            'endDate'=> '2020-10-01',
            'location'=> 'New York city',
            'project_type_id' =>ProjectType::factory()->create()->id,
            'research_type_id' =>ResearchType::factory()->create()->id,
            'coverage_type_id' =>CoverageType::factory()->create()->id,
            'program_id' =>Program::factory()->create()->id,
            'sectors' =>ImpactSector::take(2)->get(),

            'summary' => 'Este es un resumen actualizado',
            'background'=> 'Este es el antecedente actualizado',
            'justification' => 'Este es una justificación actualizado',
            'current_situation' => 'Esta es el análisis de la situación actual, actualizado',
            'general_objective' => 'Esta es el objetivo general actualizado',
            'specific_objectives' => 'Esta son los objetivos específicos actualizados',
            'sustainability' => 'Esta es la sostenibilidad actualizada',
            'methodology' => 'Esta es la metodología actualizada',
            'expected_results' => 'Estos son los resultados esperados, actualizados',
            'transference_results' => 'Estos son los resultados esperados, actualizados',
            'beneficiaries' => 'Estos son los beneficiarios, actualizados',
            'impacts' => 'Estos son los impactos, actualizados',
            'aspects' => 'Estos son los aspectos, actualizados',

        ])->assertStatus(200);

        $this->assertDatabaseHas('project', [
            'name' => 'Name has been changed',
            'slug' => 'name-has-been-changed',
            'kind'=> 'Institucional',

            'summary' => 'Este es un resumen actualizado',
            'background'=> 'Este es el antecedente actualizado',
            'justification' => 'Este es una justificación actualizado',
            'current_situation' => 'Esta es el análisis de la situación actual, actualizado',
            'general_objective' => 'Esta es el objetivo general actualizado',
            'specific_objectives' => 'Esta son los objetivos específicos actualizados',
            'sustainability' => 'Esta es la sostenibilidad actualizada',
            'methodology' => 'Esta es la metodología actualizada',
            'expected_results' => 'Estos son los resultados esperados, actualizados',
            'transference_results' => 'Estos son los resultados esperados, actualizados',
            'beneficiaries' => 'Estos son los beneficiarios, actualizados',
            'impacts' => 'Estos son los impactos, actualizados',
            'aspects' => 'Estos son los aspectos, actualizados',
        ]);
    }


    /** @test */
    public function another_users_cannot_update_a_project()
    {
        //$this->withoutExceptionHandling();
        $user = User::factory()->create();
        $project = Project::factory()->create();
        //dd($project);
        //$user->projects()->attach($project->id, ['staff_id'=>2]);
        $user->assignRole('Investigador');
        Passport::actingAs($user);

        $this->putJson(route('projects.update', $project), [
            'name'=>'Name has been changed',
            'slug'=>'name',
            'kind'=>'Institucional',
            'startDate'=> '2020-08-01',
            'endDate'=> '2020-10-01',
            'location'=> 'New York city',
            'project_type_id' =>ProjectType::factory()->create()->id,
            'research_type_id' =>ResearchType::factory()->create()->id,
            'coverage_type_id' =>CoverageType::factory()->create()->id,
            'program_id' =>Program::factory()->create()->id,
            'sectors' =>ImpactSector::take(2)->get(),

            'summary' => 'Este es un resumen actualizado',
            'background'=> 'Este es el antecedente actualizado',
            'justification' => 'Este es una justificación actualizado',
            'current_situation' => 'Esta es el análisis de la situación actual, actualizado',
            'general_objective' => 'Esta es el objetivo general actualizado',
            'specific_objectives' => 'Esta son los objetivos específicos actualizados',
            'sustainability' => 'Esta es la sostenibilidad actualizada',
            'methodology' => 'Esta es la metodología actualizada',
            'expected_results' => 'Estos son los resultados esperados, actualizados',
            'transference_results' => 'Estos son los resultados esperados, actualizados',
            'beneficiaries' => 'Estos son los beneficiarios, actualizados',
            'impacts' => 'Estos son los impactos, actualizados',
            'aspects' => 'Estos son los aspectos, actualizados',

        ])
            ->assertStatus(403)
            ->assertExactJson(['message'=>'Esta acción no está autorizada.']);
    }


}
