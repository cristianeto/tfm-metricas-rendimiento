<?php

namespace Tests\Feature\Projects;

use App\Models\Activity;
use App\Models\Component;
use App\Models\Project;
use App\Models\Requirement;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ListBudgetProjectsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function users_can_fetch_total_budget_of_a_project()
    {
        $this->withoutExceptionHandling();
        $project= Project::factory()->create(['name'=>'Proyecto de islas galápagos']);
        $components = Component::factory()->times(4)->create(['project_id'=>$project->id]);
        $activities = Activity::factory()
                ->times(5)
            ->state(new Sequence(
                ['component_id'=>$components[0]],
                ['component_id'=>$components[1]],
                ['component_id'=>$components[2]]
            ))->create();

        $requirements = Requirement::factory()
            ->times(7)
            ->state(new Sequence(
                ['activity_id' => $activities[0]],
                ['activity_id' => $activities[0]],
                ['activity_id' => $activities[1]],
                ['activity_id' => $activities[2]],
                ['activity_id' => $activities[3]],
                ['activity_id' => $activities[4]],
                ['activity_id' => $activities[4]]
            ))->create();
        Passport::actingAs(User::factory()->create());

        $totalBudget = 0;
        foreach ($requirements as $req){
            $totalBudget+=$req->total_price;
        }

        $response = $this->getJson(route('projects.budget.show', $project->slug));

        $response->assertSee($totalBudget);
    }
}
