<?php

namespace Tests\Feature\Projects;

use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CreateProjectsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_users_cannot_create_a_project()
    {
        $project = ['name'=>'Mi nuevo proyecto'];

        $this->postJson(route('projects.store', $project))
            ->assertStatus(401)
        ->assertSee('Usuario no autenticado!');

        $this->assertDatabaseMissing('project', $project);
    }

    /** @test */
    public function authenticated_users_can_create_a_project()
    {
        $user = User::factory()->create();
        $project = ['name'=>'Mi nuevo proyecto'];
        $user->assignRole('Investigador');
        Passport::actingAs($user);
        $this->assertDatabaseMissing('project', $project);

        $this->postJson(route('projects.store', $project))->assertCreated();

        $this->assertDatabaseHas('project', $project);
    }

    /** @test */
    public function name_is_required()
    {
        $user = User::factory()->create();
        $project = ['name'=>''];
        $user->assignRole('Investigador');
        Passport::actingAs($user);

        $this->postJson(route('projects.store', $project))
             ->assertStatus(422)
             ->assertSee('El campo nombre es obligatorio.');

        $this->assertDatabaseMissing('project', $project);
    }

    /** @test */
    public function slug_is_required()
    {
        $user = User::factory()->create();
        $project = ['slug'=>''];
        $user->assignRole('Investigador');
        Passport::actingAs($user);

        $this->postJson(route('projects.store', $project))
             ->assertStatus(422)
             ->assertSee('El campo slug es obligatorio.');

        $this->assertDatabaseMissing('project', $project);
    }

    /** @test */
    public function slug_must_be_unique()
    {
        Project::factory()->create(['name'=>'same slug', 'slug'=>'same-slug']);
        $user = User::factory()->create();
        $project =  Project::factory()->raw(['name'=>'same slug', 'slug'=>'same-slug']);
        $user->assignRole('Investigador');
        Passport::actingAs($user);

        $this->postJson(route('projects.store', $project))
             ->assertStatus(422)
             ->assertSee('Los datos proporcionados no son v\u00e1lidos.')
             ->assertSee('El campo slug ya ha sido registrado.');

        $this->assertDatabaseMissing('project', $project);
    }
}
