<?php

namespace Tests\Feature\Components;

use App\Models\Component;
use App\Models\Project;
use Tests\TestCase;
use App\Models\User;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeleteComponentsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_cannot_delete_a_component()
    {
        $component = Component::factory()->create();

        $this->deleteJson(route('components.destroy', $component))
            ->assertExactJson(['message'=>'Usuario no autenticado!'])
            ->assertStatus(401);
    }

    /** @test */
    public function another_users_cannot_delete_a_component()
    {
        $component = Component::factory()->create();
        Passport::actingAs(User::factory()->create());

        $this->deleteJson(route('components.destroy', $component))
            ->assertExactJson(['message'=>'Esta acción no está autorizada.'])
            ->assertStatus(403);
    }

    /** @test */
    public function members_of_project_can_delete_a_component()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create(['project_id' => $project->id]);
        Passport::actingAs($user);

        $this->deleteJson(route('components.destroy', $component->id))
            ->assertStatus(204);

        $this->assertDatabaseMissing('components', $component->attributesToArray());
    }

    /** @test */
    public function cimogsys_role_can_delete_components()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $user->assignRole('Cimogsys');
        $component = Component::factory()->create(['project_id' => $project->id]);
        Passport::actingAs($user);

        $this->deleteJson(route('components.destroy', $component->id))
            ->assertStatus(204);
        $this->assertDatabaseMissing('components', $component->attributesToArray());
    }

    /** @test */
    public function another_role_cannot_delete_components()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $user->assignRole('Rector');
        $component = Component::factory()->create(['project_id' => $project->id]);
        Passport::actingAs($user);

        $this->deleteJson(route('components.destroy', $component->id))
            ->assertExactJson(['message'=>'Esta acción no está autorizada.'])
            ->assertStatus(403);
    }
}
