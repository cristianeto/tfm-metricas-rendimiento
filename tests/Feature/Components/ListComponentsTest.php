<?php

namespace Tests\Feature\Components;

use App\Models\Component;
use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ListComponentsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function authenticated_users_can_fetch_all_components()
    {
        //$this->withoutExceptionHandling();

        $user = User::factory()->create();
        Passport::actingAs($user);
        $components = Component::factory()->times(3)->create();

        $response = $this->getJson(route('components.index'));

        $response->assertSee($components[0]->name)
            ->assertSee($components[1]->name)
            ->assertSee($components[2]->name)
            ->assertStatus(200);
    }

    /** @test */
    public function guests_users_cannot_fetch_all_components()
    {
        $components = Component::factory()->times(3)->create();

        $response = $this->getJson(route('components.index'));

        $response->assertExactJson([
            "message" => "Usuario no autenticado!",
        ])->assertStatus(401);
    }

    /** @test */
    public function members_of_project_can_fetch_components_their_project()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        Component::factory()->times(3)->create(['project_id'=>$project->id]);
        Component::factory()->times(2)->create();
        Passport::actingAs($user);

        $response = $this->getJson(route('projects.components.show', $project->slug));

        $response->assertJsonCount(3)
            ->assertStatus(200);
    }

    /** @test */
    public function another_users_can_fetch_components()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        Component::factory()->times(3)->create(['project_id'=>$project->id]);
        Component::factory()->times(2)->create();
        Passport::actingAs($user);

        $response = $this->getJson(route('projects.components.show', $project->slug));

        $response->assertJsonCount(3)
            ->assertStatus(200);
    }


}
