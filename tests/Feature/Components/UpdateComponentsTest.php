<?php

namespace Tests\Feature\Components;

use App\Models\Component;
use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class UpdateComponentsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function authenticated_members_of_project_can_update_their_components()
    {
        $user = User::factory()->create();
        $project = Project::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create(['project_id' => $project->id]);
        Passport::actingAs($user);

        $this->putJson(route('components.update', $component),[
            'name' => 'name has been changed',
            'project_id' => $project->id,
        ])->assertStatus(200);

        $this->assertDatabaseHas('components', [
            'name' => 'name has been changed',
            'project_id' => $project->id,
        ]);
    }

    /** @test */
    public function users_not_members_of_project_cannot_update_components()
    {
        $user = User::factory()->create();
        $project = Project::factory()->create();
        $component = Component::factory()->create();
        Passport::actingAs($user);

        $this->putJson(route('components.update', $component),[
            'name' => 'name has been changed',
            'project_id' => $project->id,
        ])->assertStatus(403);
    }

    /** @test */
    public function guests_cannot_update_components()
    {
        Project::factory()->create();
        $component = Component::factory()->create();

        $this->putJson(route('components.update', $component))
            ->assertExactJson([
                "message" => "Usuario no autenticado!",
            ])
            ->assertStatus(401);
    }

    /** @test */
    public function name_is_required()
    {
        $user = User::factory()->create();
        $project = Project::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create();
        Passport::actingAs($user);

        $this->putJson(route('components.update', $component),[
            'name' => '',
            'project_id' => $project->id,
        ])->assertStatus(422)
        ->assertSee('Los datos proporcionados no son v\u00e1lidos.')
        ->assertSee('El campo nombre es obligatorio.');
    }

    /** @test */
    public function project_is_required()
    {
        $user = User::factory()->create();
        $project = Project::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create();
        Passport::actingAs($user);

        $this->putJson(route('components.update', $component),[
            'name' => 'name has been changed',
            'project_id' => '',
        ])->assertStatus(422)
        ->assertSee('Los datos proporcionados no son v\u00e1lidos.')
        ->assertSee('El campo proyecto es obligatorio.');
    }
}
