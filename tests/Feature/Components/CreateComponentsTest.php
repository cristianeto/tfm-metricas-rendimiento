<?php

namespace Tests\Feature\Components;

use App\Models\Project;
use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\Models\Component;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateComponentsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_cannot_create_a_component()
    {
        $component = Component::factory()->create();

        $this->postJson(route('components.store', $component))
            ->assertExactJson(['message'=>'Usuario no autenticado!'])
            ->assertStatus(401);
    }

    /** @test */
    public function member_project_can_create_component()
    {
        $this->withoutExceptionHandling();
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->raw(['project_id'=>$project->id]);
        Passport::actingAs($user);
        $this->assertDatabaseMissing('components', $component);

        $this->postJson(route('components.store', $component))
            ->assertCreated();

        $this->assertDatabaseHas('components', $component);
    }

    /** @test */
    public function member_project_can_create_component_by_project()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
//        $component = Component::factory()->raw(['project_id'=>$project->id]);
        $component= [
            'name' => 'any Name',
            'project_slug' => $project->slug,
            'meta' => 'any Meta'
        ];
        Passport::actingAs($user);
//        $this->assertDatabaseMissing('components', $component);

        $this->postJson(route('projects.components.store', $component))
            ->assertCreated();

//        $this->assertDatabaseHas('components', $component);
    }

    /** @test */
    public function cimogsys_role_can_create_component()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $component = Component::factory()->raw(['project_id'=>$project->id]);
        $user->assignRole('Cimogsys');
        Passport::actingAs($user);
        $this->assertDatabaseMissing('components', $component);

        $this->postJson(route('components.store', $component))
            ->assertCreated();

        $this->assertDatabaseHas('components', $component);
    }

    /** @test */
    public function another_user_cannot_create_component()
    {
        $component = Component::factory()->raw();
        Passport::actingAs(User::factory()->create());

        $this->postJson(route('components.store', $component))
            ->assertExactJson([
                "message" => "Esta acción no está autorizada.",
            ])
            ->assertStatus(403);
    }

    /** @test */
    public function name_is_required()
    {
        $component = Component::factory()->raw(['name'=>'']);
        Passport::actingAs(User::factory()->create());

        $this->postJson(route('components.store'), $component)
            ->assertStatus(422)
            ->assertSee('Los datos proporcionados no son v\u00e1lidos.')
            ->assertSee('El campo nombre es obligatorio.');

        $this->assertDatabaseMissing('components', $component);
    }

    /** @test */
    public function create_component_by_project_name_max_50_characters()
    {
        $project = Project::factory()->create();
        //$component = Component::factory()->raw(['name'=>'Conseguir multiples capacitadores en el area de sis']);
        $component= [
            'name' => 'Conseguir multiples capacitadores en el area de sis',
            'project_slug' => $project->slug,
            'meta' => 'any Meta'
        ];
        $user = User::factory()->create();
        $project->users()->save($user);
        Passport::actingAs($user);

        $response = $this->postJson(route('projects.components.store'), $component);

        $response->assertStatus(422)
            ->assertSee('Los datos proporcionados no son v\u00e1lidos.')
            ->assertSee('nombre no debe ser mayor que 50 caracteres.');

        //$this->assertDatabaseMissing('components', $component);
    }

    /** @test */
    public function project_is_required()
    {
        $component = Component::factory()->raw(['project_id'=>'']);
        Passport::actingAs(User::factory()->create());

        $this->postJson(route('components.store'), $component)
            ->assertStatus(422)
            ->assertSee('Los datos proporcionados no son v\u00e1lidos.')
            ->assertSee('El campo proyecto es obligatorio.');

        $this->assertDatabaseMissing('components', $component);
    }
}
