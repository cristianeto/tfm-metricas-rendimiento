<?php

namespace Tests\Feature\Requirements;

use App\Models\Activity;
use App\Models\Component;
use App\Models\Project;
use App\Models\Requirement;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CreateRequirementsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_cannot_create_requirements()
    {
        $requirement = Requirement::factory()->raw();

        $response = $this->postJson(route('requirements.store', $requirement));

        $response->assertStatus(401)
            ->assertSee('Usuario no autenticado!');

        $this->assertDatabaseMissing('requirements', $requirement);
    }

    /** @test */
    public function members_of_project_can_create_a_requirement()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create(['project_id'=>$project->id]);
        $activity = Activity::factory()->create(['component_id' => $component->id]);
        $requirement = Requirement::factory()->raw(['activity_id'=> $activity->id]);
        Passport::actingAs($user);
        $this->assertDatabaseMissing('requirements', $requirement);

        $this->postJson(route('requirements.store', $requirement))
            ->assertCreated();

        $this->assertDatabaseHas('requirements', $requirement);


    }
    /** @test */
    public function another_users_cannot_create_requirements()
    {
        $requirement = Requirement::factory()->raw();
        Passport::actingAs(User::factory()->create());
        $this->assertDatabaseMissing('requirements', $requirement);

        $response = $this->postJson(route('requirements.store', $requirement));

        $response->assertStatus(403)
                ->assertJson(["message"=>"Esta acción no está autorizada."]);
        $this->assertDatabaseMissing('requirements', $requirement);
    }

    /** @test */
    public function name_is_required()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create(['project_id'=>$project->id]);
        $activity = Activity::factory()->create(['component_id' => $component->id]);
        $requirement = Requirement::factory()->raw(['name'=>'','activity_id'=> $activity->id]);
        Passport::actingAs($user);
        $this->assertDatabaseMissing('requirements', $requirement);

        $response = $this->postJson(route('requirements.store',$requirement));

        $response->assertStatus(422)
            ->assertSee('El campo nombre es obligatorio.');
        $this->assertDatabaseMissing('requirements', $requirement);
    }

    /** @test */
    public function quantity_is_required(){

        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create(['project_id'=>$project->id]);
        $activity = Activity::factory()->create(['component_id' => $component->id]);
        $requirement = Requirement::factory()->raw(['quantity'=>'','activity_id'=> $activity->id]);
        Passport::actingAs($user);
        $this->assertDatabaseMissing('requirements', $requirement);

        $response = $this->postJson(route('requirements.store',$requirement));

        $response->assertStatus(422)
            ->assertSee('El campo cantidad es obligatorio.');
        $this->assertDatabaseMissing('requirements', $requirement);

    }

    /** @test */
    public function price_is_required(){

        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create(['project_id'=>$project->id]);
        $activity = Activity::factory()->create(['component_id' => $component->id]);
        $requirement = Requirement::factory()->raw(['price'=>'','activity_id'=> $activity->id]);
        Passport::actingAs($user);
        $this->assertDatabaseMissing('requirements', $requirement);

        $response = $this->postJson(route('requirements.store',$requirement));

        $response->assertStatus(422)
            ->assertSee('El campo precio es obligatorio.');
        $this->assertDatabaseMissing('requirements', $requirement);

    }

    /** @test */
    public function total_price_is_required()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create(['project_id'=>$project->id]);
        $activity = Activity::factory()->create(['component_id' => $component->id]);
        $requirement = Requirement::factory()->raw(['total_price'=>'','activity_id'=> $activity->id]);
        Passport::actingAs($user);
        $this->assertDatabaseMissing('requirements', $requirement);

        $response = $this->postJson(route('requirements.store',$requirement));

        $response->assertStatus(422)
                ->assertSee('El campo precio total es obligatorio.');
        $this->assertDatabaseMissing('requirements', $requirement);

    }

    /** @test */
    public function activity_is_required()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create(['project_id'=>$project->id]);
        $activity = Activity::factory()->create(['component_id' => $component->id]);
        $requirement = Requirement::factory()->raw(['activity_id'=>'']);
        Passport::actingAs($user);
        $this->assertDatabaseMissing('requirements', $requirement);

        $response = $this->postJson(route('requirements.store',$requirement));

        $response->assertStatus(422)
                ->assertSee('El campo actividad es obligatorio.');
        $this->assertDatabaseMissing('requirements', $requirement);

    }
}
