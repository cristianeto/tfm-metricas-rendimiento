<?php

namespace Tests\Feature\Requirements;

use App\Models\Activity;
use App\Models\Component;
use App\Models\Project;
use App\Models\Requirement;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ListRequirementsTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function guests_cannot_fetch_requirements_by_component()
    {
        $component = Component::factory()->create();
        Activity::factory()->times(3)->create(['component_id'=>$component->id]);
        Requirement::factory()->times(5)->create();
        Requirement::factory()->times(2)->create();

        $response = $this->getJson(route('components.requirements.show', $component->id));

        $response
            ->assertSee('Usuario no autenticado!')
            ->assertStatus(401);
    }

    /** @test */
    public function members_of_project_can_fetch_requirements_by_component()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $project->users()->save($user);
        $component = Component::factory()->create(['project_id'=>$project->id]);
        $activities = Activity::factory()->times(3)->create(['component_id'=>$component->id]);
        $requirements = Requirement::factory()
            ->times(5)
            ->state(new Sequence(
                ['activity_id' => $activities[0]->id],
                ['activity_id' => $activities[0]->id],
                ['activity_id' => $activities[0]->id],
                ['activity_id' => $activities[1]->id],
                ['activity_id' => $activities[2]->id]
                ))
            ->create();
        Passport::actingAs($user);
        Requirement::factory()->times(2)->create();

        $response = $this->getJson(route('components.requirements.show', $component->id));

        $response->assertJsonCount(5)
            ->assertSee($requirements[0]->name)
            ->assertSee($requirements[1]->name)
            ->assertSee($requirements[2]->name)
            ->assertSee($requirements[3]->name)
            ->assertSee($requirements[4]->name)
            ->assertStatus(200);
    }

    /** @test */
    public function another_users_can_fetch_requirements_by_component()
    {
        $project = Project::factory()->create();
        $user = User::factory()->create();
        $component = Component::factory()->create(['project_id'=>$project->id]);
        $activities = Activity::factory()->times(3)->create(['component_id'=>$component->id]);
        $requirements = Requirement::factory()
            ->times(5)
            ->state(new Sequence(
                ['activity_id' => $activities[0]->id],
                ['activity_id' => $activities[0]->id],
                ['activity_id' => $activities[0]->id],
                ['activity_id' => $activities[1]->id],
                ['activity_id' => $activities[2]->id]
                ))
            ->create();
        Passport::actingAs($user);
        Requirement::factory()->times(2)->create();

        $response = $this->getJson(route('components.requirements.show', $component->id));

        $response->assertJsonCount(5)
            ->assertSee($requirements[0]->name)
            ->assertSee($requirements[1]->name)
            ->assertSee($requirements[2]->name)
            ->assertSee($requirements[3]->name)
            ->assertSee($requirements[4]->name)
            ->assertStatus(200);
    }

    /** @test */
    public function another_users_can_fetch_a_single_requirement()
    {
        $user = User::factory()->create();
        $requirement = Requirement::factory()->create();
        Passport::actingAs($user);

        $response = $this->getJson(route('requirements.show', $requirement->id));
        $response->assertSee($requirement->name)
            ->assertStatus(200);
    }
}
