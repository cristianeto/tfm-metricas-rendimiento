<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //\Gate::authorize('update', $this->route('role'));//Se puede utilzar aqui la autorizaion
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'display_name' => 'required|min:3|max:25',
            //'name' => 'required|unique:roles|min:3|max:25',
        ];
        if ($this->method() != "PUT") {
            $rules['name'] = ['required|unique:roles|min:3|max:25',];
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'El identificador es obligatorio',
            'name.unique' => 'Este identificador ya ha sido registrado.',
            'display_name.required' => 'El nombre es obligatorio'
        ];
    }
}
