<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SaveUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'identification_card' => [
                'required',
                Rule::unique('users')->ignore($this->route('user')),
                'string',
                'min:10',
                'max:10'
            ],
            'name' => 'required|string|min:3|max:100|',
            'lastname' => 'required|min:3|max:100',
            'email' => [
                'required',
                Rule::unique('users')->ignore($this->route('user')),
                'email:rfc',
            ],
            'sex'=> 'required|string',
            'belongs_espoch'=> 'required|boolean',
            'password' => 'sometimes|string',
            'roles' => 'sometimes|array',
            'permissions' => 'sometimes|array',
        ];
    }
}
