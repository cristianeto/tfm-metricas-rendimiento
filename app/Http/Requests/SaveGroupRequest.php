<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SaveGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => [
                'required',
                'alpha_dash',
                Rule::unique('groups')->ignore($this->route('group')),
                'max:10',
            ],
            'name' => [
                'required',
                Rule::unique('groups')->ignore($this->route('group')),
                'max:255',
            ],
            'acronym' => [
                'required',
                'alpha_dash',
                Rule::unique('groups')->ignore($this->route('group')),
                'max:11',
            ],
            'mission' => 'required',
            'vision' => 'required',
            'dependency_id' => 'required',
            'group_type_id' => 'required',
            'programs' => 'required',
            'lines' => 'required',
            'active' => 'sometimes|required|boolean'
        ];
    }
    /* public function messages()
    {
        return [
            'code_group.required'=>'El grupo necesita un código',
            'code_group.unique'=>'Código no disponible',
            'name_group.required'=>'El grupo necesita un nombre',
            'name_group.unique'=>'Nombre no disponible',
            'acronym_group.unique'=>'Siglas no disponibles',
            'acronym_group.required'=>'El grupo necesita una siglas',
            'acronym_group.max'=>'Siglas no deben ser mayor a 11 caracteres',

        ];
    } */
}
