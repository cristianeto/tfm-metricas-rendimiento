<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SaveProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->route('project'));
        return [
            'name' => [
                'required',
                'max:500',
            ],
            'slug' => [
                'required',
                Rule::unique('project')->ignore($this->route('project')),
                'max:60',
            ],
            'kind' => [
                'required',
                'max:15',
            ],
            'startDate' => [
                'required',
            ],
            'endDate' => [
                'required',
            ],
            'endDateReal' => [
                'sometimes',
            ],
            'location' => [
                'required',
                'max:300',
            ],
            'project_type_id' => 'required',
            'research_type_id' => 'required',
            'coverage_type_id' => 'required',
            'program_id' => 'required',
            'sectors' => 'required',
            'active_group' => 'sometimes|required|boolean',

            'summary' =>'sometimes|max:500',
            'background' =>'sometimes|max:500',
            'justification' =>'sometimes|max:500',
            'current_situation' => 'sometimes|max:500',
            'general_objective' => 'sometimes|max:500',
            'specific_objectives' => 'sometimes|max:500',
            'sustainability' => 'sometimes|max:500',
            'methodology' => 'sometimes|max:500',
            'expected_results' => 'sometimes|max:500',
            'transference_results' => 'sometimes|max:500',
            'beneficiaries' => 'sometimes|max:500',
            'impacts' => 'sometimes|max:500',
            'aspects' => 'sometimes|max:500',
        ];
    }
}
