<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ResolutionController extends Controller
{
    public function show(Project $project){

        $project = Project::where('slug',$project->slug)->select('slug', 'resolution')->firstOrFail();
        if($project->resolution!==null)$project->resolution = env('APP_URL').'/'.$project->resolution;
        return response()->json($project, 200);
    }

    public function store(Project $project){
        $this->validate(request(), [
            'file' => 'required|mimes:pdf|max:2048'
        ]);
        /** Borrando el pdf anterior*/
        $publicPath = str_replace('storage', 'public', $project->resolution);
        Storage::disk('public')->delete($publicPath);

        /*Guardando en Storage y haciéndolo público*/
        $resolutionUrl =  request()->file('file')->store('projects','public');

        /*Añadiendo la url del sistema*/
        $completeUrl = env('APP_URL').'/'.$resolutionUrl;

        /*Guardando en la base*/
        $project->resolution = $resolutionUrl;
        $project->save();
        return response()->json($completeUrl,201 );
    }
}
