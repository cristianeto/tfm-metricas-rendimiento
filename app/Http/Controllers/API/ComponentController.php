<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\Component;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;

class ComponentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $components = Component::all();
        return response()->json($components, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'project_id' => 'required|exists:App\Models\Project,id',
        ]);
        $component = new Component(['project_id'=>$request->project_id]);
        $this->authorize('create', $component);
        $component = Component::create($request->all());
        return response()->json($component, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Component $component
     * @return JsonResponse
     * @throws ValidationException
     * @throws AuthorizationException
     */
    public function update(Request $request, Component $component)
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'project_id' => 'required|exists:App\Models\Project,id',
        ]);
        $this->authorize('update', $component);
        $componentUpdated = $component->update($request->all());
        return response()->json($componentUpdated, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Component $component
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Component $component)
    {
        $this->authorize('delete', $component);
        $component->delete();
        return response()->json(null, 204);
    }
}
