<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectStatus;
use Illuminate\Http\Request;

class ProjectStatusController extends Controller
{
    public function show(Project $project) {

        $projectStatus = ProjectStatus::where('project_id', $project->id)
            ->with(['user:id,name,lastname', 'status'])
            ->latest()
            ->take(3)
            ->get();
        return response()->json($projectStatus);
    }
}
