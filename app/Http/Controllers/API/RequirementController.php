<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveRequirementRequest;
use App\Models\Activity;
use App\Models\Component;
use App\Models\Project;
use App\Models\Requirement;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RequirementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(SaveRequirementRequest $request)
    {
        $requirement = new Requirement(['activity_id'=>$request->activity_id]);
        $this->authorize('create', $requirement);
        $requirement = Requirement::create($request->validated());
        $this->syncProjectStatusTable($request->activity_id);
        return response()->json($requirement,201);
    }

    /**
     * Display the specified resource.
     *
     * @param Requirement $requirement
     * @return JsonResponse
     */
    public function show(Requirement $requirement)
    {
        $requirement = Requirement::find($requirement->id);
        return response()->json($requirement);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Requirement $requirement
     * @return \Illuminate\Http\Response
     */
    public function edit(Requirement $requirement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Requirement $requirement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Requirement $requirement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Requirement $requirement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requirement $requirement)
    {
        //
    }

    private function syncProjectStatusTable($activityId)
    {

        $activity = Activity::find($activityId);
        $component = Component::find($activity->component_id);
        $project = Project::where('id', $component->project_id)->firstOrFail();
        $project->statuses()->attach(9,['user_id'=> auth()->user()->id]);//9 es Requerimiento creado
    }
}
