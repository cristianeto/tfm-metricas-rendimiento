<?php

namespace App\Http\Controllers\API;

use DB;
use Carbon\Carbon;
use App\Models\Component;
use App\Http\Controllers\Controller;

class RequirementsComponentController extends Controller
{
    public function show(Component $component){
        $requirements = DB::table('requirements')
            ->join('activities', 'activities.id', '=','requirements.activity_id')
            ->join('components', 'components.id', '=', 'activities.component_id')
            ->select('requirements.*', 'activities.name as activity_name', 'components.name as component_name', 'components.project_id as project_id')
            ->where('activities.component_id', '=', $component->id)
            ->where('requirements.deleted_at', '=',null)
            ->orderByDesc('requirements.updated_at')
            ->get();
        foreach ($requirements as $req){
            $req->created_at = Carbon::parse($req->created_at)->format('d-M-Y H:i:s');
            $req->updated_at = Carbon::parse($req->updated_at)->format('d-M-Y H:i:s');
            $req->{'human_updated_at'} = Carbon::parse($req->updated_at)->diffForHumans();
        }
       return response()->json($requirements,200);
    }

}
