<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Throwable;

class AuthController extends Controller
{
    /**
     * Parameter @email para que sea encontrada en la base de datos
     * return @tokens
     * return @error
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:App\Models\User,email',
        ]);
        try {
            $user = User::where('email', $request->email)->with('roles:id,name,display_name')->first();
            if ($user) {
                $token = $this->getGrantToken($user);
                //return response()->json($token);
                if (isset($token->error)) return response()->json(["error" => "Credenciales inválidas para la aplicación cliente."], 401);
                $header = [
                    "token_type" => $token->token_type,
                    "access_token" => $token->access_token,
                    "expires_in" => $token->expires_in
                ];
                $token->{'user'} = $user;
                return response()->json($token, 200, $header);
            } else {
                return response()->json(["error" => "Usuario no registrado en el sistema " . env('APP_NAME', 'none')], 401);
            }
        } catch (Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 500);
        }
    }

    /**
     *
     */
    public function logout()
    {
        try {
            if (Auth::check()) {
                $user = Auth::user()->token();
                $user->revoke();
                return response()->json(__("You have been logged out!"), 200);
            } else {
                return response()->json(['message' => __("Unauthenticated user!")], 401);
            }
        } catch (Throwable $th) {
            return response()->json(["Error" => $th->getMessage()], 400);
        }
    }

    /**
     * return an Object json with Tokens type Client Credentials
     * @param $user
     * @return JsonResponse|mixed
     */
    private function getGrantToken($user)
    {
        try {
            $http = new Client;
            $response = $http->post('http://apispirit.test/oauth/token', [
                'form_params' => [
                    'grant_type' => env('PASSGRANT_TYPE'),
                    'client_id' =>  env('PASSCLIENT_ID'),
                    'client_secret' => env('PASSCLIENT_SECRET'),
                    'username' => $user->email,
                    'password' => "spirit" . $user->identification_card,
                ],
            ]);
            return (json_decode($response->getBody()));
        } catch (Throwable $ex) {
            return  ["error" => "Credenciales incorrectas"];
        }

    }
}
