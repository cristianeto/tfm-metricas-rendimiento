<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GuardController extends Controller
{
    public function index()
    {
        $guards = config('auth.guards');
        $guardsArray = [];
        $index = 1;
        foreach ($guards as $guardName => $guard) {
            array_push($guardsArray, array('id' => $index, 'guard_name' => $guardName));
            $index++;
        }

        return response()->json($guardsArray);
    }
}
