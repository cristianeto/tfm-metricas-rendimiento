<?php

namespace App\Http\Controllers\API;

use App\Models\GroupType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groupTypes = GroupType::all();
        return response()->json($groupTypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $groupType = GroupType::create($request->all());
        return response()->json($groupType, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GroupType  $groupType
     * @return \Illuminate\Http\Response
     */
    public function show(GroupType $groupType)
    {
        return response()->json($groupType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GroupType  $groupType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupType $groupType)
    {
        $groupType->update($request->all());
        return response()->json($groupType, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GroupType  $groupType
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupType $groupType)
    {
        $groupType->delete();
        return response()->json(null, 204);
    }
}
