<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Project;
use DB;
use Illuminate\Http\Request;

class BudgetProjectController extends Controller
{
    public function show(Project $project){
        $totalBudget = DB::table('requirements')
            ->join('activities', 'activities.id', '=','requirements.activity_id')
            ->join('components', 'components.id', '=', 'activities.component_id')
            ->join('project', 'project.id', '=', 'components.project_id')
            //->select('project.id', 'project.name', DB::raw('SUM(total_price) as total_budget'))
            ->select(DB::raw('SUM(total_price) as budget'))
            ->where('components.project_id', '=', $project->id)
            ->where('requirements.deleted_at', '=', null)
            ->where('activities.deleted_at', '=', null)
            ->where('components.deleted_at', '=', null)
            ->groupBy('project.id', 'project.name')
            ->first();

        return response()->json($totalBudget);
    }
}
