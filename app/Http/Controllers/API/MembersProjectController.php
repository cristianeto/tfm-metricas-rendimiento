<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use App\Models\Project;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Project_has_users;
use App\Http\Controllers\Controller;
use App\Models\Staff;

class MembersProjectController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Project $project)
    {
        $this->authorize('view', $project);
        $membersOfProject =   Project_has_users::where('project_id', $project->id)
            ->with([
                'staff',
                'user',
                'project:id,name,slug',
            ])
            ->get();
        //$users = Project::find($project->id)->users()->get();
        return response()->json($membersOfProject, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $project = Project::where('slug', $request->project_slug)->firstOrFail();
        /*//dd('holi');
        $projectHasUser = Project_has_users::where('project_id', $project->id)
            ->where('user_id', auth()->user()->id)->firstOrFail();*/
        $this->authorize('create', new Project);
        $this->validate($request, [
            'project_slug' => 'required|exists:App\Models\Project,slug',
            'user_id' => 'required|exists:App\Models\User,id|uuid',
            'staff_id' => [
                'required',
                'exists:App\Models\Staff,id',
                'integer',
                Rule::in([1, 2, 3, 4, 5, 6]),
            ],
        ]);
        $project->users()->syncWithoutDetaching([$request->user_id => ['staff_id' => $request->staff_id]]);
        return response()->json(['message' => __('Record saved successfully')], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Project $project
     * @param User $user
     * @return JsonResponse
     */
    public function update(Request $request, Project $project, User $user)
    {
        $projectHasUser = Project_has_users::where('project_id', $project->id)
            ->where('user_id', $user->id)->firstOrFail();
        //dd($projectHasUser);
        $this->authorize('update', $projectHasUser);
        $this->validate($request, [
            'staff_id' => 'required|exists:App\Models\Staff,id|integer',
        ]);
        $project->users()->updateExistingPivot($user->id, ['staff_id' => $request->staff_id]);

        return response()->json(['message' => __('Record successfully updated')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @param User $user
     * @return JsonResponse
     */
    public function destroy(Project $project, User $user)
    {
        $projectHasUser = Project_has_users::where('project_id', $project->id)
            ->where('user_id', $user->id)->firstOrFail();
        $this->authorize('delete', $projectHasUser);
        $project->users()->detach($user->id);
        return response()->json(null, 204);
    }
}
