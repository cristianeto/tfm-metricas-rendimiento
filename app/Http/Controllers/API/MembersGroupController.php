<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\GroupMember;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MembersGroupController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param Group $group
     * @return JsonResponse
     */
    public function show(Group $group)
    {
        //$this->authorize('view', $project);
        $groupMembers = GroupMember::where('group_id', $group->id)->get();

        return response()->json($groupMembers, 200);
    }
}
