<?php

namespace App\Http\Controllers\API;

use App\Models\ResearchCenter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResearchCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $researchCenters = ResearchCenter::all();
        return response()->json($researchCenters);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $researchCenter = ResearchCenter::create($request->all());
        return response()->json($researchCenter, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ResearchCenter  $researchCenter
     * @return \Illuminate\Http\Response
     */
    public function show(ResearchCenter $researchCenter)
    {
        return response()->json($researchCenter);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ResearchCenter  $researchCenter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResearchCenter $researchCenter)
    {
        $researchCenter->update($request->all());
        return response()->json($researchCenter, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ResearchCenter  $researchCenter
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResearchCenter $researchCenter)
    {
        $researchCenter->delete();
        return response()->json(null, 204);
    }
}
