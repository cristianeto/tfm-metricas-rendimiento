<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Component;
use App\Models\Project;
use App\Models\Project_has_users;
use Illuminate\Http\Request;

class ComponentsProjectController extends Controller
{

    public function show(Project $project){

//        $project_exists = Project_has_users::where('project_id', $project->id)
//            ->where('user_id', auth()->user()->id)
//            ->exists();
        //if($project_exists) {
        $this->authorize('view', new Component);
            $components = $project->components()->get();
            return response()->json($components, 200);
        //}
//        return response()->json([
//            'message' => 'Esta acción no está autorizada.'
//        ],403);
    }

    /*public function store(Project $project, Request $request){
        dd($request->toArray());
        $this->validate($request, [
            'name' => 'required',
            'project_slug' => 'required',
            'nameMeta' => 'required',
        ]);

        $component = new Component(['project_id'=>$project->id]);
        $this->authorize('create', $component);
        $component = Component::create([
            'name' => $request->nameComponent,
            'project_id' => $project->id
        ]);
        return response()->json($component, 201);
    }*/

    public function store(Request $request){

        $this->validate($request, [
            'name' => 'required|max:50',
            'project_slug' => 'required',
            'meta' => 'required'
        ]);
        $project = Project::where('slug', $request->project_slug)->firstOrFail();

        $component = new Component(['project_id'=>$project->id]);
        $this->authorize('create', $component);
        $component = Component::create([
            'name' => $request->name,
            'project_id' => $project->id,
        ]);
        return response()->json($component, 201);    }
}
