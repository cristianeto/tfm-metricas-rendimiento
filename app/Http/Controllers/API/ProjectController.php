<?php

namespace App\Http\Controllers\API;

use App\Models\Status;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Project;
use DB;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveProjectRequest;
use Illuminate\Validation\ValidationException;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        if ((auth()->user()->can('view', new Project))) {
            $projects = Project::withEntities()->get();
        }else if(auth()->user()->hasRole('Director IDI')){
            dd('hola');
        }
        else {
            $projects = User::projectsAllowded()->get();
        }
        return response()->json($projects, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', new Project);
        $request->merge([
            'slug' => $this->formatSlug($request->name),
            'year' => Carbon::now()->year,
        ]);
        $this->validate($request, [
            'name' => 'required|max:500',
            'slug' => 'required|unique:project|max:60'
        ]);
        $project = Project::create($request->all());
        $project->users()->attach(auth()->user()->id, ['staff_id' => 1]);
        $project->statuses()->attach(1,['user_id' => auth()->user()->id]);
        return response()->json($project, 201);
    }

    /**
     * Display the specified resource.
     *
     * @return JsonResponse
     * @param Project $project
     * @throws AuthorizationException
     */
    public function show(Project $project)
    {
        $this->authorize('view', $project);

        $project = Project::where('id', $project->id)
            ->withEntities()
            ->FirstOrFail();
        return response()->json($project, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return JsonResponse
     * @param Project $project
     * @throws AuthorizationException
     * @param SaveProjectRequest $request
     */
    public function update(Project $project, SaveProjectRequest $request)
    {
        $this->authorize('update', $project);
        $project->fill($request->validated());
        $project->slug = $this->formatSlug($project->name);
        $project->save();
        $this->syncPivotTable($project, $request->validated());
        $project->statuses()->attach(2, ['user_id'=>auth()->user()->id]);
        return response()->json($project, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws Exception
     */
    public function destroy(Project $project)
    {
        $this->authorize('delete', $project);
        $project->delete();
        return response()->json(null, 204);
    }


    /** Métodos auxiliares
     * @param $project
     * @param $req
     */
    private function syncPivotTable($project, $req)
    {
        $sectors = $req['sectors'];
        $idsSectors = array_column($sectors, 'id');
        $project->impactSectors()->sync($idsSectors);
    }

    private function formatSlug($value)
    {
        return Str::slug(Str::limit($value, 60, ''));
    }
}
