<?php

namespace App\Http\Controllers\API;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Dependency;
use Illuminate\Http\Response;

class DependencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $dependencies = Dependency::all()->load('dependencyType')
            ->load('fatherDependency');
            //->load('childDependencies');
        return response()->json($dependencies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $dependency = Dependency::create($request->all());

        return response()->json($dependency, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Dependency $dependency
     * @return JsonResponse
     */
    public function show(Dependency $dependency)
    {
        return response()->json($dependency);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Dependency $dependency
     * @return JsonResponse
     */
    public function update(Request $request, Dependency $dependency)
    {
        $dependency->update($request->all());

        return response()->json($dependency, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Dependency $dependency
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Dependency $dependency)
    {
        $this->authorize('delete', $dependency);
        $dependency->delete();
        return response()->json(null, 204);
    }
}
