<?php

namespace App\Http\Controllers\API;

use App\Models\Group;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveGroupRequest;
use Illuminate\Http\Response;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', new Group);
        $groups = Group::all()->load('dependency', 'lines', 'programs', 'grouptypes');
        return response()->json($groups);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveGroupRequest $request
     * @return string
     */
    public function store(SaveGroupRequest $request)
    {
        try {

            $group = Group::create($request->validated());
            $this->syncPivotTable($group, $request->validated());
            return response()->json($group->load('dependency', 'lines', 'programs', 'grouptypes'), 201);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Group $group
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(Group $group)
    {
        $this->authorize('view',  $group);
        $group = Group::find($group->id)->load('lines', 'programs');
        return response()->json($group);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Group $group
     * @param SaveGroupRequest $request
     * @return JsonResponse
     */
    public function update(Group $group, SaveGroupRequest $request)
    {
        $group->update($request->validated());
        $this->syncPivotTable($group, $request->validated());
        return response()->json($group->load('dependency', 'lines', 'programs', 'grouptypes'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Group $group
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Group $group)
    {
        $group->delete();
        return response()->json(null, 204);
    }

    private function syncPivotTable($group, $req)
    {
        $lines = $req['lines'];
        $programs = $req['programs'];
        $idsLines = array_column($lines, 'id');
        $idsPrograms = array_column($programs, 'id');
        $group->lines()->sync($idsLines);
        $group->programs()->sync($idsPrograms);
    }
}
