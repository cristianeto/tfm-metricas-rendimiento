<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveRoleRequest;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', new Role);
        //$roles = Role::select('id', 'name')->with('permissions:id,name')->get();
        $roles = Role::with('permissions:id,name,display_name')->get();
        return response()->json($roles, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveRoleRequest $request)
    {
        $this->authorize('create', new Role);

        $role = Role::create($request->validated());
        $role->givePermissionTo($request->permissions);

        return response()->json($role, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $this->authorize('view', $role);

        $role = Role::where('id', $role->id)
            ->with('permissions:id,name')
            ->FirstOrFail();
        return response()->json($role, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveRoleRequest $request, Role $role)
    {
        $this->authorize('update', $role); //Se puede utilizar en el SaveRoleRequest

        $role->update($request->validated());

        $permissions = $request->permissions;
        $permissionsNames = $this->getPermissionsNames($permissions);
        $role->syncPermissions($permissionsNames);
        return response()->json($role, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $this->authorize('delete', $role);
        $role->delete();
        return response()->json(null, 204);
    }

    private function getPermissionsNames($permissions)
    {
        $permissionsNames = [];
        foreach ($permissions as $permission) {
            $name = $permission['name'];
            array_push($permissionsNames, $name);
        }

        return $permissionsNames;
    }
}
