<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserPermissionController extends Controller
{
    public function update(Request $request, User $user)
    {
        $user->syncPermissions($request->permissions);
        return response()->json(['message' => __('Record successfully updated')], 200);
    }
}
