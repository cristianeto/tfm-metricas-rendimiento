<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\SaveActivityRequest;
use App\Models\Activity;
use App\Models\Component;
use App\Models\Project;
use App\Models\Project_has_users;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveActivityRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(SaveActivityRequest $request)
    {
        $activity = new Activity(['component_id'=>$request->component_id]);

        $this->authorize('create', $activity);
        $activity = Activity::create($request->validated());
        $this->syncProjectStatusTable($request->component_id);
        return response()->json($activity, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Activity $activity
     * @return JsonResponse
     */
    public function show(Activity $activity)
    {
        return response()->json($activity);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        //
    }

    private function syncProjectStatusTable($componentId)
    {
        $component = Component::find($componentId);
        $project = Project::where('id', $component->project_id)->firstOrFail();
        $project->statuses()->attach(10,['user_id'=> auth()->user()->id]);//10 es Actividad creado
    }
}
