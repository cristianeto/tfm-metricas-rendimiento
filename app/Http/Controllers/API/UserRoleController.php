<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UserRoleController extends Controller
{
    public function show(User $user)
    {
        //no estoy utilizando el $user
        $roles = auth()->user()->roles()->get();
        return response()->json($roles);
    }

    public function update(Request $request, User $user)
    {
        $user->syncRoles($request->roles);
        return response()->json(['message' => __('Record successfully updated')], 200);
    }
}
