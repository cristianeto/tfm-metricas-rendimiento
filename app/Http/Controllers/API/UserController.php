<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use App\Events\UserWasCreated;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveUserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        //$users = User::where('password', '=', null)->get();

        $users = User::allowed()->with('roles')->get();
        return response()->json($users);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(User $user)
    {
        $this->authorize('update', $user);
        $user = $user->with(['roles.permissions', 'permissions', 'projects', 'dependencies'])
            ->where('id', $user->id)
            ->firstOrFail();
        return response()->json($user, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SaveUserRequest $request
     * @return JsonResponse
     */
    public function store(SaveUserRequest $request)
    {
        /*$request->merge(['password' => "spirit" . $request->identification_card]);
        dd($request->validated());*/
        $user = User::create([
            'identification_card' => $request->validated()['identification_card'],
            'name' => $request->validated()['name'],
            'lastname' => $request->validated()['lastname'],
            'email' => $request->validated()['email'],
            'sex' => $request->validated()['sex'],
            'belongs_espoch' => $request->validated()['belongs_espoch'],
            'password' => "spirit" . $request->validated()['identification_card'],
        ]);

        if($user->belongs_espoch!=='0'){
            $password = Str::random(8);
            $user->password= $password;
            $user->save();
            //UserWasCreated::dispatch($user, $password);
        }else{
            //UserWasCreated::dispatch($user, 'Utilice las credenciales de la ESPOCH');

        }
        if (Auth::check() && auth()->user()->hasRole('Cimogsys')) {
            //asignando el role
            $user->syncRoles(array_column($request->validated()['roles'], 'name'));
            //Asignar permisos
            $user->givePermissionTo(array_column($request->validated()['permissions'], 'name'));
        } else {
            //Asignar Role al registrarse
            $user->assignRole(Role::find(7));
        }

        //Enviar email
        //UserWasCreated::dispatch($user, 'Utilice las credenciales de la ESPOCH');

        return response()->json($user, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param User $user
     * @param SaveUserRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function update(User $user, SaveUserRequest $request)
    {
        //if (isset($existingUser) && $existingUser->id != $user->id) return response()->json(['message' => __('Already registered user!')], 422);
        $this->authorize('update', $user);
        $user->update($request->validated());
        return response()->json($user, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(User $user)
    {
        $this->authorize('delete', $user);
        $user->delete();
        return response()->json(null, 204);
    }
}
