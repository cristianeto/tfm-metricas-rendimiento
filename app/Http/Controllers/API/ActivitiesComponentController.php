<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Activity;
use App\Models\Component;
use Illuminate\Http\Request;

class ActivitiesComponentController extends Controller
{
    public function show(Component $component){
        $activities = Activity::where('component_id', $component->id)->orderBy('start_date', 'asc')->get();
        return response()->json($activities,200);
    }
}
