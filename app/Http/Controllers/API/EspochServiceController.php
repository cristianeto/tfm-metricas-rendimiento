<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;

class EspochServiceController extends Controller
{
    public function userByDocument(Request $request){
        try {
            $http = new Client;
            $response = $http->get('http://servicioscentral.espoch.edu.ec/Central/ServiciosPersona.svc/ObtenerPorDocumento/'.$request->identification_card);
            return response()->json(json_decode($response->getBody()));
        } catch (GuzzleException $e) {
            return response(json_decode( $e->getMessage()));
        }
    }
}
