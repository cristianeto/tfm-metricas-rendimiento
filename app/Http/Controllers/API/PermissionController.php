<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function index()
    {
        $this->authorize('view', new Permission);
        $permissions = Permission::select('id', 'name', 'display_name')->get();
        return response()->json($permissions, 200);
    }

    public function show(Permission $permission)
    {
        $this->authorize('view', new Permission);
        return response()->json($permission, 200);
    }

    public function update(Request $request, Permission $permission)
    {
        $this->authorize('update', new Permission);
        $data = $request->validate(['display_name' => 'required']);
        $permission->update($data);

        return response()->json($permission, 200);
    }
}
