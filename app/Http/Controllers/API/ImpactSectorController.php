<?php

namespace App\Http\Controllers\API;

use App\Models\ImpactSector;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImpactSectorController extends Controller
{
    public function index()
    {
        $sectors = ImpactSector::orderBy('name')->get();
        return response()->json($sectors);
    }
}
