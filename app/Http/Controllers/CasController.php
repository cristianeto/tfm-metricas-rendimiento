<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CasController extends Controller
{
    public function login()
    {
        if (!Auth::check()) {
            require_once(__DIR__ . '/casConfig.php'); //Consume variables del casConfig.php
            \phpCAS::forceAuthentication(); //Autentica con el servicio CAS de microsoft
            $attributes = \phpCAS::getAttributes();
            $correo = $attributes["upn"];
            $user = \App\Models\User::where('email', $correo)->first();
            if ($user == null) {
                return redirect()->away("https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=https://seguridad.espoch.edu.ec/cas/logout?service=http://localhost/proyectosinvestigacion/public/error");
            } else {
                if (Auth::loginUsingId($user->id_person)) {
                    $user = \Auth::user();
                    $user->generateToken();
                    return redirect()
                        ->intended('/auth/cristian'); //redirije a una vista, no hay como retornar a la vista

                } else {
                    return response()->redirectTo('/errorAlLoguearse');
                }
            }
        } else {
            return view('admin.cristian');
        }
    }
    public function logout(Request $request)
    {
        if (Auth::check()) {
            require_once(__DIR__ . '/casConfig.php');
            /** variables para mi host */
            $host = $_SERVER['HTTP_HOST'];
            $host_upper = strtoupper($host);
            $path = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');

            $protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
            $baseurl = $protocol . $host . $path . "/";
            $urlencoded = urlencode($baseurl);

            $urlESPOCHandMyProject = $casprotocol . $casservername . $casbaseuri . $caslogouturl . $urlencoded;
            $api_token = $request->api_token;
            // dd($api_token);
            $user = \App\Models\User::where('api_token', $api_token)->first();
            if ($user) {
                $user->api_token = null;
                $user->save();
                Auth::logout();
                if (!Auth::check()) {
                    session_destroy();
                    //return redirect()->away("https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=https://seguridad.espoch.edu.ec/cas/logout?service=http://localhost/proyectosinvestigacion/public/");
                    return redirect()->away("https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=" . $urlESPOCHandMyProject);
                }
            } else {
                $errors = "token no existe";
                return view('admin.cristian')->withErrors($errors);
            }
        } else {
            return view('/');
        }
    }

    public function loginAPI()
    {
        require_once(__DIR__ . '/casConfig.php'); //Consume variables del casConfig.php
        \phpCAS::forceAuthentication(); //Autentica con el servicio CAS de microsoft
        $attributes = \phpCAS::getAttributes();
        $correo = $attributes["upn"];
        $user = \App\Models\User::where('email', $correo)->first();
        //dd($attributes);
        if ($user != null) {
            if ($user->api_token == null) {
                $user->generateToken();
            }
            //return redirect()->away("http://localhost/zeus")->withDatos(json_encode($user));
            return response()->json([
                'user' => $user->toArray(),
                'api_token' => $user->api_token,
            ]);
        } else {
            //$user = \Auth::user();
            return redirect()->away("https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=https://seguridad.espoch.edu.ec/cas/logout?service=http://localhost/proyectosinvestigacion/public/api/logout");
        }
    }
    public function logoutAPI(Request $request)
    {
        require_once(__DIR__ . '/casConfig.php');
        /** variables para mi host */
        $host = $_SERVER['HTTP_HOST'];
        $host_upper = strtoupper($host);
        $path = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');

        $protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
        $baseurl = $protocol . $host . $path . "/";
        $urlencoded = urlencode($baseurl);
        //dd($baseurl);
        $urlESPOCHandMyProject = $casprotocol . $casservername . $casbaseuri . $caslogouturl . $urlencoded;
        //$user = Auth::guard('api')->user();
        //dd($user);
        //$api_token = $request->header('Authorization'); //Consumir API_token para desloguearse
        $api_token = $request->api_token;
        // dd($api_token);
        if ($api_token != null) {
            $user = \App\Models\User::where('api_token', $api_token)->first();
            if ($user) {
                $user->api_token = null;
                $user->save();
                session_destroy();
                return redirect()->away("https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=https://seguridad.espoch.edu.ec/cas/logout?service=http://localhost/proyectosinvestigacion/public/api/afterCloseCASLogout");
                //return redirect()->away("https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=".$urlESPOCHandMyProject);
            } else {
                return response()->json([
                    'message' => 'Invalid token!',
                ]);
            }
        } else {
            return response()->json([
                'message' => 'Token is missing!',
            ]);
        }
    }
    public function afterCloseCASLogout()
    {
        return response()->json([
            'message' => 'User Logged Out',
        ]);
    }
    public function validateCAS(Request $request)
    {
        $url = $request->service;
        $ticket = $request->ticket;
        $urlCAS = "https://seguridad.espoch.edu.ec/cas/p3/serviceValidate?service=" . $url . "&ticket=" . $ticket;
        // var_dump(openssl_get_cert_locations());
        //var_dump(getcwd());
        try {
            // Create a new cURL resource
            $curl = curl_init();

            if (!$curl) {
                die("Couldn't initialize a cURL handle");
            }

            // Set the file URL to fetch through cURL
            curl_setopt($curl, CURLOPT_URL, $urlCAS);

            // Set a different user agent string (Googlebot)
            curl_setopt($curl, CURLOPT_USERAGENT, 'Googlebot/2.1 (+http://www.google.com/bot.html)');

            // Follow redirects, if any
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

            // Fail the cURL request if response code = 400 (like 404 errors)
            curl_setopt($curl, CURLOPT_FAILONERROR, true);

            // Return the actual result of the curl result instead of success code
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            // Wait for 10 seconds to connect, set 0 to wait indefinitely
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);

            // Execute the cURL request for a maximum of 50 seconds
            curl_setopt($curl, CURLOPT_TIMEOUT, 50);

            // Do not check the SSL certificates
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            //curl_setopt($curl, CURLOPT_CAPATH, getcwd() . "/cacert.pem");

            // Fetch the URL and save the content in $html variable
            $html = curl_exec($curl);
            // Check if any error has occurred
            if (curl_errno($curl)) {
                echo 'cURL error: ' . curl_error($curl);
            } else {
                /* cURL executed successfully
                print_r(curl_getinfo($curl));*/
                //print el content de CURL
                $res = curl_getinfo($curl);
            }
            // close cURL resource to free up system resources
            curl_close($curl);
            return response()->json($html);
        } catch (\Throwable $th) {
            print_r('Error ' . $th->getMessage());
        }
    }
}
