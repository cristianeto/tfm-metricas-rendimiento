<?php

namespace App\Policies;

use Auth;
use App\Models\User;
use App\Models\Project;
use App\Models\Project_has_users;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;



    public function before($user)
    {
        if ($user->hasRole('Cimogsys') || $user->hasRole('Director IDI')) {
            return true;
        }
    }
    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param  Project  $project
     * @return mixed
     */
    public function view(User $user, Project $project)
    {
        return Project_has_users::where('project_id', $project->id)->where('user_id', $user->id)->exists()
            || $user->hasPermissionTo('View projects', 'api');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('Create projects', 'api');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Project $project
     * @return mixed
     */
    public function update(User $user, Project $project)
    {
        //return false;
        //dd('project policy');
        return Project_has_users::where('project_id', $project->id)->where('user_id', $user->id)->exists()
            || $user->hasPermissionTo('Update projects', 'api');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Project $project
     * @return mixed
     */
    public function delete(User $user, Project $project)
    {
        return Project_has_users::where('project_id', $project->id)
            ->where('user_id', $user->id)
            ->where('staff_id', 1)->exists()
            || $user->hasPermissionTo('Delete projects', 'api');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param  Project  $project
     * @return mixed
     */
    public function restore(User $user, Project $project)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $user
     * @param  Project  $project
     * @return mixed
     */
    public function forceDelete(User $user, Project $project)
    {
        //
    }
}
