<?php

namespace App\Policies;

use App\Models\Component;
use App\Models\Project_has_users;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ComponentPolicy
{
    use HandlesAuthorization;

    public function before(User $user){
        if($user->hasRole('Cimogsys')) return true;
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Component $component
     * @return mixed
     */
    public function view(User $user, Component $component)
    {
        return isset($user);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @param Component $component
     * @return mixed
     */
    public function create(User $user, Component $component)
    {
        return $project_exists = Project_has_users::where('project_id', $component->project_id)
            ->where('user_id', $user->id)
            ->exists();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Component $component
     * @return mixed
     */
    public function update(User $user, Component $component)
    {
        return $project_exists = Project_has_users::where('project_id', $component->project_id)
            ->where('user_id', $user->id)
            ->exists();
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Component $component
     * @return mixed
     */
    public function delete(User $user, Component $component)
    {
        return $project_exists = Project_has_users::where('project_id', $component->project_id)
            ->where('user_id', $user->id)
            ->exists();
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param Component $component
     * @return mixed
     */
    public function restore(User $user, Component $component)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $user
     * @param Component $component
     * @return mixed
     */
    public function forceDelete(User $user, Component $component)
    {
        //
    }
}
