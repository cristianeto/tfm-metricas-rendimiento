<?php

namespace App\Policies;

use App\Models\Dependency;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DependencyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\User $user
     * @param Dependency $dependency
     * @return mixed
     */
    public function view(User $user, Dependency $dependency)
    {
        return $user->hasRole('Cimogsys');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole('Cimogsys');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param Dependency $dependency
     * @return mixed
     */
    public function update(User $user, Dependency $dependency)
    {
        return $user->hasRole('Cimogsys');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Dependency  $dependency
     * @return mixed
     */
    public function delete(User $user, Dependency $dependency)
    {
        if ($dependency->id ===1) return false;

        return $user->hasRole('Cimogsys');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\User $user
     * @param Dependency $dependency
     * @return mixed
     */
    public function restore(User $user, Dependency $dependency)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\User $user
     * @param Dependency $dependency
     * @return mixed
     */
    public function forceDelete(User $user, Dependency $dependency)
    {
        //
    }
}
