<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Project_has_users;
use Illuminate\Auth\Access\HandlesAuthorization;

class MembersProjectPolicy
{
    use HandlesAuthorization;

    public function before($user)
    {
        if ($user->hasRole('Cimogsys')) {
            return true;
        }
    }
    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Project_has_users  $projectHasUsers
     * @return mixed
     */
    public function view(User $user, Project_has_users $projectHasUsers)
    {
        return Project_has_users::where('project_id', $projectHasUsers->project_id)
            ->where('user_id', $user->id)->exists()
            || $user->hasPermissionTo('View project members', 'api');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user, Project_has_users $projectHasUser)
    {
        return Project_has_users::where('project_id', $projectHasUser->project_id)
            ->where('user_id', $user->id)->where('staff_id', 1)->exists()
            ||
            $user->hasPermissionTo('Create project members', 'api');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Project_has_users  $projectHasUsers
     * @return mixed
     */
    public function update(User $user, Project_has_users $projectHasUser)
    {
        if ($projectHasUser->staff_id === 1) return false;
        return Project_has_users::where('project_id', $projectHasUser->project_id)
            ->where('user_id', $user->id)->where('staff_id', 1)->exists()
            || $user->hasPermissionTo('Update project members', 'api');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Project_has_users  $projectHasUsers
     * @return mixed
     */
    public function delete(User $user, Project_has_users $projectHasUser)
    {
        /* dd(Project_has_users::where('project_id', $projectHasUser->project_id)
            ->where('user_id', $projectHasUser->user_id)->where('staff_id', 1)->exists()); */
        if ($projectHasUser->staff_id === 1) return false;
        return (Project_has_users::where('project_id', $projectHasUser->project_id)
            ->where('user_id', auth()->user()->id)->where('staff_id', 1)->exists()
            || $user->hasPermissionTo('Delete project members', 'api'));
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Project_has_users  $projectHasUsers
     * @return mixed
     */
    public function restore(User $user, Project_has_users $projectHasUsers)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Project_has_users  $projectHasUsers
     * @return mixed
     */
    public function forceDelete(User $user, Project_has_users $projectHasUsers)
    {
        //
    }
}
