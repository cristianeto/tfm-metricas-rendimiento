<?php

namespace App\Policies;

use App\Models\Group;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;


    public function before(User $user){
         if($user->hasRole('Cimogsys') || $user->hasRole('Admin groups') ) return true;
    }
    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  User  $user
     * @param  Group  $group
     * @return mixed
     */
    public function view(User $user, Group $group)
    {
        return $user->hasPermissionTo('View groups');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('Create groups');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param  Group  $group
     * @return mixed
     */
    public function update(User $user, Group $group)
    {
        return $user->hasPermissionTo('Update groups');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Group  $group
     * @return mixed
     */
    public function delete(User $user, Group $group)
    {
        return $user->hasPermissionTo('Delete groups');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param User $user
     * @param Group  $group
     * @return mixed
     */
    public function restore(User $user, Group $group)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param User $user
     * @param Group  $group
     * @return mixed
     */
    public function forceDelete(User $user, Group $group)
    {
        //
    }
}
