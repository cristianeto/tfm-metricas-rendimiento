<?php

namespace App\Policies;

use App\Models\Activity;
use App\Models\Component;
use App\Models\Project_has_users;
use App\Models\Requirement;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RequirementPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Requirement  $requirement
     * @return mixed
     */
    public function view(User $user, Requirement $requirement)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user, Requirement $requirement)
    {
        if($requirement->activity_id===null) return false;
        $activity = Activity::find($requirement->activity_id);
        $component = Component::find($activity->component_id);
        return  Project_has_users::where('project_id',$component->project_id)->where('user_id', $user->id)->exists();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Requirement  $requirement
     * @return mixed
     */
    public function update(User $user, Requirement $requirement)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Requirement  $requirement
     * @return mixed
     */
    public function delete(User $user, Requirement $requirement)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Requirement  $requirement
     * @return mixed
     */
    public function restore(User $user, Requirement $requirement)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Requirement  $requirement
     * @return mixed
     */
    public function forceDelete(User $user, Requirement $requirement)
    {
        //
    }
}
