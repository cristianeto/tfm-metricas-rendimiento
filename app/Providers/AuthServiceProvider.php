<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Dependency' => 'App\Policies\DependencyPolicy',
        'App\Models\Group' => 'App\Policies\GroupPolicy',
        'App\Models\Component' => 'App\Policies\ComponentPolicy',
        'Spatie\Permission\Models\Role' => 'App\Policies\RolePolicy',
        'Spatie\Permission\Models\Permission' => 'App\Policies\PermissionPolicy',
        'App\Models\User' => 'App\Policies\UserPolicy',
        'App\Models\Project' => 'App\Policies\ProjectPolicy',
        'App\Models\Project_has_users' => 'App\Policies\MembersProjectPolicy',
        'App\Models\Activity' => 'App\Policies\ActivityPolicy',
        'App\Models\Requirement' => 'App\Policies\RequirementPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
}
