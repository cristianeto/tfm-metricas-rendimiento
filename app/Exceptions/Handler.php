<?php

namespace App\Exceptions;

use BadMethodCallException;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $ex)
    {

        if ($ex instanceof AuthenticationException) {
            return response()->json(['message' => __("Unauthenticated user!")], 401);
        } else if ($ex instanceof ModelNotFoundException) {
            return response()->json(['message' => str_replace('App\\Models\\', '', $ex->getModel()) . " " . __("not found!")], 404);
        } else if ($ex instanceof MethodNotAllowedHttpException) {
            return response()->json(['message' => __("Method not allowed!")], 405);
        } else if ($ex instanceof QueryException) {
            error_log($ex->getMessage());
            return response()->json(['message' => "Error Query Exception!", 'errors' => $ex->getMessage()], 400);
        } else if ($ex instanceof ValidationException) {
            return response()->json(['message' => __($ex->getMessage()), 'errors' => $ex->errors(),], 422);
        } else if ($ex instanceof AuthorizationException) {
            return response()->json(['message' => __($ex->getMessage())], 403);
        } else if ($ex instanceof BadMethodCallException) {
            return response()->json(['message' => __('Method does not exist!')], 405);
        }

        return parent::render($request, $ex);
    }
}
