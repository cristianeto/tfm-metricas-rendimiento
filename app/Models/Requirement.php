<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    use HasFactory;

    protected $table= 'requirements';

    protected $fillable = [
        'name', 'quantity', 'price', 'total_price', 'activity_id'
    ];

    protected $appends = ['human_updated_at'];

    protected $touches = ['activity'];

    public function getHumanUpdatedAtAttribute()
    {
        return Carbon::parse($this->updated_at)->diffForHumans();
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return Carbon::parse($date)->format('d-M-Y H:i:s');
    }

    public function activity(){
        return $this->belongsTo(Activity::class);
    }
}
