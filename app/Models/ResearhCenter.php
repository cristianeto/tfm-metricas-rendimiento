<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResearhCenter extends Model
{
    protected $table = 'researchcenter';
	protected $primaryKey = 'id_researchCenter';

	protected $fillable = [
	'name_researchCenter',
	];

	public $timestamps = false;

	public function groups(){
        return $this->hasMany('App\Models\Group', 'id_researchCenter');
    }
}
