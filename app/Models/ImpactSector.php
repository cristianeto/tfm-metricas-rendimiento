<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ImpactSector extends Model
{
    use HasFactory;

    protected $uuidVersion = 1;
    protected $table = 'impact_sector';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
    ];

    public $timestamps = false;

    /*/**
     * Get the persons for the managementAreas.

    public function persons(){
        return $this->hasMany('App\Models\Project', 'id_impactSector');
    }*/
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'impactsector_has_project');
    }
}
