<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResearchLine extends Model
{
    protected $table = 'researchline';

    protected $fillable = [
        'name',
    ];

    public $timestamps = false;

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'researchline_has_groups', 'group_id', 'researchLine_id');
    }
}
