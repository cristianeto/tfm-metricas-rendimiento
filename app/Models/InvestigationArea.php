<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvestigationArea extends Model
{
    protected $table = 'investigationarea';
	protected $primaryKey = 'id_investigationarea';

	protected $fillable = [
	'name_investigationarea',
	];

    public $timestamps = false;
}
