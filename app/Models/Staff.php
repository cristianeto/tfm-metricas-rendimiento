<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'display_name',
    ];

    public $timestamps = false;

    public function projects()
    {
        return $this->hasMany('App\Models\Role', 'id_role');
    }
    /*  public function users()
    {
        return $this->belongsToMany(User::class, 'users_has_role', 'user_id', 'role_id');
    } */

    public function projectHasUsers()
    {
        return $this->hasMany(Project_has_users::class, 'staff_id');
    }

    public function scopeAllowed($query)
    {
        if (auth()->user()->hasRole('Cimogsys')) {
            return $query;
        } else {
            $query->where('id', '!=', 1)->get();
        }
    }
}
