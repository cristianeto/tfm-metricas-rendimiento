<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes, HasFactory;

    protected $uuidVersion = 1;
    protected $table = 'project';
    protected $primaryKey = 'id';

    protected $appends = ['human_updated_at'];

    protected $guarded = [''];

//    protected $fillable = [
//        'code',
//        'name',
//        'slug',
//        'kind',
//        'startDate',
//        'endDate',
//        'endDateReal',
//        'location',
//        'coverage_type_id',
//        'project_type_id',
//        'research_type_id',
//        'program_id',
//        'year',
//        'father',
//        'id_researchgroup',
//
//        'summary',
//        'background',
//        'justification',
//        'current_situation',
//        'general_objective',
//        'specific_objectives',
//        'sustainability',
//        'methodology',
//        'expected_results',
//        'transference_results',
//        'beneficiaries',
//        'impacts',
//        'aspects',
//    ];


    protected $date = [
        'startDate',
        'endDate',
        'endDateReal',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return Carbon::parse($date)->format('d-M-Y H:i:s');
        /*$dt = Carbon::createFromFormat('Y-m-d H:i:s', $date);
        return $dt->toCookieString();*/

    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function coverageType()
    {
        return $this->belongsTo(CoverageType::class);
    }
    public function projectType()
    {
        return $this->belongsTo(ProjectType::class);
    }
    public function researchType()
    {
        return $this->belongsTo(ResearchType::class);
    }
    public function program()
    {
        return $this->belongsTo(Program::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class, 'project_has_users')->withPivot('staff_id')->withTimestamps();
    }

    public function impactSectors()
    {
        return $this->belongsToMany(ImpactSector::class, 'impactsector_has_project');
    }

    public function dependencies()
    {
        return $this->belongsToMany(Dependency::class, 'project_has_dependency')->withPivot('date_projectHasManagmentArea');
    }

    public function components(){
        return $this->hasMany(Component::class);
    }

    public function statuses(){
        return $this->belongsToMany(Status::class, 'project_has_status')->withTimestamps();
    }

    public function childProject()
    {
        return $this->hasMany('App\Models\Project', 'father');
    }

    public function getHumanUpdatedAtAttribute()
    {
        return Carbon::parse($this->updated_at)->diffForHumans();
    }


    /**Scopes */
    public function scopeWithEntities($query)
    {
        return $query->with([
            'projectType',
            'researchType',
            'coverageType',
            'program',
            'impactSectors',
            //'childProject'
            'users:id,name,lastname',
        ]);
    }
}
