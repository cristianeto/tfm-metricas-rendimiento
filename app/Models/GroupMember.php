<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    protected $table = 'groups_has_users';

    protected $appends = ['human_created_at'];

    protected $fillable = [
        'staff_id',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return Carbon::parse($date)->format('d-M-Y H:i:s');
    }
    public function getHumanCreatedAtAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
