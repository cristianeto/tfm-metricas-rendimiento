<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeStaff extends Model
{
    protected $table = 'typestaff';
	protected $primaryKey = 'id_typeStaff';

	protected $fillable = [
	'name_typeStaff',
	];

	public $timestamps = false;
	public function projectsPersons(){
        return $this->hasMany('App\Project_has_person', 'typeStaff_id_typeStaff');
	}
}
