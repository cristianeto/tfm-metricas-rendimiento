<?php

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Activity extends Model
{
    use HasFactory;

    protected $table = 'activities';
    protected $fillable = [
        'name', 'start_date', 'end_date', 'responsable', 'component_id',
    ];


    protected $casts = [
        'start_date' => 'datetime:d-M-Y',
        'end_date' => 'datetime:d-M-Y',
    ];

    protected $appends = ['human_updated_at'];

    protected $touches = ['component'];

    public function getHumanUpdatedAtAttribute()
    {
        return Carbon::parse($this->updated_at)->diffForHumans();
    }

    public function Component(){
        return $this->belongsTo(Component::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return Carbon::parse($date)->format('d-M-Y H:i:s');
    }
}
