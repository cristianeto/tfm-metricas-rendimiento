<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProjectStatus extends Model
{
    protected $table= "project_has_status";

    protected $fillable = [
        'user_id',
    ];

    protected $appends = ['human_created_at'];

    protected function serializeDate(\DateTimeInterface $date)
    {
        return Carbon::parse($date)->format('d-M-Y H:i:s');
    }

    public function getHumanCreatedAtAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function status() {
        return $this->belongsTo(Status::class);
    }

    public function project() {
        return $this->belongsTo(Project::class);
    }
}
