<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dependency extends Model
{
    use SoftDeletes, HasFactory;

    protected $table = 'dependency';

    protected $fillable = [
        'name', 'acronym', 'contact',
        'city', 'email', 'web',
        'phone', 'dependency_type_id',
        'father',
    ];


    public function dependencyType()
    {
        return $this->belongsTo(DependencyType::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class, 'dependency_has_users', 'dependency_id', 'user_id');
    }
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_has_dependency', 'project_id', 'dependency_id')->withPivot('dependency_type')->withTimestamps;
    }
    public function fatherDependency()
    {
        return $this->belongsTo(Dependency::class, 'father');
    }
    public function childDependencies()
    {
        return $this->hasMany(Dependency::class, 'father');
    }
    public function groups()
    {
        return $this->hasMany(Group::class, 'dependency_id');
    }
}
