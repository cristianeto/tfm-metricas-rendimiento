<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResearchGroup extends Model
{
    protected $table = 'researchgroup';
	protected $primaryKey = 'id_researchGroup';

	protected $fillable = [
    'name_researchGroup',
    'id_researchCenter',
	];

    public $timestamps = false;
}
