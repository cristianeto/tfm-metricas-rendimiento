<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Component extends Model
{
    use HasFactory;

    protected $table = 'components';

    protected $fillable = [
        'name',
        'project_id'
        ];

    protected $touches = ['project'];

    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function  activities(){
        return $this->hasMany(Activity::class);
    }
}
