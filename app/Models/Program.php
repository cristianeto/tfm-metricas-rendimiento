<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Program extends Model
{
    use HasFactory;

    protected $table = 'program';

    protected $fillable = ['name_program',];

    public $timestamps = false;

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'program_has_groups', 'id_group', 'program_id');
    }
}
