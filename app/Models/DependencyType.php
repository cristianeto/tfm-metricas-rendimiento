<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DependencyType extends Model
{
    use HasFactory;

    protected $table = 'dependency_type';

    protected $fillable = [
        'name',
    ];

    public $timestamps = false;

    public function dependencies()
    {
        return $this->hasMany(Dependency::class);
    }
}
