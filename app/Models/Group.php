<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

/**
 * @method static find($id)
 */
class Group extends Model
{
    use SoftDeletes, HasFactory;

    protected $uuidVersion = 1;
    protected $table = 'groups';

    protected $fillable = [
        'id',
        'code',
        'name',
        'acronym',
        'mission',
        'vision',
        'dependency_id',
        'group_type_id',
        'research_center_id',
        'active',
    ];

    public function grouptypes()
    {
        return $this->belongsTo(GroupType::class);
    }
    public function dependency()
    {
        return $this->belongsTo(Dependency::class);
    }
    public function researchCenter()
    {
        return $this->belongsTo(ResearchCenter::class);
    }
    public function lines()
    {
        return $this->belongsToMany(ResearchLine::class, 'researchline_has_groups', 'group_id', 'researchline_id');
    }
    public function programs()
    {
        return $this->belongsToMany(Program::class, 'program_has_groups');
    }
    public function users()
    {
        return $this->belongsToMany(User::class, 'groups_has_users');
    }
}
