<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $table = 'period';
	protected $primaryKey = 'id_period';

	protected $fillable = [
    'name_period',
	];

    const startDate_period = 'startDate_period';
    const endDate_period = 'endDate_period';

    protected $date =[
		'startDate_period',
		'endDate_period',
		];
    public $timestamps = false;

    public function projects()
    {
        return $this->hasMany('App\Models\Project', 'id_period');
    }
}
