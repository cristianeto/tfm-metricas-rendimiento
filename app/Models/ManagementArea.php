<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManagementArea extends Model
{
    protected $table = 'managementarea';
	protected $primaryKey = 'id_managementArea';

	protected $fillable = [
	'name_managementArea',
	'acronym_managmentArea',
	'contact_managementArea',
	'city_managementArea',
	'email_managmentArea',
	'web_managmentArea',
	'phone_managmentArea',
	'participationType_managmentArea',
	'type_managementArea',
	];

    public $timestamps = false;
     /**
     * Get the persons for the managementAreas.
     */
    public function persons(){
        return $this->hasMany('App\Models\User', 'id_managementArea');
	}

	public function projects(){
        return $this->belongsToMany('App\Models\Project', 'project_has_managementarea','id_project', 'id_managementArea')->withPivot('date_projectHasManagmentArea');;
    }
}
