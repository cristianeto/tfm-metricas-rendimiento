<?php

namespace App\Models;

//use GoldSpecDigital\LaravelEloquentUUID\Foundation\Auth\User as Authenticatable;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

/**
 * @method static where(string $string, $email)
 */
class User extends Authenticatable
{
    use Uuid, HasApiTokens, Notifiable, SoftDeletes, HasRoles, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $keyType = 'string';
    public $incrementing = false;

    //protected $uuidVersion = 1;
    protected $fillable = [
        'identification_card', 'name', 'lastname', 'email', 'sex', 'belongs_espoch', 'password',
    ];
    protected $appends = ['fullname', 'human_updated_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //'email_verified_at' => 'datetime',
        //'identification_card' => 'string',
        'id' => 'string',
        //'created_at' => 'datetime:Y-m-d H:m:s',
        //'updated_at' => 'datetime:Y-m-d H:m:s',
    ];
    protected function uuidVersion(): int
    {
        return 1;
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return Carbon::parse($date)->format('d-M-Y H:i:s');
    }

    public function setPasswordAttribute($password){
        $this->attributes['password'] = Hash::make($password);
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_has_users')->withPivot('staff_id')->withTimestamps();
    }
    public function dependencies()
    {
        return $this->belongsToMany(Dependency::class, 'dependency_has_users');
    }
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'groups_has_users')->withTimestamps();
    }
    /*  public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_has_role', 'user_id', 'role_id'); //->withPivot('id_dependency', 'active_personRole');
    } */

    public function findForPassport($email)
    {
        return $this->where('email', $email)->first();
    }

    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->lastname}";
    }

    public function getHumanUpdatedAtAttribute()
    {
        return Carbon::parse($this->updated_at)->diffForHumans();
    }

    public function scopeProjectsAllowded($query)
    {
        return $query->find(auth()->user()->id)->projects()->with([
            'projectType',
            'researchType',
            'coverageType',
            'program',
            'impactSectors',
            //'childProject',
            'users:id,name,lastname',
        ]);
    }
    public function scopeAllowed($query)
    {
        if (auth()->user()->can('view', $this)) {
            return $query;
        }
        return $query->where('id', '!=', auth()->user()->id);
    }
}
