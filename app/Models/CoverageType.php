<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CoverageType extends Model
{
    use HasFactory;

    protected $table = 'coverage_type';

    protected $fillable = ['name',];
    public $timestamps = false;

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
