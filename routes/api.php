<?php

Route::get('/validateCAS', 'CasController@validateCAS');

/** Rutas para usuarios no autenticados */
Route::group(
    [
        'namespace' => 'API'
    ],
    function () {
        Route::post('/auth', 'AuthController@login')->name('auth.login');
        Route::get('/espochService/{identification_card}', 'EspochServiceController@userByDocument')->name('espochService.userByDocument');
        Route::post('/users', 'UserController@store')->name('users.store');

        Route::get('/projects/{project}/resolution', "ResolutionController@show")->name('resolution.show');
        Route::post('/projects/{project}/resolution', "ResolutionController@store")->name('resolution.store');
        //Route::apiResource('/groups', 'GroupController', ['only' => ['index', 'show']]);
    }
);

/** Rutas solo para usuarios autenticados */
Route::group(
    [
        'namespace' => 'API',
        'middleware' => 'auth:api'
    ],
    function () {

        //Route::get('/users/projects', 'ProjectUserController@index')->name('users.projects.index');
        Route::apiResource('/programs', 'ProgramController', ['only' => ['index', 'show']]);
        Route::apiResource('/researchtypes', 'ResearchTypeController', ['only' => ['index']]);
        Route::apiResource('/projecttypes', 'ProjectTypeController', ['only', ['index']]);
        Route::apiResource('/projecttypes', 'ProjectTypeController', ['only', ['index']]);
        Route::apiResource('/coveragetypes', 'CoverageTypeController', ['only', ['index']]);
        Route::apiResource('/dependencytypes', 'DependencyTypeController', ['only', ['index']]);
        Route::apiResource('/grouptypes', 'GroupTypeController', ['only', ['index']]);
        Route::apiResource('/lines', 'ResearchLineController', ['only', ['index']]);
        Route::apiResource('/sectors', 'ImpactSectorController', ['only', ['index']]);
        Route::apiResource('/staffs', 'StaffController', ['only' => ['index']]);

        Route::apiResource('/projects', 'ProjectController');
        Route::apiResource('/dependencies', 'DependencyController');
        Route::apiResource('/groups', 'GroupController');
        Route::apiResource('/users', 'UserController', ['except' => ['store']]);
        Route::apiResource('/roles', 'RoleController');
        Route::apiResource('/permissions', 'PermissionController', ['except' => ['store', 'destroy']]);
        Route::apiResource('/components', 'ComponentController', ['except' => ['show']]);
        Route::apiResource('/activities', 'ActivityController', ['only' => ['store', 'show']]);
        Route::apiResource('/requirements', 'RequirementController', ['only' => ['store', 'show']]);

        //Route::get('/users/projects', 'ProjectController@indexByUser');
        Route::get('/projects/{project}/members', 'MembersProjectController@show');
        Route::post('/projects/members', 'MembersProjectController@store');
        Route::put('/projects/{project}/members/{user}', 'MembersProjectController@update');
        Route::delete('/projects/{project}/members/{user}', 'MembersProjectController@destroy');

        //GroupMembers
        Route::get('/groups/{group}/users', 'MembersGroupController@show')->name('groups.users.show');

        Route::get('/projects/{project}/status', 'ProjectStatusController@show');

        Route::get('/projects/{project}/components', 'ComponentsProjectController@show')->name('projects.components.show');
        Route::post('/projects/components', 'ComponentsProjectController@store')->name('projects.components.store');

        Route::get('/components/{component}/activities', 'ActivitiesComponentController@show')->name('components.activities.show');

        Route::get('/components/{component}/requirements', 'RequirementsComponentController@show')->name('components.requirements.show');

        Route::get('/projects/{project}/budget', 'BudgetProjectController@show')->name('projects.budget.show');

        Route::get('/users/{user}/roles', 'UserRoleController@show');
        /* Route::get('/guards', 'GuardController@index'); */
        //Route::middleware('permission:Update roles')
        Route::middleware('role:Cimogsys')
            ->put('/users/{user}/roles', 'UserRoleController@update');
        Route::middleware('role:Cimogsys')
            ->put('/users/{user}/permissions', 'UserPermissionController@update');

        Route::get('/auth/logout', 'AuthController@logout')->name('auth.logout');
    }
);



Route::fallback(function () {
    return response()->json([
        'message' => __('Not found 404. If error persists, please contact cristian.guaman@espoch.edu.ec')
    ], 404);
});
