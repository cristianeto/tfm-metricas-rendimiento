<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cliente</title>
</head>
<body>
    <form action="{{url('/oauth/clients')}}" method="POST">
        <input type="text" name="name" id="name" placeholder="Name Client"><br><br>
        <input type="text" name="redirect" id="redirect" placeholder="Redirect to"><br><br>
        <input type="submit" name="send" value="Enviar">
        {{ csrf_field() }}
    </form>
    <br><br>
    <h2>Clientes registrados para Spirit Backend</h2>
    <p><strong>Usuario:</strong> {{Auth::user()->name}}</p>
    <table border="1">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name Client</th>
                <th>Redirect</th>
                <th>Secret</th>
              </tr>
        </thead>
        <tbody>
            @foreach ($clients as $client)
                <tr>
                    <td>{{$client->id}}</td>
                    <td>{{$client->name}}</td>
                    <td>{{$client->redirect}}</td>
                    <td>{{$client->secret}}</td>
                </tr>
            @endforeach
        </tbody>
    </table><br><br>
    <h2>Personal Access Tokens</h2>
<form action="{{url('/oauth/personal-access-tokens')}}" method="POST">
    <input type="text" name="name" placeholder="Nombre"/>
    <input type="submit" value="Crear"/>
    {{ csrf_field() }}
</form>
</body>
</html>
