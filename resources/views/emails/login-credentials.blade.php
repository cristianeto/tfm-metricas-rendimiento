@component('mail::message')
# Usuario registrado

Hola {{$user->name}},

Te has registrado o has sido registrado en el Sistema de Proyectos de Investigación ESPOCH {{config('app.name')}}. <br>
Utiliza estas credenciales para acceder al sistema.

## Tus credenciales:

**Correo:** {{$user->email}}<br>
**Contraseña:** {{$password}}<br>
**Fecha de registro:** {{$user->created_at}}<br>
@component('mail::button', ['url' => 'http://localhost:3000'])
Ir al sistema
@endcomponent

Gracias,<br>
{{ config('app.name') }}
**NO RESPONDER**<small> ya que este mensaje ha sido enviado automáticamente.</small>
@endcomponent
